@extends('layouts.app-employee')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link @if (session('settings') || session('changePw') || session('error') || $errors->first('new-password')) @else active @endif"
                           id="home-tab" data-toggle="tab" href="#personalinfo" role="tab" aria-controls="personalinfo"
                           aria-selected="true">Personal Informaton</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('changePw') || session('error') || $errors->first('new-password')) active @endif"
                           id="contact-tab" data-toggle="tab" href="#Change-PW" role="tab" aria-controls="Change-PW"
                           aria-selected="false">Change Password</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('settings')) active @endif" id="contact-tab" data-toggle="tab"
                           href="#notfications" role="tab" aria-controls="notfications"
                           aria-selected="false">Notification Settings</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div
                    class="tab-pane fade @if (session('settings') || session('changePw') || session('error') || $errors->first('new-password')) @else show active @endif"
                    id="personalinfo" role="tabpanel" aria-labelledby="personalinfo-tab">
                    @include('employee.settings.personal-information')
                </div>
                <div
                    class="tab-pane fade @if (session('changePw') || session('error') || $errors->first('new-password')) show active @endif"
                    id="Change-PW" role="tabpanel" aria-labelledby="Change-PW-tab">
                    @include('employee.settings.change-pw')
                </div>
                <div class="tab-pane fade @if (session('settings')) show active @endif" id="notfications"
                     role="tabpanel" aria-labelledby="notfications-tab">
                    @include('employee.settings.notification-settings')
                </div>
            </div>
        </div>
    </div>

@endsection
