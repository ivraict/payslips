@extends('layouts.app-employee')

@section('content')


    <div class="container-fluid" align="center">
        <div class="row justify-content-around">
            <div class="col-lg-8">

                <h2>Instruction Page</h2>
                <hr class="featurette-divider ">

            </div>
        </div>
    </div>

    <br><br>


    <!-------------------------------------------------------------Manage------------------------------------------------------------->


    <div class="container-fluid" align="center">
        <div class="row justify-content-around">
            <div class="col-lg-6 text-center">

                <br>
                <div class="accordion shadow-sm rounded" id="accordionExample" >
                    <div class="card ">
                        <div class="card-header " id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h5><b>Payslips</b></h5>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">

                                <div style="text-align: center"><b> In dit overzicht kan je al jouw loonstrookjes zien met de desbetreffende datum. </b></div> <br>

                                Je het Bestand inzien door op het oogje te klikken onder view, ook kan je het downloaden door op de fileknop te klikken onder download.
                                Als je een specifieke loonstrook nodig hebt kan je zoeken in de zoekbalk.
                                Ook kan je sorteren op naam en tijd, daarvoor staan de pijltjes naast de kopjes.
                                Links boven "Filename" heb je entries, hier kan je aangeven hoeveel loonstrookjes je in totaal wilt zien in de tabel.
                                <br><br>
                                In payslips kan je jouw salaris inzien.
                                Een loonstrook is voor veel werknemers de specificatie door de werkgever van het brutoloon, de toeslagen en inhoudingen, en het nettoloon, rekening houdende met de afspraken in de arbeidsovereenkomst en de wet.
                                Alle gegevens staan hier in, de gewerkte uren, salaris, uurloon etc.
                                <br><br>
                                Dit is wat er allemaal op een loonstrook staat:<br>
                                - Je naam en je burgerservicenummer<br>
                                - De gegevens van je werkgever<br>
                                - Het aantal uren dat je per week werkt<br>
                                - Je brutoloon<br>
                                - De bedragen die van je loon zijn afgetrokken aan belasting en sociale premies<br>
                                - De termijn waar de betaling over gaat. Meestal per week of een maand.<br>
                                - Het minimumloon voor werknemers van jouw leeftijd<br>
                                - Je tariefgroep voor de belasting<br>
                                - Extra vergoedingen, zoals reiskostenvergoeding<br>
                                - Helemaal onderaan: het bedrag dat je overhoudt, je nettosalaris<br>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5><b>Annual statements</b></h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                In dit overzicht kan je al jouw jaaropgaven zien.
                                Je kan ook zien van welk jaar het is.
                                Je kan het bestand inzien en downloaden.
                            <br><br>

                                Een jaaropgave is een overzicht dat je krijgt van je werkgever waarin weergeven staat wat je in het voorgaande jaar verdiend hebt.
                                Het geeft een overzicht van je loongegevens over het gehele vorige jaar.
                                Wanneer je in het afgelopen jaar voor meerdere werkgevers gewerkt hebt, krijg je ook meerdere jaaropgaven.
                                Je jaaropgave ontvang je in januari of aan het begin van februari.
                                <br><br>
                                Een jaaropgave heeft de volgende weergave:<br>
                                – het jaar waarover je loon ontvangen hebt<br>
                                – het bruto bedrag dat je ontvangen hebt (ook wel fiscaal loon genoemd) <br>
                                – de belasting en premies die je betaald hebt (de loonheffing)<br>
                                – de door je werkgever verrekende arbeidskorting<br>
                                – onbelaste vergoedingen zoals een reiskostenvergoeding<br>
                                – de ingehouden bijdrage aan de Zorgverzekeringswet<br>


                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h5><b>Personal information</b></h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                Hier kan je al jouw persoonlijke informatie inzien en bewerken indien dat nodig is.
                                Klik op de gele knop met edit your information om veranderingen aan te brengen.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTable">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true" aria-controls="collapseTable">
                                    <h5><b>Tables</b></h5>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseTable" class="collapse" aria-labelledby="headingTable" data-parent="#accordionExample">
                            <div class="card-body">

                                <div style="text-align: center"><b> De tabellen laten de gegevens zien van bepaalde onderwerpen.<br></b></div> <br>

                                Je kan naar specifieke loonstroken of jaaropgaven zoeken in de zoekbalk rechts boven, verder kan je de aantal rijen per tabel aangeven links boven en links onder kan je de aantal rijen per tabel zien.<br>
                                Je kan door naar de volgende of vorige tabel rechts onder de tabel, ook kan je filteren bij de kopjes door op de pijltjes te drukken(A-Z/Z-A).<br>
                                Als er een oogje staat kan je een bestand inzien onder het kopje "View".<br>
                                Bij het tekentje van een blaadje met een pijltje erdoor onder het kopje "Download" kan je een bestand downloaden. <br>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <br><br>

    </div>




    <style>

        .card-header {
            max-height: 70px;
            margin-top: -3%;
        }

        .card-body{
            text-align: left;
            margin-left:6% ;
            margin-right: 5%;
        }

    </style>


@endsection