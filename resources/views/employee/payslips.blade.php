@extends('layouts.app-employee')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 align="middle">Payslips</h2>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="card-body">
                <div>
                    <script>$(document).ready(function () {
                            $('#usertable').DataTable();
                        });</script>
                    <div class="table-responsive-lg">
                        <table id="usertable" class="table table-hover" border="0">
                            <thead>
                            <tr>
                                <th>Filename</th>
                                <th>Period</th>
                                <th>View</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($payslips)
                                @foreach($payslips as $payslip)
                                    <tr>
                                        <td>{{$payslip->filename}}</td>
                                        <td>@if($payslip->period)
                                                {{date('F, Y', strtotime($payslip->period))}}
                                            @else
                                                {{NULL}}
                                            @endif</td>
                                        <td><a data-url="/view-payslip/{{$payslip->id}}/{{$payslip->filename}}"
                                               class="openPDFdialog" data-toggle="modal" data-target="#modalPDF"
                                               style="color: orange"><i class="fas fa-eye"></i></a></td>
                                        <td><a href="/download-payslip/{{$payslip->id}}" style="color: orange"><i
                                                    class="fas fa-file-download"></i></a></td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!--Modal: PDFViewe-->
    <div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full-height modal-left modal-lg" role="document">


            <!--Content-->

            <div class="modal-content">
                <div class="modal-body mb-0 p-0">
                    <div class="embed-responsive embed-responsive-1by1 z-depth-1-half">
                        <iframe id="href" class="embed-responsive-item" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on("click", ".openPDFdialog", function () {
            var url = $(this).data('url');
            $("#modalPDF iframe").attr("src", url);
        });
    </script>
@endsection
