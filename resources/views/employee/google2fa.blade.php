@extends('layouts.app-employee')

@section('content')


    <hr class="featurette-divider ">
    <h2 align="middle">Google 2-Factor Settings</h2>
    <br><br><br>
    <div class="container">

        <div class="panel panel-default">

            <div class="panel-body" style="text-align: center;">
                @isset($user->google2fa_secret)
                    <h4 align="middle" class="panel-heading">Re-Authenticate Google Authenticator</h4>
                    <p>If you want to re-authenticate your google 2 Factor Authenticator scan the new barcode or manually enter the code {{$secret}}
                        and hit the 'Re-Authenticate' button below  or if u want to turn off Google 2 Factor
                        authentication hit the 'Turn Off Authentication' button below. </p>
                @endisset
                @empty($user->google2fa_secret)
                    <h4 align="middle" class="panel-heading">Set up Google Authenticator</h4>
                    <p>Set up your two factor authentication by scanning the barcode below. Alternatively, you can use the code {{ $secret }}</p>
                @endempty
                <div>
                    <img src="{{ $QR_Image }}">
                </div>
                <br>
                <div>
                    @isset($user->google2fa_secret)
                        <a href="/complete-authentication"><button class="btn btn-deep-orange">Re-Authenticate</button></a>
                    @endisset
                </div>
                <div>
                    @empty($user->google2fa_secret)
                        <a href="/complete-authentication"><button class="btn btn-deep-orange">Complete Authentication</button></a>
                    @endempty
                </div>
                <div>
                    @isset($user->google2fa_secret)
                        <a href="/delete-authentication"><button class="btn btn-deep-orange">Turn Off Authentication</button></a>
                    @endisset
                </div>
            </div>
        </div>
    </div>
@endsection