@extends('layouts.app-employee')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 align="middle">Annual Statements</h2>
            </div>
            <div class="card-body">
                <script>$(document).ready(function () {
                        $('#usertable').DataTable();
                    });</script>
                <div class="table-responsive">
                    <table id="usertable" class="table" border="0">
                        <br>
                        <thead>
                        <tr style=" color: black;">
                            <th>Filename</th>
                            <th>Year</th>
                            <th>View</th>
                            <th>Download</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($annualstatements)
                            @foreach($annualstatements as $annualstatement)
                                <tr>
                                    <td>{{$annualstatement->filename}}</td>
                                    <td>{{$annualstatement->year}}</td>
                                    <td>
                                        <a data-url="/view-annualstatement/{{$annualstatement->id}}/{{$annualstatement->filename}}"
                                           class="openPDFdialog" data-toggle="modal" data-target="#modalPDF"
                                           style="color: orange"><i class="fas fa-eye"></i></a></td>
                                    <td><a href="/download-annualstatement/{{$annualstatement->id}}"
                                           style="color: orange"><i
                                                class="fas fa-file-download"></i></a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--Modal: PDFViewer-->
    <div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full-height modal-left modal-lg" role="document">


            <!--Content-->

            <div class="modal-content">
                <div class="modal-body mb-0 p-0">
                    <div class="embed-responsive embed-responsive-1by1 z-depth-1-half">
                        <iframe id="href" class="embed-responsive-item" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on("click", ".openPDFdialog", function () {
            var url = $(this).data('url');
            $("#modalPDF iframe").attr("src", url);
        });
    </script>
@endsection
