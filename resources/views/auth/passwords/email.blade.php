@extends('layouts.login')

@section('content')
<form method="POST" action="{{ route('password.email') }}">
    @csrf
    <div class="demo">
        <div class="login">
            <img style="width: 100% ; height: 55%;" src="https://cdn1.iconfinder.com/data/icons/web-and-mobile-ui-outline-icon-kit/512/UI_Icons_2-06-512.png">
            <div class="login__form">
                @if (session('status'))
                    <span style="font-size:12px; color: limegreen; " class="invalid-feedback" role="alert">
                                      Your password reset-link has been sent!
                                    </span>
                @endif
                @if ($errors->has('email'))
                        <span style="font-size:12px; color: red; " class="invalid-feedback" role="alert">
                                       The E-mail you entered does not exist in our records. Please double-check and try again.
                                    </span>
                @endif
                <div class="login__row">
                    <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                        <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                    </svg>
                    <input class="login__input name" id="email" type="email"
                           name="email" value="{{ old('email') }}" placeholder="E-Mail" required autofocus/>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="login__submit">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                    <br><br>
                    <a class="login__input" href="/">
                        Back to Login?
                    </a>
                </div>
                <br>
            </div>
        </div>
    </div>
    <div>
    </div>
</form>
@endsection
