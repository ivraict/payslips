@extends('layouts.app-auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="car card-seethrough">
                <div align="center" class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{--@extends('layouts.login')--}}

{{--@section('content')--}}

    {{--<form method="POST" action="{{ route('password.update') }}">--}}
        {{--@csrf--}}

        {{--<input type="hidden" name="token" value="{{ $token }}">--}}
        {{--<div class="demo">--}}
            {{--<div class="login">--}}
                {{--<img style="width: 100% ; height: 55%;" src="https://cdn1.iconfinder.com/data/icons/web-and-mobile-ui-outline-icon-kit/512/UI_Icons_2-06-512.png">--}}
                {{--<div class="login__form">--}}
                    {{--<div>--}}
                        {{--@if ($errors->has('email'))--}}
                            {{--<span style="font-size:12px; color: red; " class="invalid-feedback" role="alert">--}}
                                       {{--The E-mail and/or password you entered did not match our records. Please double-check and try again.--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="login__row">--}}
                        {{--<svg class="login__icon name svg-icon" viewBox="0 0 20 20">--}}
                            {{--<path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />--}}
                        {{--</svg>--}}
                        {{--<input id="email" type="email" class="login__input name form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"--}}
                               {{--name="email" placeholder="E-mail" required autofocus>--}}
                        {{--<div class="login__row">--}}
                            {{--<svg class="login__icon pass svg-icon" viewBox="0 0 20 20">--}}
                                {{--<path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />--}}
                            {{--</svg>--}}
                            {{--<input id="password" type="password" class="login__input pass form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"--}}
                                   {{--name="password" placeholder="new password" required>--}}

                        {{--</div>--}}
                        {{--<div class="login__row">--}}
                            {{--<svg class="login__icon pass svg-icon" viewBox="0 0 20 20">--}}
                                {{--<path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />--}}
                            {{--</svg>--}}
                            {{--<input id="password-confirm" type="password" class="login__input pass form-control"--}}
                                   {{--name="password_confirmation" placeholder="confirm password" required>--}}

                        {{--</div>--}}
                        {{--<br><br><br>--}}
                        {{--<button type="submit" class="login__submit">--}}
                            {{--{{ __('Reset Password') }}--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</form>--}}
{{--@endsection--}}