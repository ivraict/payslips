@extends('layouts.login')

@section('content')

    <form method="POST" action="{{ route('login') }}">
        @csrf
            <div class="demo">
                <div class="login">
                   <img style="width: 100% ; height: 55%;" src="https://cdn1.iconfinder.com/data/icons/web-and-mobile-ui-outline-icon-kit/512/UI_Icons_2-06-512.png">
                    <div class="login__form">
                        <div>
                        @if ($errors->has('email'))
                            <span style="font-size:12px; color: red; " class="invalid-feedback" role="alert">
                                       The E-mail and/or password you entered did not match our records. Please double-check and try again.
                                    </span>
                        @endif
                        </div>

                        <div class="login__row">
                            <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                                <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                            </svg>
                            <input class="login__input name" id="email" type="email"
                                   name="email" value="{{ old('email') }}" placeholder="E-Mail" required autofocus/>
                        <div class="login__row">
                            <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                                <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
                            </svg>
                            <input class="login__input pass" id="password" type="password" name="password" placeholder="Password" required/>
                        </div>
                        <br><br><br>
                        <div>
                                @if (Route::has('password.request'))
                                    <a class="login__input" href="{{ route('password.request') }}">
                                        Forgot your password?
                                    </a>
                                @endif
                        </div>
                        <button type="submit" class="login__submit">{{ __('Login') }}</button>
                        <br>
                    </div>
                </div>
            </div>
            </div>
    </form>
@endsection
