@extends('layouts.login')

@section('content')
    <form method="POST" action="{{ route('2fa') }}">
        @csrf
        <div class="demo">
            <div class="login">
                <img style="width: 100% ; height: 300px;"
                     src="https://cdn1.iconfinder.com/data/icons/web-and-mobile-ui-outline-icon-kit/512/UI_Icons_2-06-512.png">
                <div class="login__form">
                    <div class="login__row">
                        <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                            <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0"/>
                        </svg>
                        <input class="login__input pass" id="one_time_password" type="number" class="form-control"
                               name="one_time_password" placeholder="Authenticator Password" required autofocus/>
                    </div>
                    <button type="submit" class="login__submit">{{ __('Login') }}</button>
                </div>
            </div>
        </div>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="/js/login.js"></script>
    </form>

@endsection