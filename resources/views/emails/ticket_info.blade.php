<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Suppor Ticket Information</title>
</head>
<body>
<p>
    Thank you {{ $user->first_name . " " . $user->insertion . " " . $user->last_name}} for contacting payslips support. We received your message and are eager to help! <br>
    A member of our support team will review your question and follow up with you as soon as possible. <br>
    You will be notified  by email when a response is made.
    <br><br>
    The details of your ticket are shown below:
</p>

<p>
    Title: {{ $ticket->title }} <br>
    Priority: {{ $ticket->priority }} <br>
    Status: {{ $ticket->status }} <br>
</p>

<p>
    You can view the ticket at any time at <a href="{{ url('/super-admin/tickets/'. $ticket->ticket_id) }}">{{ url('/super-admin/tickets/'. $ticket->ticket_id) }}</a>
</p>

</body>
</html>
