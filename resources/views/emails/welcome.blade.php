
<head>
    <title></title>
</head>
<body>

Dear {{$user->first_name}} {{$user->insertion}} {{$user->last_name}}, <br><br>

Your new account for the company {{ $user->company->name }} has been made on {{ config('app.url') }}<br>
@if ($user->hasRole('employee'))
Here you can find your monthly payslips and annual statements.<br>
@endif
<br>
Click <a href="{{ config('app.url') }}">here</a> to check it out!
<br><br>
Your login details are: <br>
username: {{$user->email}} <br>
password: {{$user->temp_password}} <br><br>

We recommend changing your password as soon as possible for security reasons.
<br><br>

Kind regards,<br>
Team {{ config('app.name') }}

</body>


