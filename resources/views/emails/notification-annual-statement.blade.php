
<head>
    <title>{{ $user->company->name }}</title>
</head>
<body>

Dear {{$user->first_name}} {{$user->insertion}} {{$user->last_name}}, <br><br>

{{ $user->company->name }} has uploaded a new annual statement for you and and is available on <i>{{ config('app.url') }}</i>.<br>
Click <a href="{{ config('app.url') }}">here</a> to check it out!<br><br>

Kind regards,<br>
Team {{ config('app.name') }}
</body>


