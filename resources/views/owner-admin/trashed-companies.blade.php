@extends('layouts.app-owner-admin')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 align="middle">Deleted Companies</h2>
            </div>

                @if (session('succes'))
                    <div class="alert alert-success">
                        {{ session('succes') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="container table-responsive">
                    <script>$(document).ready(function () {
                            $('#usertable').DataTable();
                        });</script>
                    <table id="usertable" class="table-hover table" border="0" align="center">
                        <br>
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created at</th>
                            <th>Restore</th>
                            <th>Perma-Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($trashedcompanies)
                            @foreach($trashedcompanies as $trashedcompany)

                                <tr>
                                    <td>{{$trashedcompany->id}}</td>
                                    <td>{{$trashedcompany->name}}</td>
                                    <td>{{$trashedcompany->created_at}}</td>
                                    <td ><a href="restore-company/{{$trashedcompany->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td>
                                    <td><a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-company/{{$trashedcompany->id}}"style="color: orange"><i class="fas fa-trash-alt"></i></a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
