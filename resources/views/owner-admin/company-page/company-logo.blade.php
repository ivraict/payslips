<div class="container">
    <div class="row justify-content-around">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 align="middle">Current Logo</h3>
                </div>
                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    <div style="margin-left: 2%">
                        @isset ($company->logo_path)
                            <img height="50px" src="{{ asset('storage/' . $company->logo_path) }}"></a>
                        @else
                            <p>No logo set!</p>
                        @endisset

                        {{--remove logo:--}}
                        <form action="{{ url('/owner-admin/delete-logo/')}}" method="POST">
                            {!! csrf_field() !!}
                            {{--input type hidden, so it deletes from the corresponding company--}}
                            <input type="hidden" name="company_id"
                                   @if($company)
                                   value="{{$company->id}}" class="form-control" placeholder="{{$company->id}}"
                                    @endif >
                            @isset ($company->logo_path)
                                <button type="submit" class="btn btn-danger">Remove logo</button>
                            @endisset
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 align="middle">Upload Logo</h3>

                </div>

                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div style="margin-left: 2%">
                        <form action="/owner-admin/upload-logo" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="company_id"><h3 align="middle">Upload Logo</h3></label>
                                <p>Must be PNG or JPG format. PNG is recommended.</p>
                            </div>
                            {{--input type hidden, so it uploads to the corresponding company--}}
                            <input type="hidden" name="company_id"
                                   @if($company)
                                   value="{{$company->id}}" class="form-control" placeholder="{{$company->id}}"
                                    @endif >
                            <input type="file" accept="image/*" class="form-control-file" name="documents[]" required/>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                            </div>
                            <input type="submit" class="btn btn-primary" value="Upload"/> <br><br>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
