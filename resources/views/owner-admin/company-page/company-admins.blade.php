<div class="container-fluid">
    <div class="row justify-content-around">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    @if($company)
                        <h2 align="middle"> Company - {{$company->name}}</h2>
                    @endif
                </div>

                <div class="card-body">
                    @if (session('succes'))
                        <div class="alert alert-success">
                            {{ session('succes') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="">
                        <a href="" class="btn btn-warning btn-deep-orange mb-4" data-toggle="modal"
                           data-target="#modalRegisterForm">
                            <i class="fas fa-user-plus"></i></a>
                    </div>

                    <div class="container table-responsive">
                        <script>$(document).ready(function () {
                                $('#usertable').DataTable();
                            });</script>
                        <table id="usertable" class="table">
                            <thead>
                            <tr style=" color: black;">
                                <th>Full Name</th>
                                <th>E-mail</th>
                                <th>Number of permissions</th>
                                <th>Edit/View Permissions</th>
                                <th>Edit Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($admins)
                                @foreach($admins as $admin)
                                    <tr>
                                        <td>{{$admin->first_name}} {{$admin->insertion}} {{$admin->last_name}}</td>
                                        <td>{{$admin->email}}</td>
                                        <td>{{$admin->getAllPermissions()->count()}}</td>
                                        <td><a href="{{ url('owner-admin/permissions/' . $admin->id) }}"><i
                                                        style="color: orange;" class="fas fa-user-lock"></i></a></td>
                                        <td><a href="{{action('CompanyController@edit_sa' , $admin['id'])}}"
                                               data-id="{{$admin->id}}" data-externalid="{{$admin->external_id}}"
                                               data-firstname="{{$admin->first_name}}"
                                               data-insertion="{{$admin->insertion}}"
                                               data-lastname="{{$admin->last_name}}" data-email="{{$admin->email}}"
                                               data-google2fa_secret="{{$admin->google2fa_secret}}"
                                               data-toggle="modal" data-target="#modalEditUserForm"
                                               style="color: orange;"><i class="fas fa-user-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="card">
                <div class="card-header">
                    Available permissions
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @foreach ($uniquepermis as $uniquepermi)
                            <li class="list-group-item">{{ $uniquepermi }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="{{action('CompanyController@insert_admin_to_company')}}"
              class="form-container">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="get">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 align="middle" class="modal-title w-100 font-weight-bold">Add Admin</h4>
                        <div class="md-form">
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <input type="hidden" name="company_id" value="{{$company->id}}"
                                       class="form-control validate">
                            </div>

                            <div class="form-row">
                                <div class="col">
                                    First Name
                                    <input type="text" name="first_name" class="form-control" required>
                                </div>
                                <div class="col">
                                    Insertion
                                    <input type="text" name="insertion" class="form-control">
                                </div>
                                <div class="col">
                                    Last Name
                                    <input type="text" name="last_name" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                E-Mail
                                <input type="email" name="email" class="form-control validate" required>
                            </div>

                            <div class="form-group">
                                Password
                                <input id="password" type="text" name="password" class="form-control validate" required>
                            </div>

                            <button type="button" class="btn btn-deep-orange" href="#" onClick="autoFill()">Generate
                                password
                            </button>


                            <div class="form-group">
                                Permissions
                                @if ($permissions)
                                    <ul class="list-group">
                                        @foreach ($permissions as $permission)
                                            <li class="list-group-item">{{ $permission->name }}

                                                <input class="float-right" type="checkbox" name="permission[]"
                                                       value="{{$permission->name}}"
                                                >
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-deep-orange">Submit</button>
                    </div>

                </div>
            </div>
        </form>
    </div>


    <div class="modal fade" id="modalEditUserForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        @if(count($errors) > 0)
            <div class="alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($message = Session::get('success'))
            <div class="alert-success">
                <p>{{$message}}</p>
            </div>
        @endif
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">Edit (Super)admin</h4>
                    <div class="md-form">
                        <form class="editform" method="post" action="/owner-admin/updateuser/" id="EditUserForm">
                            {{method_field('PATCH')}}
                            {{csrf_field()}}
                            <input type="hidden" name="id" id="user_id">
                            <div class="form-group">
                                First Name
                                <input id="user_first" type="text" name="first_name" class="form-control">
                            </div>

                            <div class="form-group">
                                Insertion
                                <input id="user_insertion" type="text" name="insertion" class="form-control">
                            </div>

                            <div class="form-group">
                                Last Name
                                <input id="user_last" type="text" name="last_name" class="form-control">
                            </div>

                            <div class="form-group">
                                E-Mail
                                <input id="user_email" type="email" required name="email" class="form-control">
                            </div>
                            <div class="form-group col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="google2fa_secret" id="user_google2fa_secret">
                                    <input type="checkbox" name="google2fa_secret" id="google2fa_secret"
                                           class="custom-control-input" value="{{NULL}}">
                                    <label class="custom-control-label checkbox-inline" for="google2fa_secret">Turn-Off
                                        Google 2FA</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-deep-orange">Save Changes</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $('#modalEditUserForm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            var firstname = button.data('firstname')
            var inserion = button.data('insertion')
            var lastname = button.data('lastname')
            var email = button.data('email')
            var google2fa_secret = button.data('google2fa_secret')

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #user_id').val(id)
            modal.find('.modal-body #user_first').val(firstname)
            modal.find('.modal-body #user_insertion').val(inserion)
            modal.find('.modal-body #user_last').val(lastname)
            modal.find('.modal-body #user_email').val(email)
            modal.find('.modal-body #user_google2fa_secret').val(google2fa_secret)
            $('#EditUserForm').attr('action', '/owner-admin/updateuser/' + id);
        })
    </script>

    <script>
        function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        function autoFill() {
            document.getElementById('password').value = makeid(8);
        }
    </script>

    <script>$('#google2fa').tooltip({boundary: 'window'})</script>
</div>
