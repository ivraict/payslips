@extends('layouts.app-owner-admin')

@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('owneradmin.viewallcompanies')}}">
                        &laquo;Back</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (session('success')) @else active @endif" id="home-tab" data-toggle="tab" href="#personalinfo" role="tab" aria-controls="personalinfo"
                    aria-selected ="true">Personal Informaton</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (session('success')) active @endif" id="profile-tab" data-toggle="tab" href="#companyinfo" role="tab" aria-controls="companyinfo"
                    aria-selected ="false">Company Info</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade @if (session('success')) @else show active @endif" id="personalinfo" role="tabpanel" aria-labelledby="personalinfo-tab">
            <br>
            @include('owner-admin.company-page.company-admins')
        </div>
        <div class="tab-pane fade @if (session('success')) show active @endif" id="companyinfo" role="tabpanel" aria-labelledby="companyinfo-tab">
        <br>
        @include('owner-admin.company-page.company-logo')
    </div>
</div>
</div>
</div>
@endsection
