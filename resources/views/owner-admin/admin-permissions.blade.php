@extends('layouts.app-owner-admin')

@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 align="middle">
                            Permissions: {{ $admin->first_name }} {{ $admin->insertion }} {{ $admin->last_name }}</h4>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('owneradmin.show-company-information', ['id' => $admin->company_id]) }}" type="button" class="btn btn-primary">&laquo; Back</a><br><br>
                        <form method="post"
                              action="{{action('PermissionController@assign_permission_as_owner_admin', $admin->id)}}">
                            @csrf
                            @if ($permissions)
                                <ul class="list-group">
                                    @foreach ($permissions as $permission)
                                        <li class="list-group-item">{{ $permission->name }}

                                            <input class="float-right" type="checkbox" name="permission[]"
                                                   value="{{$permission->name}}"
                                                   @if(in_array($permission->name, $adminperminames))
                                                   checked="checked"
                                                @endif>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            <input type="submit" class="btn btn-deep-orange" value="Save">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
