@extends('layouts.app-owner-admin')
@section('content')

    <ul class="nav nav-tabs" id="myTab" role="tablist">

        <li class="nav-item">
            <a class="nav-link active" id="Google2FA-tab" data-toggle="tab" href="#Google2FA" role="tab" aria-controls="Google2FA"
               aria-selected="true">Change Password</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="Google2FA" role="tabpanel" aria-labelledby="Google2FA-tab">
            <br><br><br>
            @include('owner-admin.settings.change-pw')
        </div>
    </div>

@endsection