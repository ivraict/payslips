@extends('layouts.app-owner-admin')

@section('content')


    <div class="container">
        <h2 align="middle">Old Payslips</h2>
        <h6 align="middle">Payslips that are older than 7 years will show here</h6>
        @if (session('succes'))
            <div class="alert alert-success">
                {{ session('succes') }}
            </div>
        @endif
        <a href="/owner-admin/forcedelete-old-payslips">
            <button class="btn btn-warning">
                Delete all
            </button>
        </a>
        <script>$(document).ready(function () {
                $('#usertable').DataTable();
            });</script>
        <table id="usertable" class="table" border="0">
            <thead>
            <tr>
                <th>Filename</th>
                <th>Period</th>
                <th>Company</th>
                <th>Perma-Delete</th>
            </tr>
            </thead>
            <tbody>
            @if($payslips)
                @foreach($payslips as $payslip)

                    <tr>
                        <td>{{$payslip->filename}}</td>
                        <td>@if($payslip->period)
                                {{date('F, Y', strtotime($payslip->period))}}
                            @else
                                {{NULL}}
                            @endif</td>
                        <td>@isset ($payslip->company)
                                {{$payslip->company->name}}
                            @endisset</td>
                        <td><a href="/owner-admin/forcedelete-payslip/{{$payslip->id}}" style="color:
                            @isset($auth->company->theme_color)
                            {{$auth->company->theme_color}};
                            @else
                                orange;
                            @endisset"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
