@extends('layouts.app-owner-admin')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 align="middle">Companies</h2>
            </div>

            <div class="card-body">
                <div class="">
                        <a href="" class="btn btn-warning btn-deep-orange mb-4" data-toggle="modal"
                           data-target="#modalRegisterForm">
                            <i class="fas fa-plus-circle"></i></a>
                </div>

                @if (session('succes'))
                    <div class="alert alert-success">
                        {{ session('succes') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="container table-responsive">
                    <script>$(document).ready(function () {
                            $('#usertable').DataTable();
                        });</script>
                    <table id="usertable" class="table-hover table" border="0" align="center">
                        <br>
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created at</th>
                            <th>View</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($companies)
                            @foreach($companies as $company)

                                <tr>
                                    <td>{{$company->id}}</td>
                                    <td>{{$company->name}}</td>
                                    <td>{{$company->created_at}}</td>
                                    <td><a href="/owner-admin/show-company-information/{{$company->id}}" style="color: orange"><i
                                                    class="fas fa-eye"></i></a></td>
                                    <td><a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                                           href="/owner-admin/softdelete-company/{{$company->id}}" style="color: orange"><i
                                                    class="fas fa-trash-alt"></i></a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="{{action('CompanyController@insert_company')}}" class="form-container">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="get">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title w-100 font-weight-bold">Add New Company</h4>
                        <div class="md-form">

                            <div class="form-group">
                               Company Name
                                <input type="text" name="name" class="form-control validate" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-deep-orange">Submit</button>
                    </div>

                </div>
            </div>
        </form>
    </div>

@endsection
