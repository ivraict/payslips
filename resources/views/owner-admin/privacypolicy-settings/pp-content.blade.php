@extends('layouts.app-owner-admin')

@section('content')


    <script>

        $(document).ready(function() {

            $("#demo-editor-bootstrap").Editor();

        });

    </script>

    <div class="container-fluid">
        <div class="row">
            <h2 class="demo-text">A demo of jQuery / Bootstrap based editor</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 nopadding">
                        <textarea id="demo-editor-bootstrap"></textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
