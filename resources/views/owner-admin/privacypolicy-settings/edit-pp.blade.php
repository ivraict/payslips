@extends('layouts.app-owner-admin')

@section('content')

    <h2 class="editheader">Edit Privacy Policy</h2>

    <form align="middle" class="editform" method="post" action="{{action('OwnerAdminController@update_pp', $pp_particle['id'])}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH">

        <h4>Header</h4>
        <input style=" width:1000px" type="text" name="header" value="{{$pp_particle->header}}">
        <br>

        <h4>Text Field</h4>
        <br>
        <textarea name="text_field" style="height:500px; width:1000px">{{$pp_particle->text_field}}</textarea>
        <br>

        <input type="submit" value="Save">
    </form>
@endsection
