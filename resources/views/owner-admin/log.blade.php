@extends('layouts.app-owner-admin')

@section('content')

    <h2 align="middle">Log: Owner</h2>

    <div class="container-fluid">
        <div>Company: <select onchange="location = this.value;">
                <option>Select</option>
                <option value="/owner-admin/log/">Owner (you)</option>
                @php
                $nontrashedcompanies = $companies->where('deleted_at', NULL);
                @endphp
                @if($nontrashedcompanies)
                    @foreach($nontrashedcompanies as $company)
                        <option value="/owner-admin/log/{{$company->id}}">{{$company->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <script>$(document).ready(function () {
                $('#usertable').DataTable();
            });</script>
        <div class="table-responsive">
            <table id="usertable" class="table-hover" border="0" align="center">
                <thead>
                <tr>
                    <th>Date/Time</th>
                    <th>User</th>
                    <th>Event</th>
                    <th>Type</th>
                    <th>Old values</th>
                    <th>New values</th>
                    {{--<th>Route</th>--}}
                    {{--<th>IP Address</th>--}}
                </tr>
                </thead>
                <tbody>
                @if($audits)
                    @foreach($audits as $audit)
                        <tr>
                            <td>{{date('F dS, Y - H:i', strtotime($audit->created_at))}}</td>
                            <td>{{$audit->user->first_name}} {{$audit->user->insertion}} {{$audit->user->last_name}}</td>
                            <td>@if ($audit->event == 'created' &&
                            ($audit->auditable_type == 'App\Payslip' ||
                            $audit->auditable_type == 'App\AnnualStatement' ||
                            $audit->auditable_type == 'App\PaymentStatement' ||
                            $audit->auditable_type == 'App\JournalEntry' ||
                            $audit->auditable_type == 'App\PayrollTax'))
                                    uploaded
                                @elseif ($audit->event == 'created' &&
                                (strpos($audit->url, 'import') == true))
                                    imported
                                @elseif ($audit->event == 'deleted' &&
                                (strpos($audit->url, 'softdelete') == true))
                                    soft deleted
                                @elseif ($audit->event == 'deleted' &&
                                (strpos($audit->url, 'softdelete') == false))
                                    perma deleted
                                @else
                                    {{$audit->event}}
                                @endif
                            </td>
                            <td>{{substr($audit->auditable_type, 4)}}</td>
                            <td>@foreach($audit->old_values as $key => $old_value)
                                    @isset ($old_value)
                                        @if ($key == 'user_id')
                                            <b>user: </b>
                                            @php
                                                $user = $users->find($old_value);
                                            @endphp
                                            @if ($user)
                                                {{ $user->first_name }}
                                                {{ $user->insertion }}
                                                {{ $user->last_name }}
                                                <br>
                                            @else
                                                <br>
                                            @endif
                                        @elseif ($key == 'company_id')
                                            <b>company: </b>
                                            @php
                                                $company = $companies->find($old_value);
                                            @endphp
                                            @if ($company)
                                                {{ $company->name }}
                                                <br>
                                            @endif
                                        @elseif ($audit->auditable_type == 'App\User' && $key == 'id')
                                            @php
                                                $user = $users->find($old_value);
                                            @endphp
                                            @if ($user)
                                                @if ($user->hasRole('admin'))
                                                    <b>role:</b> admin
                                                    <br>
                                                @elseif ($user->hasRole('employee'))
                                                    <b>role:</b> employee
                                                    <br>
                                                @endif
                                            @endif
                                        @elseif ($audit->auditable_type == 'App\User' && $audit->event == 'updated')
                                            @php
                                                $user = $users->find($audit->auditable_id);
                                            @endphp
                                            @if ($user)
                                                <b>user: </b> {{ $user->first_name }} {{ $user->insertion }} {{ $user->last_name }}
                                                <br>
                                                <b>{{$key}}: </b> {{ $old_value }}
                                                <br>
                                            @endif
                                        @else
                                            <b>{{$key}}: </b> {{ $old_value }}
                                            <br>
                                        @endif
                                    @endisset
                                @endforeach
                            </td>
                            <td>@foreach($audit->new_values as $key => $new_value)
                                    @isset ($new_value)
                                        @if ($key == 'user_id')
                                            <b>user: </b>
                                            @php
                                                $user = $users->find($new_value);
                                            @endphp
                                            @if ($user)
                                                {{ $user->first_name }}
                                                {{ $user->insertion }}
                                                {{ $user->last_name }}
                                                <br>
                                            @else
                                                <br>
                                            @endif
                                        @elseif ($key == 'company_id')
                                            <b>company: </b>
                                            @php
                                                $company = $companies->find($new_value);
                                            @endphp
                                            @if ($company)
                                                {{ $company->name }}
                                                <br>
                                            @endif
                                        @elseif ($audit->auditable_type == 'App\User' && $key == 'id')
                                            @php
                                                $user = $users->find($new_value);
                                            @endphp
                                            @if ($user)
                                                @if ($user->hasRole('admin'))
                                                    <b>role:</b> admin
                                                    <br>
                                                @elseif ($user->hasRole('employee'))
                                                    <b>role:</b> employee
                                                    <br>
                                                @endif
                                            @endif
                                        @elseif ($audit->auditable_type == 'App\User' && $audit->event == 'updated')
                                            @php
                                                $user = $users->find($audit->auditable_id);
                                            @endphp
                                            @if ($user)
                                                <b>user: </b> {{ $user->first_name }} {{ $user->insertion }} {{ $user->last_name }}
                                                <br>
                                                <b>{{$key}}: </b> {{ $new_value }}
                                                <br>
                                            @endif
                                        @else
                                            <b>{{$key}}: </b> {{ $new_value }}
                                            <br>
                                        @endif
                                    @endif
                                @endforeach
                            </td>
                            {{--<td>{{$audit->url}}</td>--}}
                            {{--<td>{{$audit->ip_address}}</td>--}}
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
