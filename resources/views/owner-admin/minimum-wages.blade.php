@extends('layouts.app-owner-admin')

@section('content')
    <div class="container">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable">
            Create minimum wage
        </button>
        @if (session('succes'))
            <div class="alert alert-success">
                {{ session('succes') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif
    <!-- Modal -->
        <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Create minimum wage</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{url('/owner-admin/store/minimum-wages')}}" class="form-container">
                            @csrf
                            <div class="form-group">
                                <label>Hours/Week</label>
                                <input type="text" name="hoursweek" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>15</label>
                                <input type="text" name="age15" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>16</label>
                                <input type="text" name="age16" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>17</label>
                                <input type="text" name="age17" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>18</label>
                                <input type="text" name="age18" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>19</label>
                                <input type="text" name="age19" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>20</label>
                                <input type="text" name="age20" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>21</label>
                                <input type="text" name="age21" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>22+</label>
                                <input type="text" name="age22AndOlder" class="form-control">
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h2 align="middle">Minimum wages Law</h2>
                </div>
                <div class="card-body">
                    For more information about minimum wages in the Netherlands:
                    <a href="https://www.rijksoverheid.nl/onderwerpen/minimumloon/bedragen-minimumloon/bedragen-minimumloon-2019"
                       target="_blank">
                        <i>Here</i></a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Edit</th>
                                <th scope="col">Hours/Week</th>
                                <th scope="col">15</th>
                                <th scope="col">16</th>
                                <th scope="col">17</th>
                                <th scope="col">18</th>
                                <th scope="col">19</th>
                                <th scope="col">20</th>
                                <th scope="col">21</th>
                                <th scope="col">22+</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($lawminimumwages)
                                @foreach($lawminimumwages as $lawminimumwage)
                                    <tr>
                                        <td>
                                            <a onclick="" href=""
                                               data-id="{{$lawminimumwage->id}}"
                                               data-hoursweek="{{$lawminimumwage->hoursweek}}"
                                               data-age15="{{$lawminimumwage->age15}}"
                                               data-age16="{{$lawminimumwage->age16}}"
                                               data-age17="{{$lawminimumwage->age17}}"
                                               data-age18="{{$lawminimumwage->age18}}"
                                               data-age19="{{$lawminimumwage->age19}}"
                                               data-age20="{{$lawminimumwage->age20}}"
                                               data-age21="{{$lawminimumwage->age21}}"
                                               data-age22="{{$lawminimumwage->age22AndOlder}}"


                                               data-toggle="modal"
                                               data-target="#modalEditForm"
                                               style="color: orange;">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        </td>
                                        <td>{{$lawminimumwage->hoursweek}} h</td>
                                        <td>€ {{$lawminimumwage->age15}}</td>
                                        <td>€ {{$lawminimumwage->age16}}</td>
                                        <td>€ {{$lawminimumwage->age17}}</td>
                                        <td>€ {{$lawminimumwage->age18}}</td>
                                        <td>€ {{$lawminimumwage->age19}}</td>
                                        <td>€ {{$lawminimumwage->age20}}</td>
                                        <td>€ {{$lawminimumwage->age21}}</td>
                                        <td>€ {{$lawminimumwage->age22AndOlder}}</td>
                                        <td>
                                            <a onclick="return confirm('Are you sure you want to perma-delete this record?')"
                                               href="/owner-admin/destroy/minimum-wage/{{$lawminimumwage->id}}"
                                               style="color: orange"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>




    <div class="modal fade" id="modalEditForm" tabindex="-1" role="dialog"
         aria-labelledby="modalEditFormTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditFormTitle">Create minimum wage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/owner-admin/update/minimum-wage/" class="form-container" id="EditForm">
                        {{method_field('PATCH')}}
                        @csrf

                        <input type="hidden" name="id" id="id">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Hours/Week</label>
                            <input id="hoursweek" type="text" name="hoursweek" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">15</label>
                            <input id="age15" type="text" name="age15" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">16</label>
                            <input id="age16" type="text" name="age16" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">17</label>
                            <input id="age17" type="text" name="age17" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">18</label>
                            <input id="age18" type="text" name="age18" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">19</label>
                            <input id="age19" type="text" name="age19" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">20</label>
                            <input id="age20" type="text" name="age20" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">21</label>
                            <input id="age21" type="text" name="age21" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">22+</label>
                            <input id="age22" type="text" name="age22" class="form-control">
                        </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#modalEditForm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            var hoursweek = button.data('hoursweek')
            var age15 = button.data('age15')
            var age16 = button.data('age16')
            var age17 = button.data('age17')
            var age18 = button.data('age18')
            var age19 = button.data('age19')
            var age20 = button.data('age20')
            var age21 = button.data('age21')
            var age22 = button.data('age22')


            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #id').val(id)
            modal.find('.modal-body #hoursweek').val(hoursweek)
            modal.find('.modal-body #age15').val(age15)
            modal.find('.modal-body #age16').val(age16)
            modal.find('.modal-body #age17').val(age17)
            modal.find('.modal-body #age18').val(age18)
            modal.find('.modal-body #age19').val(age19)
            modal.find('.modal-body #age20').val(age20)
            modal.find('.modal-body #age21').val(age21)
            modal.find('.modal-body #age22').val(age22)

            $('#EditForm').attr('action', '/owner-admin/update/minimum-wage/' + id);
        })
    </script>
@endsection
