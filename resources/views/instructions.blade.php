@extends('layouts.app-admin')
@section('content')


    <div class="container-fluid" align="center">
        <div class="row justify-content-around">
            <div class="col-lg-8">

    <h2>Instruction Page Super-Admin</h2>
    <hr class="featurette-divider ">

            </div>
        </div>
    </div>

    <br><br>


    <!-------------------------------------------------------------Manage------------------------------------------------------------->


    <div class="container-fluid" align="center">
        <div class="row justify-content-around">
            <div class="col-lg-6">

                <h2><b>Manage:</b></h2>
                <br>
                <div class="accordion shadow-sm rounded" id="accordionExample" >
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h5><b>Employees</b></h5>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">

                                <div style="text-align: center"><b>In dit overzicht kan je alle werknemers en admins zien.</b></div> <br>

                                Hier zie je de user nummer, de volledige naam en de email.
                                Verder kan je ook nog naar een bepaalde user zijn profiel gaan door op het oogje te klikken onder "Employee profile", en je kan de employee editten of verwijderen.
                               Boven de entries zie je een knop met een persoontje en een plus, met deze knop kan je werknemers toevoegen.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5><b>List of all payslips</b></h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">

                                <div style="text-align: center"><b> In dit overzicht kan je alle loonstroken zien.</b></div> <br>

                                je kan zien aan wie de loonstroken gekoppeld zijn en de periode van de payslip.
                                Ook kan je ze inzien door op het oogje onder "view" te klikken.
                                Daarnaast kan je het downloaden door op het file icoontje te klikken onder download.
                                En je kan de naam van de file, de periode en het gekoppelde persoon veranderen door op het blokje met de pen te klikken onder "edit".
                                Als laatste kan je ze deleten door op het prullenbakje te klikken onder het kopje "Delete".
                                <br><br>
                                Helemaal rechts vind u de plek waar u de loonstroken kunt uploaden.
                                Kies eerst het jaar en daarna het bestand of meerdere bestanden.
                                Je kan het nu normal uploaden en word het niet toegekent aan een persoon.
                                Zodra je op auto assign upload klikt dan worden de payslips automatisch gekoppeld aan de juiste persoon indien de juiste gegevens en juiste naam conventie zijn gebruikt.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h5><b>List of annual statements</b></h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">

                                <div style="text-align: center"><b> In dit overzicht kan je alle jaaropgaven zien.</b></div> <br>

                                Je kan zien aan wie de jaaropgaven gekoppeld zijn het jaar van de jaaropgaven.
                                Ook kan je ze inzien door op het oogje onder "view" te klikken.
                                Daarnaast kan je het downloaden door op het file icoontje te klikken onder download.
                                En je kan de naam van de file, het jaar en het gekoppelde persoon veranderen door op het blokje met de pen te klikken onder "edit".
                                Als laatste kan je ze deleten door op het prullenbakje te klikken onder het kopje "Delete".
                                Ook hier kan je zoeken in de zoekbalk en aangeven hoeveel jaaropgaven je per tabel wilt zien.
                                <br><br>
                                Helemaal rechts vind u de plek waar u de jaaropgaven kunt uploaden.
                                Kies eerst het jaar en daarna het bestand of meerdere bestanden.
                                Je kan het nu normal uploaden en word het niet toegekent aan een persoon.
                                Zodra je op auto assign upload klikt dan worden de jaaropgaven  automatisch gekoppeld aan de juiste persoon, indien de gegevens goed staan ingevult en een juiste naam conventie is aangehouden.
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <br><br>

<!-------------------------------------------------------------Fiscal support------------------------------------------------------------->


    <div class="container-fluid" align="center">
        <div class="row justify-content-around">
            <div class="col-lg-6">

                <h2><b>Fiscal:</b></h2>
                <br>
                <div class="accordion shadow-sm rounded" id="accordionExample" >
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    <h5><b>List of all payment statements</b></h5>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="card-body">
                                <div style="text-align: center"><b> Hier is een betaal overzicht van alle mensen in loondienst per periode.  <br></b></div>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <h5><b>List of all journal entries</b></h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                            <div class="card-body">
                                <div style="text-align: center"><b>Hier is een journaalpost van alle mensen in loondienst per periode te vinden. <br></b></div>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <h5><b>List of all payrol taxes</b></h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                            <div class="card-body">
                                <div style="text-align: center"><b>Hier is een overzicht van af te dragen belastingen voor alle mensen in loondienst per periode te vinden. <br></b></div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <br><br>

    <!-------------------------------------------------------------Payments------------------------------------------------------------->

        <div class="container-fluid" align="center">
            <div class="row justify-content-around">
                <div class="col-lg-6">

                    <h2><b>Payments:</b></h2>
                    <br>
                    <div class="accordion shadow-sm rounded" id="accordionExample" >

                        <div class="card">
                            <div class="card-header" id="headingNine">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                                        <h5><b>Bank transactions</b></h5>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                                <div class="card-body">

                                    <div style="text-align: center"><b>  Hier kunnen exel exports van bank transacties geupload worden en automatisch worden toegewezen aan de desbetreffende medewerker.</b></div> <br>

                                    Als je jouw medewerkers hebt betaald kan je een export excel file ervan maken bij jouw bank.
                                    Dit bestand kan je bij bank transactions in deze applicatie uploaden door eerst op "bestand kiezen" te klikken en daarna de groene knop "import user data".
                                    De gegevens worden uit het excel bestand gelezen en in de tabel geplaatst.
                                    Als u op de knop "auto-assign" klikt worden deze gegevens automatisch gekoppeld aan de bijbehorende profielen van de medewerkers door het bankrekening nummer.
                                    Mocht je weer een export willen draaien van alles in de tabel kan dat ook door op de gele knop "export user data" te klikken
                                    <br><br>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTen">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                        <h5><b>Failed bank transactions</b></h5>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
                                <div class="card-body">

                                    <div style="text-align: center"><b>Hier is een overzicht van banktransacties die niet zijn gekoppeld aan medewerkers (unassigned).<br></b></div> <br>

                                    Wanneer je bij Bank transactions op auto assign klikt en hij kan het niet auto assignen aan een profiel dan word hij in de tabel van failed bank transactions gegooid.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
    </div>
            <br><br>


        <!-------------------------------------------------------------Others------------------------------------------------------------->

        <div class="container-fluid" align="center">
            <div class="row justify-content-around">
                <div class="col-lg-6">

                    <h2><b>Others:</b></h2>
                    <br>
                    <div class="accordion shadow-sm rounded" id="accordionExample" >
                        <div class="card">
                            <div class="card-header" id="headingEleven">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven">
                                        <h5><b>Trashcan</b></h5>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionExample">
                                <div class="card-body">

                                    <div style="text-align: center"><b> Hier zijn alle soft-deleted employees en admins, payslips, annual statements, payroll taxes, payment statements,transactions en jourlnal entries.<br></b></div> <br>

                                    Als er een getal achter een kopje staat betekent het dat er iets is ge-softdeleted.
                                    Als je daar naartoe gaat kom je bij een tabel van soft-deleted daar kan je ze terughalen of permanent wilt verwijderen (bekijk handleiding van table).
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTable">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true" aria-controls="collapseTable">
                                            <h5><b>Tables</b></h5>
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseTable" class="collapse" aria-labelledby="headingTable" data-parent="#accordionExample">
                                    <div class="card-body">

                                        <div style="text-align: center"><b> De tabellen laten de gegevens zien van bepaalde onderwerpen.<br></b></div> <br>

                                        Je kan naar specifieke gegevens zoeken in de zoekbalk rechts boven, verder kan je de aantal rows per tabel aangeven links boven en links onder kan je de aantal rijen per tabel zien.<br>
                                        Je kan door naar de volgende of vorige tabel rechts onder de tabel, ook kan je filteren bij de kopjes door op de pijltjes te drukken(A-Z/Z-A).<br>
                                        Als er een oogje staat kan je een bestand inzien onder het kopje "View" of naar een specifiek profiel toe onder het kopje "User profile".<br>
                                        Als er een prullenbakje met een pijltje erdoor staat onder het kopje "Restore" betekent dit dat je een soft-deleted gegeven terug kan halen.<br>
                                        Bij een gewoon prullenbakje onder het kopje "delete" kan je gegevens soft-deleten en onder het kopje "Perma-Delete" Kan je ze helemaal uit het systeem halen.<br>
                                        Bij het tekentje van een blaadje met een pijltje erdoor onder het kopje "Download" kan je een bestand downloaden. <br>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwelve">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve">
                                                <h5><b>Generate payslip</b></h5>
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordionExample">
                                        <div class="card-body">

                                            <div style="text-align: center"><b>Hier kunnen er payslips gemaakt worden<br></b></div> <br>

                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingThirteen">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="true" aria-controls="collapseThirteen">
                                                    <h5><b>Support</b></h5>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div style="text-align: center"><b>Hier kan je naartoe gaan als je hulp nodig hebt, deze instructie pagina, FAQ en je kan contact opzoeken met de host.<br></b></div> <br>

                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingFourteen">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="true" aria-controls="collapseFourteen">
                                                        <h5><b>Log</b></h5>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordionExample">
                                                <div class="card-body">

                                                    <div style="text-align: center"><b> Hier kan je elke handeling zien die is gemaakt binnen jouw bedrijf.<br></b></div> <br>

                                                    Je kan dit gebruiken om te zien wat de admins gedaan hebben zodat je altijd op de hoogte bent.
                                                    Je kan het tijdstip zien van wanneer de actie ondernomen is, welke gebruiker het heeft gedaan, wat de actie is, waaraan hij of zij iets heeft veranderd, wat het eerst was en wat er veranderd is.
                                                    En je kan de route zien waar hij iets aan heeft veranderd.
                                                    En tot slot kan je het Ip adres zien van waaruit hij of zij werkt.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

























        <style>

            .card-header {
                max-height: 70px;
                margin-top: -3%;
            }

            .card-body{
                text-align: left;
                margin-left:6% ;
                margin-right: 5%;
            }

        </style>

@endsection




