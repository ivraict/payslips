<!-- Footer -->
<footer class="page-footer font-small pt-4">
    <div style="background-color:
        @isset($auth->company->footer_color)
            {{$auth->company->footer_color}};
        @else
            darkorange;
        @endif
        color: black">
        <div class="container">
    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Footer links -->
        <div class="row text-center text-md-left mt-3 pb-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                <br>
                <h6 class="text-uppercase mb-4 font-weight-bold">
                    @isset($auth->company)
                    {{$auth->company->name}}</h6>
                @endisset
                @isset($auth->company->footer_text)
                <p>{{$auth->company->footer_text}}</p>
                @endisset


            </div>
            <!-- Grid column -->




            <!-- Grid column -->
            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                <br>
                <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                @isset($auth->company->address)
                <p>
                    <i class="fas fa-home mr-3"></i>{{$auth->company->address}}</p>
                @endisset
                @isset($auth->company->email)
                <p>
                    <i class="fas fa-envelope mr-3"></i><a style="color: black;" href="mailto:{{$auth->company->email}}">{{$auth->company->email}}</a></p>
                @endisset
                @isset($auth->company->phone_number)
                <p>
                    <i class="fas fa-phone mr-3"></i>{{$auth->company->phone_number}}</p>
                @endisset
                @isset($auth->company->email2)
                <p >
                    <i class="fas fa-envelope mr-3"></i><a style="color: black;" href="mailto:{{$auth->company->email2}}">{{$auth->company->email2}}</a></p>
                @endisset
                @isset($auth->company->phone_number2)
                <p>
                    <i class="fas fa-phone mr-3"></i>{{$auth->company->phone_number2}}</p>
                @endisset
            </div>
            <!-- Grid column -->

        </div>
        <!-- Footer links -->

        <hr>

        <!-- Grid row -->
        <div class="row d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-7 col-lg-8">
                {{--Privacy Policy--}}
                <p><a style="color: black;" href="/privacypolicy"><strong>Privacy Policy</strong></a></p>
                <!--Copyright-->
                <p class="text-center text-md-left">© 2019 Copyright:
                    <a style="color: black;" target="_blank">
                        <strong>{{ config('app.name') }}</strong>
                    </a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-5 col-lg-4 ml-lg-0">

                <!-- Social buttons -->
                <div class="text-center text-md-right">
                    <ul class="list-unstyled list-inline">
                        @isset($auth->company->facebook_link)
                        <li class="list-inline-item">
                            <a style="color: black;" href="{{$auth->company->facebook_link}}" target="_blank" class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        @endif
                        @isset($auth->company->instagram_link)
                        <li class="list-inline-item">
                            <a style="color: black;" href="{{$auth->company->instagram_link}}" target="_blank" class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-instagram "></i>
                            </a>
                        </li>
                        @endif
                        @isset($auth->company->linkedin_link)
                        <li class="list-inline-item">
                            <a style="color: black;" href="{{$auth->company->linkedin_link}}" target="_blank" class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->
        </div>
    </div>
</footer>
<!-- Footer -->
</html>
