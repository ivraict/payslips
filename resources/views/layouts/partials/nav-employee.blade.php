<nav class="navbar navbar-expand-md bg-light navbar-light fixed-top">
    <a class="navbar-brand" href="/payslips/">
        @if($auth->company->logo_path)
            <img height="50px" src="{{ asset('storage/' . $auth->company->logo_path) }}">
        @else
            <img height="50px" src="../../img/MastiekLogoHeader.png">
        @endif
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/payslips">
                    Payslips</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/annual-statements">
                    Annual Statements</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-users-cog"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/settings"><i class="fas fa-user-cog"></i>
                        Settings</a>
                    <a class="dropdown-item" href="/Google2FA"><i class="fas fa-user-cog"></i>
                        Google2FA</a>
                    <a class="dropdown-item" href="/instructions"><i class="fas fa-question"></i>
                        Help</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>
                    Sign Out</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
