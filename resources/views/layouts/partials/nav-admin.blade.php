<nav class="navbar navbar-expand-md bg-light navbar-light fixed-top">
    <a class="navbar-brand" href="/payslips/">
        @if($auth->company->logo_path)
            <img height="50px" src="{{ asset('storage/' . $auth->company->logo_path) }}">
        @else
            <img height="50px" src="../../img/MastiekLogoHeader.png">
        @endif
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Manage
                </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                    @if(auth()->user()->can('create admins'))
                        <a class="dropdown-item" href="/admin/all-admins"><i class="fas fa-user-shield"></i> Admins</a>
                    @endif
                    <a class="dropdown-item" href="/admin/"><i class="fas fa-user-alt"></i> Employees</a>
                    <a class="dropdown-item" href="/admin/all-payslips"><i class="fas fa-file-alt"></i>
                        Payslips</a>
                    <a class="dropdown-item" href="/admin/all-annual-statements"><i class="fas fa-file-alt"></i>
                        Annual Statements</a>
                        <a class="dropdown-item" href="/admin/all-bank-transactions"><i
                                    class="fas fa-file-import"></i>
                            Bank transactions</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Fiscal
                </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/admin/all-payment-statements"><i class="fas fa-file-alt"></i>
                        Payment
                        Statements</a>
                    <a class="dropdown-item" href="/admin/all-journal-entries"><i class="fas fa-file-alt"></i>
                        Journal Entries</a>
                    <a class="dropdown-item" href="/admin/all-payroll-taxes"><i class="fas fa-file-alt"></i>
                        Payroll Taxes</a>
                </div>
            </li>
            @if(auth()->user()->can('generate pdf'))
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Make PDF <span class="badge badge-warning">Beta</span>
                </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/admin/make-payslip/index"><i class="fas fa-file-pdf"></i>
                        Make Payslips</a>
                    <a class="dropdown-item" href="/admin/make-annual-statement/index"><i class="fas fa-file-pdf"></i>
                        Make Annual Statements</a>
                </div>
            </li>
            @endif

            <li class="nav-item">
                <a class="nav-link" href="/admin/minimum-wages">
                    Minimum Wage</a>
            </li>



            <li class="nav-item">
                <a class="nav-link" href="/admin/trash">
                    Trash</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/log">
                    Log</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-users-cog"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/admin/settings"><i class="fas fa-user-cog"></i> Settings</a>
                    <a class="dropdown-item" href="/admin/Google2FA"><i class="fas fa-user-cog"></i> Google2FA</a>
                    <a class="dropdown-item" href="/admin/support"><i class="fas fa-question"></i>
                        Help</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>
                        Sign
                        Out</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
