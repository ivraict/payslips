<nav class="navbar navbar-expand-md bg-light navbar-light fixed-top">
    <a class="navbar-brand" href="/owner-admin/">
        <img height="50px" src="../../img/MastiekLogoHeader.png">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Companies
                </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/owner-admin">All Companies</a>
                    <a class="dropdown-item" href="/owner-admin/trashed-companies">Deleted Companies</a>
                </div>
            </li>
            {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="/owner-admin/pp-content">Edit Privacy Policy</a>--}}
            {{--</li>--}}
            <li class="nav-item">
                <a class="nav-link" href="/owner-admin/log">Log</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/owner-admin/minimum-wages">Minimum Wages</a>
            </li>

            {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="/owner-admin/tickets">Tickets</a>--}}
            {{--</li>--}}

            <li class="nav-item">
                <a class="nav-link" href="/owner-admin/old-payslips"><span class="badge badge-primary badge-pill">{{$oldpayslipcount}}</span>
                    Old payslips
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-users-cog"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/owner-admin/settings"><i class="fas fa-user-cog"></i>
                        Settings</a>
                    <a class="dropdown-item" href="/owner-admin/Google2FA"><i class="fas fa-user-cog"></i>
                        Google2FA</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>
                        Sign Out</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
