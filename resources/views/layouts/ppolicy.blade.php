<html lang="en">

<head>

    @include('layouts.partials.head')

</head>

<body class="padding">


@include('layouts.partials.nav-ppolicy')




<div class="full-height">
    @yield('content')
</div>
{{--owner admin heeft geen company_id, dus ook geen footer data--}}
@role('owner_admin')
@else
@include('layouts.partials.footer')
@endrole

</body>

</html>
