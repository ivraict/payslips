@extends('layouts.app-admin')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-around">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h2 align="middle">List of all Payment Statements</h2>
                    </div>

                    <div class="card-body">

                        @if(session('uploaded'))
                            <div class="alert alert-success">
                                <ul>
                                    @foreach ((session('uploaded')) as $upload)
                                        <li>{{$upload}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session('deleted'))
                            <div class="alert alert-info">
                                <ul>
                                    @foreach ((session('deleted')) as $delet)
                                        <li>{{$delet}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <script>$(document).ready(function () {
                                $('#usertable').DataTable();
                            });</script>
                        <br><br><br>
                        <table id="usertable" class="table">
                            <thead>
                            <tr>

                                <th>Filename</th>
                                <th>Period</th>
                                <th>Download</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($paymentstatements)
                                @foreach($paymentstatements as $paymentstatement)
                                    <tr>

                                        <td>{{$paymentstatement->filename}}</td>
                                        <td>@if($paymentstatement->period)
                                                {{date('F, Y', strtotime($paymentstatement->period))}}
                                            @else
                                                {{NULL}}
                                            @endif</td>
                                        <td><a href="/admin/download-payment-statement/{{$paymentstatement->id}}"
                                               style="color: orange"><i class="fas fa-file-download"></i></a></td>
                                        <td>
                                            <a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                                               href="/admin/softdelete-payment-statement/{{$paymentstatement->id}}"
                                               style="color: orange"><i class="fas fa-trash-alt"></i></a></td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 align="middle">File Upload</h3>
                    </div>

                    <div class="card-body">
                        <br>
                        <div style="margin-left: 2%">
                            <form action="/admin/upload-paymentstatements" method="post"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="company_id"></label>
                                </div>
                                <input type="month" class="form-control" name="period" required>
                                <label for="File Name">Date of the Payment Statement <br></label><br>
                                <br/>
                                <input type="file" class="form-control-file" name="documents[]" multiple required>
                                <br/>(can attach more than one)
                                <br/>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <input type="hidden" name="company_id" value="{{$auth->company_id}}"
                                           class="form-control validate">
                                </div>
                                <br>
                                <input type="submit" name="submit-normal" class="btn btn-deep-orange"
                                       value="Normal Upload"/>
                                <br><br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MULTIPLE FILE UPLOAD-->
        </div>
    </div>

@endsection
