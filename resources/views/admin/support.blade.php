@extends('layouts.app-admin')

@section('content')
    <aside style="margin-left: 20%; margin-right: 20%">
        <div>
            <h1>Instructions</h1>
            <p>In our knowledge base you will find extensive manuals for both employees and admins.</p>
        </div>
        <a href="instructions">
            <button class="btn btn-primary">
                Go to instructions
            </button>
        </a>
        <hr class="featurette-divider">
        <br>

        <div>
            <h1>Contact</h1>
            <p>Below is our e-mail address where you can contact us.<br>
            <ul>
                <li>test@mail.com</li>
            </ul>
            </p>
        </div>

        {{--<hr class="featurette-divider">--}}
        {{--<br>--}}
        {{--<div>--}}
        {{--<h1>Send ticket</h1>--}}
        {{--<p>If you have a short question, we can often answer it by ticket.--}}
        {{--As soon as you have more questions or the answer becomes more complex, we will propose to contact you by--}}
        {{--phone.</p>--}}
        {{--<a href="my_tickets">--}}
        {{--<button class="btn btn-primary">--}}
        {{--Tickets--}}
        {{--</button>--}}
        {{--</a>--}}
        {{--</div>--}}


    </aside>
@endsection
