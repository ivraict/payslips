<div class="container">
    <h2 align="center"> User Settings - @if($user) {{$user->first_name}} {{$user->insertion}} {{$user->last_name}} @endif</h2>
        <div class="card-body">
            @if (session('settings'))
                <div class="alert alert-success">
                    {{ session('settings') }}
                </div>
            @endif
            <form class="editform" method="post"
                  action="{{action('EmployeeController@update_user_settings', $user->id)}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <br>
                Email notification for when a new payslip is uploaded:
                <input type="hidden" name="email_notif_payslip" value="0"/>
                <input type="checkbox" name="email_notif_payslip" value="1"
                       @if($user->email_notif_payslip == 1)
                       checked="checked"
                        @endif>
                <br><br>
                Email notification for when a new annual statement is uploaded:
                <input type="hidden" name="email_notif_annual_statement" value="0"/>
                <input type="checkbox" name="email_notif_annual_statement" id="enp" value="1"
                       @if($user->email_notif_annual_statement == 1)
                       checked="checked"
                        @endif>
                <br><br>
                Turn off the Google 2fa settings for the user.
                <input type="hidden" name="google2fa_secret" value="{{$user->google2fa_secret}}">
                <input type="checkbox" name="google2fa_secret" id="google2fa_secret" value="{{NULL}}">
                <br><br>
                <input type="submit" class="btn btn-deep-orange" value="Save">
            </form>
        </div>
    </div>
</div>
