<div class="container">
<div class="card">
    <div class="card-header">
        <h2 align="middle"> Bank Transactions
            - @if($user) {{$user->first_name}} {{$user->insertion}} {{$user->last_name}} @endif</h2>
    </div>
    <div class="card-body">
    <script>$(document).ready(function () {
            $('#usertable3').DataTable();
        });
    </script>
    <table id="usertable3" class="table-hover table" border="0" align="center">
        <thead>
        <tr>
            <th>Assigned?</th>
            <th>Date</th>
            <th>Name</th>
            <th>Account Number</th>
            <th>Contra account number</th>
            <th>Code</th>
            <th>Out/In</th>
            <th>Amount</th>
            <th>Mutation type</th>
            <th>Communications</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tbody>
        @if($banktransactions)
            @foreach($banktransactions as $banktransaction)
                <tr>
                    <td>@if($banktransaction->user_id)
                            <i class="fas fa-check">
                        @endif
                    </td>
                    <td>{{$banktransaction->date}}</td>
                    <td>{{$banktransaction->name}}</td>
                    <td>{{$banktransaction->bank_account_number}}</td>
                    <td>{{$banktransaction->contra_account_number}}</td>
                    <td>{{$banktransaction->code}}</td>
                    <td>{{$banktransaction->out_in}}</td>
                    <td>{{$banktransaction->amount}}</td>
                    <td>{{$banktransaction->mutation_type}}</td>
                    <td>{{$banktransaction->communications}}</td>
                    <td>
                        <a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                           href="/admin/delete-bank-transaction/{{$banktransaction->id}}"
                           style="color: orange"><i
                                class="fas fa-trash-alt"></i></a></td>

                </tr>
            @endforeach
        @endif

        </tbody>
    </table>
</div>
</div>
</div>