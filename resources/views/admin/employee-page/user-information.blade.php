<h2 align="center">Personal Information</h2>
<div class="card-body">
<br>
    @if (session('succes'))
        <div class="alert alert-success">
            {{ session('succes') }}
        </div>
    @endif

    <form method="post" action="{{action('EmployeeController@update', $user->id)}}">
        {{method_field('PATCH')}}
        {{csrf_field()}}
        <input type="hidden" name="id" id="user_id">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputFirstname">First Name</label>
                <input type="text" class="form-control" name="first_name" id="inputFirstname" value="{{$user->first_name}}" required>
            </div>
            <div class="form-group col-md-2">
                <label for="inputInsertion">Insertion</label>
                <input type="text" class="form-control" name="insertion" id="inputInsertion" value="{{$user->insertion}}" >
            </div>
            <div class="form-group col-md-4">
                <label for="inputLastname">Last Name</label>
                <input type="text" class="form-control" name="last_name" id="inputLastname" value="{{$user->last_name}}" required>
            </div>
            <div class="form-group col-md-2">
                <label for="inputExternal">External ID</label>
                <input type="number" class="form-control" name="external_id" id="inputExternal" value="{{$user->external_id}}" >
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="inputEmail">E-Mail</label>
                <input type="email" class="form-control" name="email" id="inputEmail" value="{{$user->email}}" required>
        </div>
            <div class="form-group col-md-2">
                <label for="Phonenumber">Phone Number</label>
                <input type="tel" class="form-control" name="phone_number"  id="Phonenumber" value="{{$user->phone_number}}" >
            </div>
            <div class="form-group col-md-3">
                <label for="IBAN">Bank Account Number (IBAN)</label>
                <input type="text" class="form-control" name="bank_account_number" id="IBAN" value="{{$user->bank_account_number}}" required>
            </div>
            <div class="form-group col-md-2">
                <label for="BSN">BSN</label>
                <input type="text" class="form-control" name="bsn" id="BSN" value="{{$user->bsn}}" required>
            </div>
            <div class="form-group col-md-2">
                <label for="Birthdate">Date of Birth</label>
                <input type="text" class="form-control" name="date_of_birth" id="Birthdate" value="{{$user->date_of_birth}}" >
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="Streetname">Street Name</label>
                <input type="text" class="form-control" name="street_name" id="Streetname" value="{{$user->street_name}}" >
            </div>
            <div class="form-group col-md-2">
                <label for="HouseNumber">House Number</label>
                <input type="text" class="form-control" name="house_number" id="HouseNumber" value="{{$user->house_number}}" >
            </div>
            <div class="form-group col-md-2">
                <label for="ZipCode">Zip Code</label>
                <input type="text" class="form-control" name="postal_code" id="ZipCode" value="{{$user->postal_code}}" >
            </div>
            <div class="form-group col-md-2">
                <label for="Residence">Place of Residence</label>
                <input type="text" class="form-control" name="residence" id="Residence" value="{{$user->residence}}" >
            </div>
            <div class="form-group col-md-2">
                <label for="Google2FA">Google2FA</label><br>
                @isset($user->google2fa_secret)
                    <input type="text" class="form-control" id="Google2FA" value="Turned On" disabled>
                @endisset
                @empty($user->google2fa_secret)
                    <input type="text" class="form-control" id="Google2FA" value="Turned Off" disabled>
                @endempty
            </div>
        </div>
        <button type="submit" class="btn btn-deep-orange" id="SaveButton">Save Changes</button>
    </form>
    </div>
