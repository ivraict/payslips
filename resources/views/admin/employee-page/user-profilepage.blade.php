@extends('layouts.app-admin')
@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.viewallusers')}}">
                            &laquo;Back</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('settings') ||
            session('uploadedAS') ||
            session('uploadedPS') ||
            session('workinfo') ||
            session('deletedPS') ||
            session('deletedAS')) @else active @endif"
                           id="home-tab" data-toggle="tab" href="#personalinfo" role="tab"
                           aria-controls="personalinfo"
                           aria-selected="true">Personal Informaton</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('workinfo')) active @endif" id="contact-tab" data-toggle="tab"
                           href="#workinfo" role="tab"
                           aria-controls="workinfo"
                           aria-selected="false">Work Informaton</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('uploadedPS') || session('deletedPS')) active @endif"
                           id="profile-tab" data-toggle="tab"
                           href="#payslipsinfo" role="tab"
                           aria-controls="payslipsinfo"
                           aria-selected="false">Payslips</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('uploadedAS') || session('deletedAS')) active @endif"
                           id="contact-tab" data-toggle="tab"
                           href="#annualstatementsinfo" role="tab"
                           aria-controls="annualstatementsinfo"
                           aria-selected="false">Annual Statements</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#banktransactionsinfo" role="tab"
                           aria-controls="banktransactionsinfo"
                           aria-selected="false">Bank Transactions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('settings')) active @endif" id="contact-tab" data-toggle="tab"
                           href="#settings" role="tab" aria-controls="settings"
                           aria-selected="false">Settings</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade @if (session('settings') ||
        session('uploadedAS') ||
        session('uploadedPS') ||
        session('workinfo') ||
        session('deletedPS') ||
        session('deletedAS'))  @else show active @endif"
                     id="personalinfo" role="tabpanel" aria-labelledby="personalinfo-tab">
                    <br>
                    @include('admin.employee-page.user-information')
                    <br>
                </div>
                <div class="tab-pane fade @if (session('workinfo')) show active @endif" id="workinfo" role="tabpanel"
                     aria-labelledby="workinfo-tab">
                    <br>
                    @include('admin.employee-page.work-information')
                    <br>
                </div>
                <div class="tab-pane fade @if (session('uploadedPS') || session('deletedPS')) show active @endif"
                     id="payslipsinfo" role="tabpanel"
                     aria-labelledby="payslipsinfo-tab">
                    <br><br>
                    @include('admin.employee-page.user-payslips')
                    <br>
                </div>
                <div class="tab-pane fade @if (session('uploadedAS') || session('deletedAS') ) show active @endif"
                     id="annualstatementsinfo"
                     role="tabpanel" aria-labelledby="annualstatementsinfo-tab">
                    <br><br>
                    @include('admin.employee-page.user-annual-statements')
                    <br>
                </div>
                <div class="tab-pane fade" id="banktransactionsinfo" role="tabpanel"
                     aria-labelledby="banktransactionsinfo-tab">
                    <br><br>
                    @include('admin.employee-page.user-bank-transactions')
                    <br>
                </div>
                <div class="tab-pane fade @if (session('settings')) show active @endif" id="settings" role="tabpanel"
                     aria-labelledby="settings-tab">
                    <br><br>
                    @include('admin.employee-page.user-settings')
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection

