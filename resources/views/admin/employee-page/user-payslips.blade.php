<div class="container-fluid">
    <div class="row justify-content-around">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h2 align="middle"> Payslips
                        - @if($user) {{$user->first_name}} {{$user->insertion}} {{$user->last_name}} @endif</h2>
                </div>

                <div class="card-body">

                    <script>$(document).ready(function () {
                            $('#usertable').DataTable();
                        });</script>
                    @if (session('deletedPS'))
                        <div class="alert alert-success">
                            {{ session('deletedPS') }}
                        </div>
                    @endif

                    @if (session('uploadedPS'))
                        <div class="alert alert-success">
                            {{ session('uploadedPS') }}
                        </div>
                    @endif

                    <table id="usertable" class="table">
                        <thead>
                        <tr style=" color: black;">
                            <th>Filename</th>
                            <th>Period</th>
                            <th>View</th>
                            <th>Download</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($payslips)
                            @foreach($payslips as $payslip)
                                <tr>
                                    <td>{{$payslip->filename}}</td>
                                    <td>@if($payslip->period)
                                            {{date('F, Y', strtotime($payslip->period))}}
                                        @else
                                            {{NULL}}
                                        @endif</td>
                                    <td>
                                        <a data-url="/admin/view-payslip/{{$payslip->id}}/{{$payslip->filename}}"
                                           class="openPDFdialog" data-toggle="modal" data-target="#modalPDF"
                                           style="color: orange"><i class="fas fa-eye"></i></a></td>
                                    <td><a href="/admin/download-payslip/{{$payslip->id}}"
                                           style="color: orange"><i class="fas fa-file-download"></i></a></td>
                                    <th><a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                                            href="/admin/softdelete-payslip/{{$payslip->id}}"
                                           style="color: orange"><i class="fas fa-trash-alt"></i></a></th>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="card">
                <div class="card-header">
                    <h3 align="middle">File Upload</h3>

                </div>

                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div style="margin-left: 2%">
                        <form action="/admin/singleupload" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{--input type hidden, zodat het alleen bij de desbetreffende employee upload--}}
                            <input type="hidden" name="user_id" @if($user) value="{{$user->id}}" class="form-control"
                                   placeholder="{{$user->id}}" @endif><br>
                            <label for="File Name">Date<br></label>
                            <input type="month" id="File Name" class="form-control" name="period" placeholder="yyyy-mm" required/>
                            <br/>
                            <input type="file" class="form-control-file" accept="application/pdf" name="documents[]"/>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <input type="hidden" name="company_id" value="{{$auth->company_id}}" class="form-control validate">
                            </div>
                            <br/>
                            <input type="submit" class="btn btn-deep-orange" value="Upload"/> <br><br>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- END MULTIPLE FILE UPLOAD-->
    </div>
</div>

<!--Modal: PDFViewe-->
<div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full-height modal-left modal-lg" role="document">


        <!--Content-->

        <div class="modal-content">
            <div class="modal-body mb-0 p-0">
                <div class="embed-responsive embed-responsive-1by1 z-depth-1-half">
                    <iframe id="href" class="embed-responsive-item" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on("click", ".openPDFdialog", function () {
        var url = $(this).data('url');
        $("#modalPDF iframe").attr("src", url);
    });
</script>
