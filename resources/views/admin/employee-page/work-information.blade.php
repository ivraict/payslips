<div class="container">
    <h2 align="middle"> Work Information
    - @if($user) {{$user->first_name}} {{$user->insertion}} {{$user->last_name}} @endif</h2>
    <div class="card-body">
        <br>
        @if (session('workinfo'))
            <div class="alert alert-success">
                {{ session('workinfo') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action="{{action('EmployeeController@update_work', $user->id)}}">

            {{method_field('PATCH')}}
            {{csrf_field()}}
            <input type="hidden" name="id" id="user_id">
            <div class="form-row">

                <div class="form-group col-md-4">
                    <label for="inputProfession">Profession</label>
                    <select id="inputProfession" class="form-control" name="profession">
                        @empty($user->profession_id)
                            <option selected="selected" value="">
                            </option>
                            @if ($professions)
                                @foreach ($professions as $profession)
                                    <option value="{{$profession->id}}">
                                        {{$profession->name}}
                                    </option>
                                @endforeach
                            @endif
                        @else
                            @if($user->profession)
                                <option selected="selected" value="">
                                </option>
                                <option selected="selected" value="{{$user->profession->id}}">
                                    {{$user->profession->name}}
                                </option>
                            @endif
                            @if ($professions)
                                @foreach ($professions as $profession)
                                    @if ($user->profession)
                                        @if ($profession->id !== $user->profession->id)
                                            <option value="{{$profession->id}}">
                                                {{$profession->name}}
                                            </option>
                                        @endif
                                    @else
                                        <option value="{{$profession->id}}">
                                            {{$profession->name}}
                                        </option>
                                    @endif
                                @endforeach
                            @endif
                        @endempty
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmployed">Employed Since</label>
                    <input type="date" class="form-control" name="employed_since" id="inputEmployed"
                           value="{{$user->employed_since}}">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputExternal">External ID</label>
                    <input type="text" class="form-control" name="external_id" id="inputExternal"
                           value="{{$user->external_id}}">
                </div>

            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="inputWorkdays">Days Worked</label>
                    <input type="text" class="form-control" name="work_days" id="inputWorkdays"
                           value="{{$user->work_days}}"
                    >
                </div>
                <div class="form-group col-md-4">
                    <label for="iputMinimumwage">Minimum Wage</label>
                    <input type="text" class="form-control" name="minimum_wage" id="inputMinimumwage"
                           value="{{$user->minimum_wage}}">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputWageperhour">Wage per Hour</label>
                    <input type="text" class="form-control" name="wage_per_hour" id="inputWageperhour"
                           value="{{$user->wage_per_hour}}">
                </div>
                <div class="form-group col-md-2">
                    <label for="inputPayrolltaxcredit">Payrolltaxcredit</label>
                    <select class="form-control" name="payroll_tax_credit" id="inputPayrolltaxcredit">

                        <option value="1">Yes</option>
                        <option value="0">No</option>

                    </select>

                </div>
            </div>
            <button type="submit" class="btn btn-deep-orange" id="SaveButton">Save Changes</button>
        </form>
    </div>
</div>
