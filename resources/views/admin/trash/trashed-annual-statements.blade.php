<div class="card">
    <div class="card-header">
        <h2 align="middle"> Annual Statements </h2>
    </div>
    <div class="card-body">

        @if (session('warningAS'))
        <div class="alert alert-warning"><i class="fas fa-exclamation-triangle"></i>
            {{ session('warningAS') }}
        </div>
        @endif

        @if(session('permadeletedAS'))
        <div class="alert alert-info">
            <ul>
                @foreach ((session('permadeletedAS')) as $delet)
                <li>{{$delet}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(session('restoredAS'))
        <div class="alert alert-info">
            <ul>
                @foreach ((session('restoredAS')) as $restore)
                <li>{{$restore}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{url('/admin/multiPermaDeleteOrRestore/annualstatements')}}" method="post">
            @csrf
            <script>
                $(document).ready(function() {
                    $('#usertable2').DataTable();
                });
            </script>
            <div class="table-responsive">
                <table id="usertable2" class="table" border="0">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Filename</th>
                            <th>Year</th>
                            {{-- <th>Restore</th>
                    @if(auth()->user()->can('perma delete'))
                        <th>Perma-Delete</th>
                        @endif --}}
                            {{--<th><input type="checkbox" id="select_all_users" /> Select all</th>--}}
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        @if($trashedannualstatements)
                        @foreach($trashedannualstatements as $trashedannualstatement)

                        <tr>
                            <td>
                                @if($trashedannualstatement->user)
                                    {{$trashedannualstatement->user->first_name . " " . $trashedannualstatement->user->insertion . " " . $trashedannualstatement->user->last_name}}
                                    @endif
                            </td>
                            <td>{{$trashedannualstatement->filename}}</td>
                            <td>{{$trashedannualstatement->year}}</td>
                            {{--RECOVER PAYSLIP --}}
                            {{-- <td><a href="restore-annual-statement/{{$trashedannualstatement->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td> --}}
                            {{-- <td>
                        @if(auth()->user()->can('perma delete'))
                            <a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-annual-statement/{{$trashedannualstatement->id}}"
                            style="color: orange"><i class="fas fa-trash-alt"></i></a>
                            @else
                            <a data-toggle="tooltip" data-placement="right" title="You do not have this permission!">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            @endif
                            </td> --}}
                            <td><input type="checkbox" class="checkbox" name="annualstatements[]" value="{{$trashedannualstatement->id}}">
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                @if(auth()->user()->can('perma delete'))
                    <input type="submit" class="btn btn-danger" name="permadelete" value="Delete selected" id="btnMultiDelete"
                    onclick="return confirm('Are you sure you want to perma-delete the selected records?')">
                    @endif
                    <input type="submit" class="btn btn-info" name="restore" value="Restore selected" id="btnMultiRestore"
                    onclick="return confirm('Are you sure you want to restore the selected records?')">
        </form>
    </div>
</div>
</div>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
