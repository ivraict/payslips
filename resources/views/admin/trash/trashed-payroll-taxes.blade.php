<div class="card">
    <div class="card-header">
        <h2 align="middle"> Payroll Taxes </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <script>
                $(document).ready(function() {
                    $('#usertable3').DataTable();
                });
            </script>
            <table id="usertable3" class="table" border="0">
                <thead>
                    <tr>
                        <th>Filename</th>
                        <th>Period</th>
                        <th>Restore</th>
                        @if(auth()->user()->can('perma delete'))
                            <th>Perma Delete</th>
                            @endif
                    </tr>
                </thead>
                <tbody>
                    @if($trashedpayrolltaxes)
                    @foreach($trashedpayrolltaxes as $trashedpayrolltax)

                    <tr>
                        <td>{{$trashedpayrolltax->filename}}</td>
                        <td>
                            @if($trashedpayrolltax->period)
                                {{date('F, Y', strtotime($trashedpayrolltax->period))}}
                                @else
                                {{NULL}}
                                @endif</td>
                                {{--RECOVER PAYSLIP --}}
                        <td><a href="restore-payroll-tax/{{$trashedpayrolltax->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td>
                        <td>
                            @if(auth()->user()->can('perma delete'))
                                <a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-payroll-tax/{{$trashedpayrolltax->id}}" style="color: orange"><i
                                      class="fas fa-trash-alt"></i></a>
                                @else
                                <a data-toggle="tooltip" data-placement="right" title="You do not have this permission!">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                @endif
                        </td>
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
