@if (session('succes'))
<div class="alert alert-success">
    {{ session('succes') }}
</div>
@endif

<div class="card">
    <div class="card-header">
        <h2 align="middle"> Employees </h2>
    </div>
    <div class="card-body">

        @if (session('warningUser'))
        <div class="alert alert-warning"><i class="fas fa-exclamation-triangle"></i>
            {{ session('warningUser') }}
        </div>
        @endif

        @if(session('permadeletedUser'))
        <div class="alert alert-info">
            <ul>
                @foreach ((session('permadeletedUser')) as $delet)
                <li>{{$delet}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(session('restoredUser'))
        <div class="alert alert-info">
            <ul>
                @foreach ((session('restoredUser')) as $restore)
                <li>{{$restore}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{url('/admin/multiPermaDeleteOrRestore/users')}}" method="post">
            @csrf
            <script>
                $(document).ready(function() {
                    $('#usertable').DataTable();
                });
            </script>
            <div class="table-responsive">
                <table id="usertable" class="table" border="0">
                    <thead>
                        <tr>
                            <th>First name</th>
                            <th>Insertion</th>
                            <th>Last name</th>
                            <th>E-mail</th>
                            {{-- <th>Restore</th>
                                @if(auth()->user()->can('perma delete'))
                                    <th>Perma-Delete</th>
                                    @endif --}}
                            {{--<th><input type="checkbox" id="select_all_users" /> Select all</th>--}}
                            <th>Select</th>

                        </tr>
                    </thead>
                    <tbody>
                        @if($trashedusers)
                        @foreach($trashedusers as $trasheduser)
                        <tr>
                            <td>{{$trasheduser->first_name}}</td>
                            <td>{{$trasheduser->insertion}}</td>
                            <td>{{$trasheduser->last_name}}</td>
                            <td>{{$trasheduser->email}}</td>
                            {{-- <td><a href="restore-user/{{$trasheduser->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td>
                            <td>
                                @if(auth()->user()->can('perma delete'))
                                    <a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-user/{{$trasheduser->id}}" style="color: orange"><i
                                          class="fas fa-trash-alt"></i></a>
                                    @else
                                    <a data-toggle="tooltip" data-placement="right" title="You do not have this permission!">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                    @endif
                            </td> --}}
                            <td><input type="checkbox" class="checkbox" name="users[]" value="{{$trasheduser->id}}">
                            </td>
                        </tr>

                        @endforeach
                        @endif
                    </tbody>
                </table>
                @if(auth()->user()->can('perma delete'))
                    <input type="submit" class="btn btn-danger" name="permadelete" value="Delete selected" id="btnMultiDelete"
                    onclick="return confirm('Are you sure you want to perma-delete the selected records?')">
                    @endif
                    <input type="submit" class="btn btn-info" name="restore" value="Restore selected" id="btnMultiRestore"
                    onclick="return confirm('Are you sure you want to restore the selected records?')">
        </form>

    </div>
</div>
</div>

<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })


    // $(document).ready(function(){
    //     $('#select_all_users').on('click',function(){
    //         if(this.checked){
    //             $('.checkbox').each(function(){
    //                 this.checked = true;
    //                 $('#btnMultiDeleteUsers').attr("disabled", false);
    //             });
    //         }else{
    //             $('.checkbox').each(function(){
    //                 this.checked = false;
    //                 $('#btnMultiDeleteUsers').attr("disabled", true);
    //             });
    //         }
    //     });
</script>
