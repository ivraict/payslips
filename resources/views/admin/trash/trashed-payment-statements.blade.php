<div class="card">
    <div class="card-header">
        <h2 align="middle"> Payment Statements </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive sm">
            <script>
                $(document).ready(function() {
                    $('#usertable4').DataTable();
                });
            </script>
            <table id="usertable4" class="table" border="0">
                <thead>
                    <tr>
                        <th style="width: 25%">Filename</th>
                        <th>Period</th>
                        <th>Restore</th>
                        @if(auth()->user()->can('perma delete'))
                            <th>Perma Delete</th>
                            @endif
                    </tr>
                </thead>
                <tbody>
                    @if($trashedpaymentstatements)
                    @foreach($trashedpaymentstatements as $trashedpaymentstatement)

                    <tr>
                        <td>{{$trashedpaymentstatement->filename}}</td>
                        <td>
                            @if($trashedpaymentstatement->period)
                                {{date('F, Y', strtotime($trashedpaymentstatement->period))}}
                                @else
                                {{NULL}}
                                @endif</td>
                                {{--RECOVER PAYSLIP --}}
                        <td><a href="restore-payment-statement/{{$trashedpaymentstatement->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td>
                        <td>
                            @if(auth()->user()->can('perma delete'))
                                <a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-payment-statement/{{$trashedpaymentstatement->id}}"
                                  style="color: orange"><i class="fas fa-trash-alt"></i></a>
                                @else
                                <a data-toggle="tooltip" data-placement="right" title="You do not have this permission!">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                @endif
                        </td>
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
