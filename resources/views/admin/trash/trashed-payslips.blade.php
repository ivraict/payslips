@if (session('succes'))
<div class="alert alert-success">
    {{ session('succes') }}
</div>
@endif


<div class="card">
    <div class="card-header">
        <h2 align="middle"> Payslips </h2>
    </div>
    <div class="card-body">

      @if (session('warningPS'))
          <div class="alert alert-warning"><i class="fas fa-exclamation-triangle"></i>
              {{ session('warningPS') }}
          </div>
      @endif

      @if(session('permadeletedPS'))
          <div class="alert alert-info">
              <ul>
                  @foreach ((session('permadeletedPS')) as $delet)
                      <li>{{$delet}}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      @if(session('restoredPS'))
          <div class="alert alert-info">
              <ul>
                  @foreach ((session('restoredPS')) as $restore)
                      <li>{{$restore}}</li>
                  @endforeach
              </ul>
          </div>
      @endif
        <form action="{{url('/admin/multiPermaDeleteOrRestore/payslips')}}" method="post">
            @csrf
            <script>
                $(document).ready(function() {
                    $('#usertable1').DataTable();
                });
            </script>
            <div class="table-responsive">
                <table id="usertable1" class="table" border="0">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Filename</th>
                            <th>Period</th>
                            {{-- <th>Restore</th>
                            @if(auth()->user()->can('perma delete'))
                                <th>Perma-Delete</th>
                                @endif --}}
                            <th></th>
                            {{-- <th><input type="checkbox" id="select_all" /> Select all</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @if($trashedpayslips)
                        @foreach($trashedpayslips as $trashedpayslip)

                        <tr>
                            <td>
                                @if($trashedpayslip->user)
                                    {{$trashedpayslip->user->first_name . " " . $trashedpayslip->user->insertion . " ". $trashedpayslip->user->last_name}}
                                    @endif
                            </td>
                            <td>{{$trashedpayslip->filename}}</td>
                            <td>
                                @if($trashedpayslip->period)
                                    {{date('F, Y', strtotime($trashedpayslip->period))}}
                                    @else
                                    {{NULL}}
                                    @endif</td>
                                    {{--RECOVER PAYSLIP --}}
                                    {{-- <td><a href="restore-payslip/{{$trashedpayslip->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a>
                            </td>
                            <td>
                                @if(auth()->user()->can('perma delete'))
                                    <a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-payslip/{{$trashedpayslip->id}}" style="color: orange"><i
                                          class="fas fa-trash-alt"></i></a>
                                    @else
                                    <a data-toggle="tooltip" data-placement="right" title="You do not have this permission!">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                    @endif
                            </td> --}}
                            <td><input type="checkbox" class="checkbox" name="payslips[]" value="{{$trashedpayslip->id}}">
                            </td>
                        </tr>

                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>



            @if(auth()->user()->can('perma delete'))
                <input type="submit" class="btn btn-danger" name="permadelete" value="Delete selected" id="btnMultiDelete"
                onclick="return confirm('Are you sure you want to perma-delete the selected records?')">
                @endif
                <input type="submit" class="btn btn-info" name="restore" value="Restore selected" id="btnMultiRestore"
                onclick="return confirm('Are you sure you want to restore the selected records?')">
        </form>
    </div>
</div>
<script>
    $(function() {
    $('[data-toggle="tooltip"]').tooltip()
    })

    // function check() {
    //     var atLeastOneIsChecked = $('input[name="payslips[]"]:checked').length > 0;
    //     // var $boxes = $('input[name="payslips[]"]:checked');
    //     if (atLeastOneIsChecked) {
    //         $('#btnMultiDelete').attr("disabled", false);
    //     } else {
    //         $('#btnMultiDelete').attr("disabled", true);
    //     }
    // }
    //
    // $(document).ready(function(){
    //     $('#select_all').on('click',function(){
    //         if(this.checked){
    //             $('.checkbox').each(function(){
    //                 this.checked = true;
    //                 $('#btnMultiDelete').attr("disabled", false);
    //             });
    //         }else{
    //             $('.checkbox').each(function(){
    //                 this.checked = false;
    //                 $('#btnMultiDelete').attr("disabled", true);
    //             });
    //         }
    //     });

    // $('.checkbox').on('click',function(){
    //     if($('.checkbox:checked').length == $('.checkbox').length){
    //         $('#select_all').prop('checked',true);
    //     }else{
    //         $('#select_all').prop('checked',false);
    //     }
    // });
    });
</script>
