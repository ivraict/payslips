<div class="card">
    <div class="card-header">
        <h2 align="middle"> Bank Transactions </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <script>
                $(document).ready(function() {
                    $('#usertable6').DataTable();
                });
            </script>
            <table id="usertable6" class="table" border="0">
                <thead>
                    <tr>

                        <th>Date</th>
                        <th>Name</th>
                        <th>Account Number</th>
                        <th>Contra account number</th>
                        <th>Code</th>
                        <th>Out/In</th>
                        <th>Amount</th>
                        <th>Mutation type</th>
                        <th>Communications</th>
                        <th>Restore</th>
                    </tr>
                </thead>
                <tbody>
                    @if($trashedbanktransactions)
                    @foreach($trashedbanktransactions as $trashedbanktransaction)
                    <tr>

                        <td>{{$trashedbanktransaction->date}}</td>
                        <td>{{$trashedbanktransaction->name}}</td>
                        <td>{{$trashedbanktransaction->account_number}}</td>
                        <td>{{$trashedbanktransaction->contra_account_number}}</td>
                        <td>{{$trashedbanktransaction->code}}</td>
                        <td>{{$trashedbanktransaction->out_in}}</td>
                        <td>{{$trashedbanktransaction->amount}}</td>
                        <td>{{$trashedbanktransaction->mutation_type}}</td>
                        <td>{{$trashedbanktransaction->communications}}</td>
                        <td><a href="/admin/restore-bank-transaction/{{$trashedbanktransaction->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td>

                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
