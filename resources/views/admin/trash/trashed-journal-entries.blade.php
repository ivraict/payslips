<div class="card">
    <div class="card-header">
        <h2 align="middle"> Journal Entries </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <script>
                $(document).ready(function() {
                    $('#usertable5').DataTable();
                });
            </script>
            <table id="usertable5" class="table" border="0">
                <thead>
                    <tr>
                        <th>Filename</th>
                        <th>Period</th>
                        <th>Restore</th>
                        @if(auth()->user()->can('perma delete'))
                            <th>Perma Delete</th>
                            @endif
                    </tr>
                </thead>
                <tbody>
                    @if($trashedjournalentries)
                    @foreach($trashedjournalentries as $trashedjournalentry)

                    <tr>
                        <td>{{$trashedjournalentry->filename}}</td>
                        <td>
                            @if($trashedjournalentry->period)
                                {{date('F, Y', strtotime($trashedjournalentry->period))}}
                                @else
                                {{NULL}}
                                @endif</td>
                                {{--RECOVER PAYSLIP --}}
                        <td><a href="restore-journal-entry/{{$trashedjournalentry->id}}" style="color: orange"><i class="fas fa-trash-restore-alt"></i></a></td>
                        <td>
                            @if(auth()->user()->can('perma delete'))
                                <a onclick="return confirm('Are you sure you want to permanently destroy this record? You cannot restore this record afterwards')" href="forcedelete-journal-entry/{{$trashedjournalentry->id}}" style="color: orange"><i
                                      class="fas fa-trash-alt"></i></a>
                                @else
                                <a data-toggle="tooltip" data-placement="right" title="You do not have this permission!">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                @endif
                        </td>
                    </tr>

                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
