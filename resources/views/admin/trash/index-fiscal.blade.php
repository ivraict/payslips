@extends('layouts.app-admin')

@section('content')

    {{--not used anymore--}}

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Fiscal</li>
            <li class="breadcrumb-item active" aria-current="page">Trash</li>
        </ol>
    </nav>

    <aside style="margin-left: 20%; margin-right: 20%">
        <div>
            <h1><i class="fas fa-trash-alt"></i>    Trash</h1>
            <p>Deleted fiscal records will show here.</p>
        </div>
        <hr class="featurette-divider">
        <br>

    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a href="/admin/trashed-payroll-taxes">Payroll Taxes</a>
            <span class="badge badge-primary badge-pill">{{$payrolltaxcount}}</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a href="/admin/trashed-payment-statements">Payment Statements</a>
            <span class="badge badge-primary badge-pill">{{$paymentstatementcount}}</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a href="/admin/trashed-journal-entries">Journal Entries</a>
            <span class="badge badge-primary badge-pill">{{$journalentrycount}}</span>
        </li>
    </ul>
    </aside>
@endsection
