@extends('layouts.app-admin')

@section('content')

<div class="container">
    <h1 align="middle"><i class="fas fa-trash-alt"></i> Trash</h1>
    <p align="middle">Deleted records will show here.</p>
    <hr><br>

    <div class="row">
        <div class="col-2">
            <div class="card">

                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link @if (
        session('permadeletedPS') ||
        session('restoredPS') ||
        session('warningPS') ||
        session('permadeletedAS') ||
        session('restoredAS') ||
        session('warningAS')
        )
      @else active @endif"
                    id="v-pills-employees-tab" data-toggle="pill" href="#v-pills-employees" role="tab" aria-controls="v-pills-employees" aria-selected="true">Employees <span class="badge badge-light">{{$usercount}}</span> &#8250;</a>
                    <a class="nav-link @if (
          session('permadeletedPS') ||
          session('restoredPS') ||
          session('warningPS')
          )
          active @endif"
                    id="v-pills-payslips-tab" data-toggle="pill" href="#v-pills-payslips" role="tab" aria-controls="v-pills-payslips" aria-selected="false">Payslips <span class="badge badge-light">{{$payslipcount}}</span> &#8250;</a>
                    <a class="nav-link @if (
          session('permadeletedAS') ||
          session('restoredAS') ||
          session('warningAS')
          )
          active @endif"
          id="v-pills-annualstatements-tab" data-toggle="pill" href="#v-pills-annualstatements" role="tab" aria-controls="v-pills-annualstatements" aria-selected="false">Annual Statements <span class="badge badge-light">{{$annualstatementcount}}</span> &#8250;</a>
                    <a class="nav-link"
                    id="v-pills-paymentstatements-tab" data-toggle="pill" href="#v-pills-paymentstatements" role="tab" aria-controls="v-pills-paymentstatements" aria-selected="false">Payment Statements</a>
                    <a class="nav-link" id="v-pills-journalentries-tab" data-toggle="pill" href="#v-pills-journalentries" role="tab" aria-controls="v-pills-journalentries" aria-selected="false">Journal Entries</a>
                    <a class="nav-link" id="v-pills-payrolltaxes-tab" data-toggle="pill" href="#v-pills-payrolltaxes" role="tab" aria-controls="v-pills-payrolltaxes" aria-selected="false">Payroll Taxes</a>
                    <a class="nav-link" id="v-pills-banktransactions-tab" data-toggle="pill" href="#v-pills-banktransactions" role="tab" aria-controls="v-pills-banktransactions" aria-selected="false">Bank Transactions</a>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade @if (
          session('permadeletedPS') ||
          session('restoredPS') ||
          session('warningPS') ||
          session('permadeletedAS') ||
          session('restoredAS') ||
          session('warningAS')
          )
      @else show active @endif"
                id="v-pills-employees" role="tabpanel" aria-labelledby="v-pills-employees-tab">
                @include('admin.trash.trashed-users')
            </div>
            <div class="tab-pane fade @if (
          session('permadeletedPS') ||
          session('restoredPS') ||
          session('warningPS')
          )
          show active @endif"
          id="v-pills-payslips" role="tabpanel" aria-labelledby="v-pills-payslips-tab">
            @include('admin.trash.trashed-payslips')
        </div>
        <div class="tab-pane fade @if  (
      session('permadeletedAS') ||
      session('restoredAS') ||
      session('warningAS')
      )
        show active @endif"
        id="v-pills-annualstatements" role="tabpanel" aria-labelledby="v-pills-annualstatements-tab">
        @include('admin.trash.trashed-annual-statements')
    </div>
    <div class="tab-pane fade"
    id="v-pills-paymentstatements" role="tabpanel" aria-labelledby="v-pills-paymentstatements-tab">
        @include('admin.trash.trashed-payment-statements')
    </div>
    <div class="tab-pane fade" id="v-pills-journalentries" role="tabpanel" aria-labelledby="v-pills-journalentries-tab">
        @include('admin.trash.trashed-journal-entries')
    </div>
    <div class="tab-pane fade" id="v-pills-payrolltaxes" role="tabpanel" aria-labelledby="v-pills-payrolltaxes-tab">
        @include('admin.trash.trashed-payroll-taxes')
    </div>
    <div class="tab-pane fade" id="v-pills-banktransactions" role="tabpanel" aria-labelledby="v-pills-banktransactions-tab">
        @include('admin.trash.trashed-bank-transactions')
    </div>
</div>
</div>
</div>
</div>
@endsection
