@extends('layouts.app-admin')

@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 align="middle">
                            Permissions: {{ $admin->first_name }} {{ $admin->insertion }} {{ $admin->last_name }}</h4>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('admin.viewalladmins')}}" type="button" class="btn btn-primary">&laquo;
                            Back</a><br><br>
                        @can('assign permissions')
                            @if ($auth->id !== $admin->id)
                                <form method="post"
                                      action="{{action('PermissionController@assign_permission', $admin->id)}}">
                                    @csrf
                                    @if ($permissions)
                                        <ul class="list-group">
                                            @foreach ($permissions as $permission)
                                                <li class="list-group-item">{{ $permission->name }}

                                                    <input class="float-right" type="checkbox" name="permission[]"
                                                           value="{{$permission->name}}"
                                                           @if(in_array($permission->name, $adminperminames))
                                                           checked="checked"
                                                           @endif
                                                           @if(!in_array($permission->name, $uniquepermis))
                                                           disabled
                                                            @endif
                                                    >
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    <input type="submit" class="btn btn-deep-orange" value="Save">
                                </form>
                            @else
                                <div class="alert alert-warning" role="alert">
                                    <i class="fas fa-exclamation-triangle"></i> You cannot change your own permissions.
                                </div>
                                @if ($permissions)
                                    <ul class="list-group">
                                        @foreach ($permissions as $permission)
                                            <li class="list-group-item">{{ $permission->name }}

                                                <input class="float-right" type="checkbox" name="permission[]"
                                                       value="{{$permission->name}}"
                                                       @if(in_array($permission->name, $uniquepermis))
                                                       checked="checked"
                                                       @endif
                                                       disabled
                                                >
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                                <button type="button" class="btn btn-deep-orange disabled">Save</button>
                            @endif

                        @else
                            <div class="alert alert-warning" role="alert">
                                <i class="fas fa-exclamation-triangle"></i> You do not have the permission to edit other
                                admins' permissions.
                            </div>
                            @if ($permissions)
                                <ul class="list-group">
                                    @foreach ($permissions as $permission)
                                        <li class="list-group-item">{{ $permission->name }}

                                            <input class="float-right" type="checkbox" name="permission[]"
                                                   value="{{$permission->name}}"
                                                   @if(in_array($permission->name, $adminperminames))
                                                   checked="checked"
                                                   @endif
                                                   disabled
                                            >
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            <button type="button" class="btn btn-deep-orange disabled">Save</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
