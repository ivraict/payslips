@extends('layouts.app-admin')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 align="middle">Employees</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-lg-9">
                        <a href="" class="btn btn-warning btn-deep-orange mb-4" data-toggle="modal"
                           data-target="#modalRegisterForm">
                            <i class="fas fa-user-plus"></i></a>
                    </div>

                    <!-- Button trigger modal -->
                    <div class="col">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#exampleModal">
                            Import users
                        </button>
                    </div>
                </div>

                <div class="col">
                    <div class="col-lg-9">
                        <a href="" class="btn btn-warning btn-deep-orange mb-4" data-toggle="modal"
                           data-target="#modalProfessionForm">
                            <i class="fas fa-user-plus"></i></a>
            </div>



                @if (session('succes'))
                    <div class="alert alert-success">
                        {{ session('succes') }}
                    </div>
                @endif

                @if(session('deleted'))
                    <div class="alert alert-info">
                        <ul>
                            @foreach ((session('deleted')) as $delet)
                                <li>{{$delet}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session('employees'))
                    <div class="alert alert-success">
                        <ul>
                            @foreach ((session('employees')) as $employee)
                                <li>{{$employee}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session('professions'))
                    <div class="alert alert-success">
                        <ul>
                            @foreach ((session('professions')) as $profession)
                                <li>{{$profession}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <form action="{{url('/admin/multisoftdelete/users')}}" method="post">
                    @csrf
                    <script>$(document).ready(function () {
                            $('#usertable').DataTable();
                        });</script>
                    <div class="table-responsive">
                        <table id="usertable" class="table table-hover table" border="0" align="center">
                            <thead>
                            <tr>
                                <th>External ID</th>
                                <th>First Name</th>
                                <th>Insertion</th>
                                <th>Last&nbsp;Name</th>
                                <th>E-mail</th>
                                <th>Bankaccount Number</th>
                                <th>Most recent payslip</th>
                                <th>User Profile</th>
                                <th>Delete Employee</th>
                                <th>Select</th>
                            </tr>

                            </thead>


                            <tbody>
                            @if($users)
                                @foreach($users as $user)

                                    <tr>
                                        <td>{{$user->external_id}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->insertion}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->bank_account_number}}</td>
                                        <td>
                                            @if (count($user->payslips->where('deleted_at', NULL)) > 0)
                                                @php
                                                    $latestpayslip = $user->payslips->where('deleted_at', NULL)->sortByDesc('period')->take(1)->first();
                                                @endphp
                                                @isset($latestpayslip->period)
                                                    {{date('F, Y', strtotime($latestpayslip->period)) }}
                                                @endisset
                                            @endif
                                        </td>
                                        <td><a href="/admin/show-user-information/{{$user->id}}"
                                               style="color: orange"><i
                                                    class="fas fa-eye"></i></a></td>
                                        {{--DELETE USER--}}
                                        <td>
                                            <a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                                               href="/admin/softdelete/{{$user->id}}" style="color:orange">
                                                <i class="fas fa-trash-alt"></i></a></td>
                                        <td><input type="checkbox" name="users[]" value="{{$user->id}}"
                                                   onClick="check()">
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Delete selected" id="btnMultiDelete"
                           onclick="return confirm('Are you sure you want to soft-delete the selected records?')"
                           disabled>
                </form>
            </div>
        </div>
    </div>








    <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="{{action('EmployeeController@store')}}" class="form-container">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="get">
            <input type="hidden" name="company_id" value="{{$auth->company_id}}" class="form-control validate" required>
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-body ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 align="middle" class="modal-title w-100 font-weight-bold">Add New Employee</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="md-form">
                            <div class="form-row justify-content-around">
                                <div class="form-group">
                                    First Name
                                    <input type="text" name="first_name" class="form-control validate" required>
                                </div>

                                <div class="form-group">
                                    Insertion
                                    <input type="text" name="insertion" class="form-control validate">
                                </div>

                                <div class="form-group">
                                    Last Name
                                    <input type="text" name="last_name" class="form-control validate" required>
                                </div>
                            </div>
                            <div class="form-row justify-content-around">
                                <div class="form-group">
                                    External ID
                                    <input type="number" name="external_id" class="form-control validate">
                                </div>

                                <div class="form-group">
                                    Bankaccount Number
                                    <input type="text" name="bank_account_number" class="form-control validate"
                                           required>
                                </div>

                                <div class="form-group">
                                    E-Mail
                                    <input type="email" name="email" class="form-control validate" required>
                                </div>
                            </div>

                            <div class="form-row justify-content-around">
                                <div class="form-group col-3">
                                    Date of Birth
                                    <input type="date" name="date_of_birth" class="form-control validate" >
                                </div>

                                <div class="form-group">
                                    BSN
                                    <input type="text" name="bsn" class="form-control validate" >
                                </div>
                                <div class="form-group">
                                    Place of Residence
                                    <input type="text" class="form-control" name="residence" id="Residence" >
                                </div>
                            </div>
                            <div class="form-row justify-content-around">
                                <div class="form-group">
                                    Street Name
                                    <input type="text" class="form-control" name="street_name" id="Streetname" >
                                </div>

                                <div class="form-group">
                                    House Number
                                    <input type="text" class="form-control" name="house_number" id="HouseNumber"
                                           >
                                </div>

                                <div class="form-group">
                                    Zip Code
                                    <input type="text" class="form-control" name="postal_code" id="ZipCode" >
                                </div>
                            </div>
                            <br>

                            <div class="row justify-content-center">
                                <div class="form-group">
                                    Password
                                    <input id="password" type="text" name="password" class="form-control validate"
                                           required>
                                </div>
                                <div class="col-md-3">
                                    Password strength
                                    <div class="progress progress-striped active">
                                        <div id="jak_pstrength" class="progress-bar" role="progressbar"
                                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 0%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <div class="col">
                                <label for="mailnotification">Send welcome mail</label>
                                <input id="mailnotification" type="checkbox" name="mailnotif" value="1" checked/>
                            </div>
                            <button type="submit" class="btn btn-deep-orange">Submit</button>
                            <button type="button" class="btn btn-deep-orange" href="#" onClick="autoFill()">Generate
                                password
                            </button>

                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>

{{--PROFESSION CREATE FUNCTION --}}

    <div class="modal fade" id="modalProfessionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="{{action('ProfessionController@store')}}" class="form-container">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="get">
            <input type="hidden" name="company_id" value="{{$auth->company_id}}" class="form-control validate" required>
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-body ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 align="middle" class="modal-title w-100 font-weight-bold">Add New Profession</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="md-form">
                            <div class="form-row justify-content-around">
                                <div class="form-group">
                                    Name
                                    <input type="text" name="name" class="form-control validate" required>
                                </div>


                                </div>
                            </div>
                        </div>
                        <div class="text-center">

                            <button type="submit" class="btn btn-deep-orange">Submit</button>


                        </div>
                    </div>

                </div>
        </form>
    </div>



    {{--PROFESSION CREATE FUNCTION END--}}


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 align="middle"> Excel user import</h3>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('admin.userimport') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control-file" required>
                            <br>
                            <button class="btn btn-success">Import Data</button>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        function autoFill() {
            document.getElementById('password').value = makeid(8);
        }

        function passwordStrength(password) {

            var desc = [{'width': '0px'}, {'width': '20%'}, {'width': '40%'}, {'width': '60%'}, {'width': '80%'}, {'width': '100%'}];

            var descClass = ['', 'progress-bar-danger', 'progress-bar-danger', 'progress-bar-warning', 'progress-bar-success', 'progress-bar-success'];

            var score = 0;

            //if password bigger than 6 give 1 point
            if (password.length > 6) score++;

            //if password has both lower and uppercase characters give 1 point
            if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

            //if password has at least one number give 1 point
            if (password.match(/d+/)) score++;

            //if password has at least one special caracther give 1 point
            if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) score++;

            //if password bigger than 12 give another 1 point
            if (password.length > 10) score++;

            // display indicator
            $("#jak_pstrength").removeClass(descClass[score - 1]).addClass(descClass[score]).css(desc[score]);
        }

        jQuery(document).ready(function () {
            // jQuery("#password").focus();
            jQuery("#password").keyup(function () {
                passwordStrength(jQuery(this).val());
            });
        });
    </script>
    <script>
        function check() {
            var atLeastOneIsChecked = $('input[name="users[]"]:checked').length > 0;
            // var $boxes = $('input[name="payslips[]"]:checked');
            if (atLeastOneIsChecked) {
                $('#btnMultiDelete').attr("disabled", false);
            } else {
                $('#btnMultiDelete').attr("disabled", true);
            }
        }
    </script>
    <script>
        @if (count($errors) > 0)
        $('#modalRegisterForm').modal('show');
        @endif
    </script>

    <script>
        @if (count($errors) > 0)
        $('#modalProfessionForm').modal('show');
        @endif
    </script>
    </div>

@endsection
