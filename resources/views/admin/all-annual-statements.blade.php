@extends('layouts.app-admin')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-around">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h2 align="middle">Annual Statements</h2>
                    </div>

                    <div class="card-body">
                        @if(session('uploaded'))
                            <div class="alert alert-success">
                                <ul>
                                    @foreach ((session('uploaded')) as $upload)
                                        <li>{{$upload}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session('deleted'))
                            <div class="alert alert-info">
                                <ul>
                                    @foreach ((session('deleted')) as $delet)
                                        <li>{{$delet}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{url('/admin/multisoftdelete/annualstatements')}}" method="post">
                            @csrf
                            <script>$(document).ready(function () {
                                    $('#usertable').DataTable();
                                });</script>
                            <div class="table-responsive">
                                <table id="usertable" class="table">
                                    <thead>
                                    <tr>
                                        <th>Employee</th>
                                        <th>Filename</th>
                                        <th>Year</th>
                                        <th>View</th>
                                        <th>Download</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>Select</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($annualstatements)
                                        @foreach($annualstatements as $annualstatement)
                                            <tr>
                                                <td>@if($annualstatement->user)
                                                        {{$annualstatement->user->first_name . " " . $annualstatement->user->insertion . " " . $annualstatement->user->last_name}}
                                                    @endif</td>
                                                <td>{{$annualstatement->filename}}</td>
                                                <td>{{$annualstatement->year}}</td>
                                                <td>
                                                    <a data-url="/admin/view-annual-statement/{{$annualstatement->id}}/{{$annualstatement->filename}}"
                                                       class="openPDFdialog" data-toggle="modal" data-target="#modalPDF"
                                                       style="color: orange"><i class="fas fa-eye"></i></a></td>
                                                <td><a href="/admin/download-annual-statement/{{$annualstatement->id}}"
                                                       style="color: orange"><i class="fas fa-file-download"></i></a>
                                                </td>
                                                <td>
                                                    <a href="" data-id="{{$annualstatement->id}}"
                                                       data-currentuserid="{{$annualstatement->user_id}}"
                                                       data-currentusername="@if($annualstatement->user)
                                                       {{$annualstatement->user->first_name . " " . $annualstatement->user->insertion . " " . $annualstatement->user->last_name}}
                                                       @endif"
                                                       data-year="{{$annualstatement->year}}" data-toggle="modal"
                                                       data-target="#modalEditAnnualStatementForm"
                                                       style="color: orange"><i
                                                            class="fas fa-edit"></i></a></td>
                                                <td>
                                                    <a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                                                       href="/admin/softdelete-annual-statement/{{$annualstatement->id}}"
                                                       style="color: orange"><i class="fas fa-trash-alt"></i></a></td>
                                                <td><input type="checkbox" name="annualstatements[]"
                                                           value="{{$annualstatement->id}}"
                                                           onClick="check()">
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <input type="submit" class="btn btn-danger" value="Delete selected" id="btnMultiDelete"
                                   onclick="return confirm('Are you sure you want to soft-delete the selected records?')"
                                   disabled>
                        </form>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 align="middle">File Upload</h3>
                    </div>

                    <div class="card-body">
                        <br>
                        <form action="/admin/upload-annualstatements" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="user_id"></label>
                            </div>
                            <input type="text" class="form-control" name="year" value={{date("Y")}}>
                            <label for="File Name">Date of the payslip <br></label><br>
                            <br/>
                            <input type="file" class="form-control-file" accept="application/pdf" name="documents[]"
                                   multiple required>
                            <br/>(can attach more than one)
                            <br/>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <input type="hidden" name="company_id" value="{{$auth->company_id}}"
                                       class="form-control validate">
                            </div>
                            <br>
                            <input type="submit" name="submit-normal" class="btn btn-deep-orange"
                                   value="Normal Upload"/> <br><br>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MULTIPLE FILE UPLOAD-->
        </div>
    </div>
    <!--Modal: PDFViewer-->
    <div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full-height modal-left modal-lg" role="document">


            <!--Content-->

            <div class="modal-content">
                <div class="modal-body mb-0 p-0">
                    <div class="embed-responsive embed-responsive-1by1 z-depth-1-half">
                        <iframe id="href" class="embed-responsive-item" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditAnnualStatementForm" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 align="middle" class="modal-title w-100 font-weight-bold">Edit Annual Statement</h4>
                    <div class="md-form">
                        <form autocomplete="off" method="post" action="/admin/updateannualstatement/"
                              id="EditAnnualStatementForm">
                            {{method_field('PATCH')}}
                            {{csrf_field()}}
                            <input type="hidden" name="id" id="annualstatement_id">
                            <div class="form-group">
                                Current Employee
                                <input type="text" class="form-control" id="currentusername" readonly="readonly">
                            </div>
                            <input type="hidden" name="user_id" id="currentuserid">
                            <div class="form-row">
                                <div class="form-group col-11">
                                    Change Employee
                                    <select style="width: 90%;" id="selectuser_id"
                                            class="js-example-theme-single form-control" name="user_id" disabled>
                                        @if($users)
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">
                                                    {{$user->first_name . " " . $user->insertion . " " . $user->last_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <br style="line-height: 15px"/>
                                    <div style="display: inline-block" class="custom-control custom-checkbox">
                                        <input id="checkboxdisable" type="checkbox" class="custom-control-input"
                                               data-toggle="tooltip" data-placement="top"
                                               title="When the Checkbox is checked the employee cannot be changed.
                                                  When the Checkbox is unchecked the employee can be changed"
                                               autocomplete="off" checked>
                                        <label class="custom-control-label checkbox-inline"
                                               for="checkboxdisable"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                Year
                                <input class="form-control" type="text" id="year" name="year" required>
                            </div>
                            <button type="submit" class="btn btn-deep-orange">Save Changes</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on("click", ".openPDFdialog", function () {
            var url = $(this).data('url');
            $("#modalPDF iframe").attr("src", url);
        });
    </script>
    <script>
        $('#modalEditAnnualStatementForm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            var currentusername = button.data('currentusername')
            var currentuserid = button.data('currentuserid')
            var year = button.data('year')

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #annualstatement_id').val(id)
            modal.find('.modal-body #currentusername').val(currentusername)
            modal.find('.modal-body #currentuserid').val(currentuserid)
            modal.find('.modal-body #year').val(year)
            $('#EditAnnualStatementForm').attr('action', '/admin/updateannualstatement/' + id);
        })
    </script>
    <script>
        $("#checkboxdisable, #modalEditAnnualStatementForm").change(function () {
            if ($('#checkboxdisable').prop('checked')) {
                $(".js-example-theme-single").prop("disabled", true);
            } else {
                $(".js-example-theme-single").prop("disabled", false);
            }
        });
    </script>
    <script>
        function check() {
            var atLeastOneIsChecked = $('input[name="annualstatements[]"]:checked').length > 0;
            // var $boxes = $('input[name="payslips[]"]:checked');
            if (atLeastOneIsChecked) {
                $('#btnMultiDelete').attr("disabled", false);
            } else {
                $('#btnMultiDelete').attr("disabled", true);
            }
        }
    </script>
    <script>
        $(".js-example-theme-single").select2({
            dropdownParent: $('#modalEditAnnualStatementForm')
        });
    </script>
    <script>$('#checkboxdisable').tooltip({boundary: 'window'})</script>
@endsection
