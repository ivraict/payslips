@extends('layouts.app-admin')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 align="middle">Admins</h2>
            </div>

            <div class="card-body">

                @if (session('succes'))
                    <div class="alert alert-success">
                        {{ session('succes') }}
                    </div>
                @endif

                @if (session('deleted'))
                    <div class="alert alert-info">
                        {{ session('deleted') }}
                    </div>
                @endif

                @if (session('warning'))
                    <div class="alert alert-warning"><i class="fas fa-exclamation-triangle"></i>
                        {{ session('warning') }}
                    </div>
                @endif

                <div class="">
                    <a href="" class="btn btn-warning btn-deep-orange mb-4" data-toggle="modal"
                       data-target="#modalRegisterForm">
                        <i class="fas fa-user-plus"></i></a>
                </div>

                <div class="container table-responsive">
                    <script>$(document).ready(function () {
                            $('#usertable').DataTable();
                        });</script>
                    <table id="usertable" class="table table-hover table" border="0" align="center">
                        <br>
                        <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>E-mail</th>
                            <th>Number of permissions</th>
                            <th>Edit/View Permissions</th>
                            <th>Delete</th>
                        </tr>

                        </thead>


                        <tbody>
                        @if($admins)
                            @foreach($admins as $admin)

                                <tr>
                                    <td>
                                        <br class="halfbr">{{$admin->first_name}} {{$admin->insertion}} {{$admin->last_name}}
                                    </td>
                                    <td><br class="halfbr">{{$admin->email}}</td>
                                    <td><br class="halfbr">{{$admin->getAllPermissions()->count()}}</td>
                                    <td><br class="halfbr"><a href="{{ url('admin/permissions/' . $admin->id) }}"
                                                              style="color: orange">
                                            <i class="fas fa-user-lock"></i></a></td>
                                    <td>
                                        @if(auth()->user()->can('perma delete'))
                                            <form action="{{ url('/admin/delete-admin', ['id' => $admin->id]) }}"
                                                  id="perma-delete" method="post">
                                                <button class="btn btn-link px-1 py-1"
                                                        onclick="return confirm('Are you sure to perma delete this admin ? You cannot restore this afterwards');"
                                                        type="submit"><span style="color: darkorange;"><i
                                                            class="fa fa-trash"></i></span></button>
                                                <input type="hidden" name="_method" value="Delete"/>
                                                {!! method_field('delete') !!}
                                                {!! csrf_field() !!}
                                            </form>
                                        @else
                                            <a data-toggle="tooltip" data-placement="right"
                                               title="You do not have this permission!">
                                                <button><i class="fas fa-trash-alt"></i></button>
                                            </a>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="{{action('AdminController@store_admin')}}"
              class="form-container">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="get">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 align="middle" class="modal-title w-100 font-weight-bold">Create Admin</h4>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(auth()->user()->can('create admins'))
                            <div class="md-form">
                                <div class="form-row justify-content-around">
                                    <div class="form-group">
                                        <div class="form-group">
                                            First Name
                                            <input type="text" name="first_name" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            Insertion
                                            <input type="text" name="insertion" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            Last Name
                                            <input type="text" name="last_name" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            E-Mail
                                            <input type="email" name="email" class="form-control validate" required>
                                        </div>

                                        <div class="form-group">
                                            Password
                                            <input id="password" type="text" name="password"
                                                   class="form-control validate" required>
                                        </div>

                                        Password strength
                                        <div class="progress progress-striped active">
                                            <div id="jak_pstrength" class="progress-bar" role="progressbar"
                                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 0%;"></div>
                                        </div>

                                        <button type="button" class="btn btn-deep-orange" href="#" onClick="autoFill()">
                                            Generate
                                            password
                                        </button>
                                        <br>
                                        <button type="submit" class="btn btn-deep-orange">Submit</button>
                                    </div>

                                    <div class="form-group col-4">
                                        <b>Permissions</b>
                                        @if(auth()->user()->can('assign permissions'))
                                            @if ($permissions)
                                                <ul class="list-group">
                                                    @foreach ($permissions as $permission)
                                                        <li class="list-group-item">{{ $permission->name }}

                                                            <input class="float-right" type="checkbox"
                                                                   name="permission[]"
                                                                   value="{{$permission->name}}"
                                                            >
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        @else
                                            <div class="alert alert-warning" role="alert">
                                                <i class="fas fa-exclamation-triangle"></i> You do not have the
                                                permission to assign permissions to other admins
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="alert alert-warning" role="alert">
                                <i class="fas fa-exclamation-triangle"></i> You do not have this permission!
                            </div>
                        @endcan

                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <script>
        function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        function autoFill() {
            document.getElementById('password').value = makeid(8);
        }

        function passwordStrength(password) {

            var desc = [{'width': '0px'}, {'width': '20%'}, {'width': '40%'}, {'width': '60%'}, {'width': '80%'}, {'width': '100%'}];

            var descClass = ['', 'progress-bar-danger', 'progress-bar-danger', 'progress-bar-warning', 'progress-bar-success', 'progress-bar-success'];

            var score = 0;

            //if password bigger than 6 give 1 point
            if (password.length > 6) score++;

            //if password has both lower and uppercase characters give 1 point
            if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

            //if password has at least one number give 1 point
            if (password.match(/d+/)) score++;

            //if password has at least one special caracther give 1 point
            if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) score++;

            //if password bigger than 12 give another 1 point
            if (password.length > 10) score++;

            // display indicator
            $("#jak_pstrength").removeClass(descClass[score - 1]).addClass(descClass[score]).css(desc[score]);
        }

        jQuery(document).ready(function () {
            // jQuery("#password").focus();
            jQuery("#password").keyup(function () {
                passwordStrength(jQuery(this).val());
            });
        });
    </script>

    <script>
        @if (count($errors) > 0)
        $('#modalRegisterForm').modal('show');
        @endif
    </script>
@endsection
