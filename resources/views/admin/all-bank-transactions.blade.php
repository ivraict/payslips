@extends('layouts.app-admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-around">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                          <h2 align="middle">Bank Transactions</h2>
                    </div>

                    @if(session('deleted'))
                        <div class="alert alert-info">
                            <ul>
                                @foreach ((session('deleted')) as $delet)
                                    <li>{{$delet}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <form action="/admin/multisoftdelete/banktransactions" method="post">
                            @csrf
                        <script>$(document).ready(function () {
                                $('#usertable').DataTable();
                            });</script>

                        <div class="table-responsive">
                            <table id="usertable" class="table-hover table" border="0" align="center">
                                <thead>
                                <tr>
                                    <th>Assigned?</th>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Account Number</th>
                                    <th>Contra account number</th>
                                    <th>Code</th>
                                    <th>Out/In</th>
                                    <th>Amount</th>
                                    <th>Mutation type</th>
                                    <th>Communications</th>
                                    <th>Delete</th>
                                    <th>Select</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if($banktransactions)
                                    @foreach($banktransactions as $banktransaction)
                                        <tr>
                                            <td>@if($banktransaction->user_id)
                                                    <i class="fas fa-check">
                                                        @else
                                                            <i class="fas fa-times">
                                                @endif

                                            </td>
                                            <td>{{$banktransaction->date}}</td>
                                            <td>{{$banktransaction->name}}</td>
                                            <td>{{$banktransaction->bank_account_number}}</td>
                                            <td>{{$banktransaction->contra_account_number}}</td>
                                            <td>{{$banktransaction->code}}</td>
                                            <td>{{$banktransaction->out_in}}</td>
                                            <td>{{$banktransaction->amount}}</td>
                                            <td>{{$banktransaction->mutation_type}}</td>
                                            <td>{{$banktransaction->communications}}</td>
                                            <td>
                                                <a onclick="return confirm('Are you sure you want to soft-delete this record?')"
                                                   href="/admin/softdelete-bank-transaction/{{$banktransaction->id}}"
                                                   style="color: orange"><i
                                                            class="fas fa-trash-alt"></i></a></td>
                                            <td><input type="checkbox" name="banktransactions[]" value="{{$banktransaction->id}}"
                                                       onClick="check()">
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                        </div>
                            <input type="submit" class="btn btn-danger" value="Delete selected" id="btnMultiDelete"
                                   disabled>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 align="middle"> Excel bank information</h3>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('admin.import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control-file" required>
                            <br>
                            <button class="btn btn-success">Import Data</button>
                        </form>
                        <form action="{{ route('admin.autoassign-banktrans') }}" method="post"
                              enctype="multipart/form-data">
                            {{method_field('PATCH')}}
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-success">Auto-Assign</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function check() {
            var atLeastOneIsChecked = $('input[name="banktransactions[]"]:checked').length > 0;
            // var $boxes = $('input[name="payslips[]"]:checked');
            if (atLeastOneIsChecked) {
                $('#btnMultiDelete').attr("disabled", false);
            } else {
                $('#btnMultiDelete').attr("disabled", true);
            }
        }
    </script>
@endsection
