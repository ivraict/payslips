<div class="container">

    <div class="card-body">
        <h5 align="center">Edit Information {{$company->name}}</h5>
        @can('edit company data')
            @if($message = Session::get('success'))
                <div class="alert-success">
                    <p>{{$message}}</p>
                </div>
            @endif
            @if (session('CompanyUpdated'))
                <div class="alert alert-success">
                    {{ session('CompanyUpdated') }}
                </div>
            @endif


            <form class="editform" method="post"
                  action="{{action('AdminController@update_company_data')}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" name="name" id="inputName"
                               value="{{$company->name}}" required>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputEmail">E-Mail</label>
                        <input type="email" class="form-control" name="email" id="inputEmail"
                               value="{{$company->email}}" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="inputAddress">Address</label>
                        <input type="text" class="form-control" name="address" id="inputAddress"
                               value="{{$company->address}}" >
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputPhonenumber1">Phone Number 1</label>
                        <input type="tel" class="form-control" name="phone_number"
                               id="inputPhonenumber1" value="{{$company->phone_number}}" >
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputPhonenumber2">Phone Number 2</label>
                        <input type="tel" class="form-control" name="phone_number2"
                               id="inputPhonenumber2" value="{{$company->phone_number2}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="inputFooter">Footer Text</label>
                        <textarea type="tet" style="height:179px" class="form-control"
                                  name="footer_text"
                                  id="inputFooter">{{$company->footer_text}}</textarea>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputFooterColor">Footer Color</label>
                        <input type="color" class="form-control" name="footer_color" id="inputName"
                               value="{{$company->footer_color}}" >

                        <label for="inputThemeColor">Theme Color</label>
                        <input type="color" class="form-control" name="theme_color" id="inputEmail"
                               value="{{$company->theme_color}}" >

                        &nbsp;
                        <button type="submit" class="btn btn-primary" id="SaveButton">Save Changes
                        </button>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="inputFacebookURL">Facebook (URL)</label>
                        <input type="text" class="form-control" name="facebook_link"
                               id="inputFacebookURL" value="{{$company->facebook_link}}">

                        <label for="inputInstagramURL">Instagram (URL)</label>
                        <input type="text" class="form-control" name="instagram_link"
                               id="inputFacebookURL" value="{{$company->instagram_link}}">

                        <label for="inputLinkedINURL">LinkedIN (URL)</label>
                        <input type="text" class="form-control" name="linkedin_link"
                               id="inputFacebookURL" value="{{$company->linkedin_link}}">
                    </div>
                </div>
            </form>
        @else
            <div class="alert alert-warning" role="alert">
                <i class="fas fa-exclamation-triangle"></i> You do not have this permission!
            </div>
        @endcan
    </div>
</div>
