@extends('layouts.settings')

@section('content')
    <a href="edit-auto-assign-settings">Back</a> </br>
    <b>Filename:</b>
    @isset($filename)
        {{$filename}} <br>
    @else - <br>
    @endisset
    <b>User ID:</b>
    @isset($AutoUserId)
            {{$AutoUserId}} <br>
    @else - <br>
    @endisset

    <b>Year String:</b>
    @isset($YearString)
        {{$YearString}} <br>
    @else - <br>
    @endisset

    <b>Month String:</b>
    @isset($MonthString)
        {{$MonthString}} <br>
    @else - <br>
    @endisset

    <b>Month:</b>
    @isset($month)
        {{$month}} <br>
    @else - <br>
    @endisset
@endsection
