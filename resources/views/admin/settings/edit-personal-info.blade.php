<div class="container">

        <div class="card-body">
            <h5 align="center" class="card-title">Personal Information</h5>
    <form>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="inputFirstname">First Name</label>
                <input type="text" class="form-control" id="inputFirstname"
                       value="{{$auth->first_name}}" disabled>
            </div>
            <div class="form-group col-md-2">
                <label for="inputInsertion">Insertion</label>
                <input type="text" class="form-control" id="inputInsertion"
                       value="{{$auth->insertion}}" disabled>
            </div>
            <div class="form-group col-md-5">
                <label for="inputLastname">Last Name</label>
                <input type="text" class="form-control" id="inputLastname"
                       value="{{$auth->last_name}}" disabled>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-8">
                <label for="inputEmail">E-Mail</label>
                <input type="email" class="form-control" id="inputEmail"
                       value="{{$auth->email}}" disabled>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="MemberRegister">Member Since</label>
                <input type="email" class="form-control" id="MemberRegister"
                       value="{{$auth->created_at}}" disabled>
            </div>
            <div class="form-group col-md-6">
            </div>
        </div>
    </form>
        </div>
    </div>

