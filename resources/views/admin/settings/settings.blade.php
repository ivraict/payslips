@extends('layouts.app-admin')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link @if (session('CompanyUpdated') || session('uploadSettings') || session('changePw') || session('error') || $errors->first('new-password')) @else active @endif"
                           id="home-tab" data-toggle="tab" href="#personalinfo" role="tab" aria-controls="personalinfo"
                           aria-selected="true">Personal Informaton</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if (session('CompanyUpdated')) active @endif" id="profile-tab"
                           data-toggle="tab" href="#companyinfo" role="tab" aria-controls="companyinfo"
                           aria-selected="false">Company Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('uploadSettings')) active @endif" id="AutoAssign-tab"
                           data-toggle="tab" href="#AutoAssign" role="tab" aria-controls="AutoAssign"
                           aria-selected="false">Auto-Assign</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (session('changePw') || session('error') || $errors->first('new-password')) active @endif"
                           id="Google2FA-tab" data-toggle="tab" href="#Google2FA" role="tab" aria-controls="Google2FA"
                           aria-selected="false">Change Password</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">

                <div
                    class="tab-pane fade @if (session('CompanyUpdated') || session('uploadSettings') || session('changePw') || session('error') || $errors->first('new-password'))  @else show active @endif"
                    id="personalinfo" role="tabpanel" aria-labelledby="personalinfo-tab">
                    @include('admin.settings.edit-personal-info')
                </div>
                <div class="tab-pane fade @if (session('CompanyUpdated')) show active @endif" id="companyinfo"
                     role="tabpanel" aria-labelledby="companyinfo-tab">
                    @include('admin.settings.edit-company-data')
                </div>
                <div class="tab-pane fade @if (session('uploadSettings')) show active @endif" id="AutoAssign"
                     role="tabpanel" aria-labelledby="AutoAssign-tab">
                    @include('admin.settings.edit-auto-assign-settings')
                </div>
                <div
                    class="tab-pane fade @if (session('changePw') || session('error') || $errors->first('new-password')) show active @endif"
                    id="Google2FA" role="tabpanel" aria-labelledby="Google2FA-tab">
                    @include('admin.settings.change-pw')
                </div>
            </div>
        </div>
    </div>

@endsection
