<div class="container">

    <div class="card-body">
        <h5 align="center">Edit Auto-Assign Settings</h5>
        @can('edit auto assign')
            @if($message = Session::get('success'))
                <div class="alert-success">
                    <p>{{$message}}</p>
                </div>
            @endif
            @if (session('uploadSettings'))
                <div class="alert alert-success">
                    {{ session('uploadSettings') }}
                </div>
            @endif


            <br>
            <text>To use the auto-assign upload functionality, you must use files that have a consistent
                filename convention. The filename must contain at least a personnel ID
                and a string which can indicate the corresponding month. <br><br>
                Take the following payslip filename convention as an example:
                <b>'Salarisspecificatie_177_322_60_0.pdf'</b>.
                <br>
                - '177' (starts from char 20 & char length = 3) indicates the personnel ID. <br>
                - '60_' (starts from char 28 & char length = 3) indicates the month June.
                The char length of the month here is 3 because '100' indicates the month October. <br>
            </text>
            <br>
            <form class="editform " method="post"
                  action="{{action('AdminController@update_auto_assign_settings')}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-row">
                    <div class="form-group col-md-7">

                        <h6><b>Configure how filenames are read</b></h6>
                        <table class="table-hover">
                            <tr>
                                <td align="left">Start reading from right to left:</td>
                                <td><input type="hidden" name="substr_start_or_end" value="0"/>
                                    <input id="startOrEnd" type="checkbox" name="substr_start_or_end"
                                           value="1"
                                           @if($company->substr_start_or_end == 1)
                                           checked="checked"
                                        @endif></td>
                            </tr>
                            <tr>
                                <td align="left">Filename Personnel ID <br> Start reading from
                                    character:
                                </td>
                                <td><input id="idStart" type="text" class="form-control"
                                           name="filename_id_start"
                                           value="{{$company->filename_id_start}}"></td>
                            </tr>
                            <tr>
                                <td align="left">Filename Personnel ID <br> Character lengths of
                                    Personnel ID:
                                </td>
                                <td><input id="idLength" type="text" class="form-control"
                                           name="filename_id_length"
                                           value="{{$company->filename_id_length}}"></td>
                            </tr>
                            <tr>
                                <td align="left">Filename Year Start <br> Start reading from character:
                                </td>
                                <td><input id="yearStart" type="text" class="form-control"
                                           name="filename_year_start"
                                           value="{{$company->filename_year_start}}"></td>
                            </tr>
                            <tr>
                                <td align="left">Filename Year Length <br> Character lengths of Year:
                                </td>
                                <td><input id="yearLength" type="text" class="form-control"
                                           name="filename_year_length"
                                           value="{{$company->filename_year_length}}"></td>
                            </tr>
                            <tr>
                                <td align="left">Filename month start <br> Start reading from character:
                                </td>
                                <td><input id="monthStart" type="text" class="form-control"
                                           name="filename_month_start"
                                           value="{{$company->filename_month_start}}"></td>
                            </tr>
                            <tr>
                                <td align="left">Filename month length <br> Character lengths of Month:
                                </td>
                                <td><input id="monthLength" type="text" class="form-control"
                                           name="filename_month_length"
                                           value="{{$company->filename_month_length}}"></td>
                            </tr>
                        </table>
                        <br><br><br>
                        <button type="button" class="btn btn-primary" href="#"
                                onClick="autoFill()">Default settings
                        </button>
                        <br>
                        <button type="submit" class="btn btn-primary" value="Save">Save Changes</button>
                    </div>

                    <div class="form-group col-md-4">
                        <h6><b>Configure how months are indicated</b></h6>
                        <table class="table-hover">
                            <tr>
                                <td align="left">January</td>
                                <td><input id="jan" type="text" class="form-control" name="jan"
                                           value="{{$company->jan}}"></td>
                            </tr>
                            <tr>
                                <td align="left">February</td>
                                <td><input id="feb" type="text" class="form-control" name="feb"
                                           value="{{$company->feb}}"></td>
                            </tr>
                            <tr>
                                <td align="left">March</td>
                                <td><input id="mar" type="text" class="form-control" name="mar"
                                           value="{{$company->mar}}"></td>
                            </tr>
                            <tr>
                                <td align="left">April</td>
                                <td><input id="apr" type="text" class="form-control" name="apr"
                                           value="{{$company->apr}}"></td>
                            </tr>
                            <tr>
                                <td align="left">May</td>
                                <td><input id="may" type="text" class="form-control" name="may"
                                           value="{{$company->may}}"></td>
                            </tr>
                            <tr>
                                <td align="left">June</td>
                                <td><input id="jun" type="text" class="form-control" name="jun"
                                           value="{{$company->jun}}"></td>
                            </tr>
                            <tr>
                                <td align="left">July</td>
                                <td><input id="jul" type="text" class="form-control" name="jul"
                                           value="{{$company->jul}}"></td>
                            </tr>
                            <tr>
                                <td align="left">August</td>
                                <td><input id="aug" type="text" class="form-control" name="aug"
                                           value="{{$company->aug}}"></td>
                            </tr>
                            <tr>
                                <td align="left">September</td>
                                <td><input id="sep" type="text" class="form-control" name="sep"
                                           value="{{$company->sep}}"></td>
                            </tr>
                            <tr>
                                <td align="left">October</td>
                                <td><input id="oct" type="text" class="form-control" name="oct"
                                           value="{{$company->oct}}"></td>
                            </tr>
                            <tr>
                                <td align="left">November</td>
                                <td><input id="nov" type="text" class="form-control" name="nov"
                                           value="{{$company->nov}}"></td>
                            </tr>
                            <tr>
                                <td align="left">December</td>
                                <td><input id="dec" type="text" class="form-control" name="dec"
                                           value="{{$company->dec}}"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
        @else
            <div class="alert alert-warning" role="alert">
                <i class="fas fa-exclamation-triangle"></i> You do not have this permission!
            </div>
        @endcan
    </div>
</div>


<script>
    function autoFill() {
        document.getElementById('startOrEnd').value = '0';
        document.getElementById('idStart').value = '20';
        document.getElementById('idLength').value = '3';
        document.getElementById('yearStart').value = '24';
        document.getElementById('yearLength').value = '4';
        document.getElementById('monthStart').value = '29';
        document.getElementById('monthLength').value = '2';

        document.getElementById('jan').value = '01';
        document.getElementById('feb').value = '02';
        document.getElementById('mar').value = '03';
        document.getElementById('apr').value = '04';
        document.getElementById('may').value = '05';
        document.getElementById('jun').value = '06';
        document.getElementById('jul').value = '07';
        document.getElementById('aug').value = '08';
        document.getElementById('sep').value = '09';
        document.getElementById('oct').value = '10';
        document.getElementById('nov').value = '11';
        document.getElementById('dec').value = '12';
    }
</script>
