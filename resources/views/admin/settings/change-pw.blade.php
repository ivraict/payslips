<div class="container">

    <div class="card-body">
        <h5 align="center">Change Password</h2>
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('changePw'))
                <div class="alert alert-success">
                    {{ session('changePw') }}
                </div>
            @endif
            <form method="GET" action="{{ route('admin.changepassword') }}">
                {{ csrf_field() }}

                @if (session('success'))
                    <script>
                        alert('{{ session('success') }}')
                    </script>
                @endif
                <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                    <label for="new-password" class="col-md-4 control-label">Current Password</label>
                    <input id="current-password" type="password" class="form-control" name="current-password" required>
                    @if ($errors->has('current-password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                    <label for="new-password" class="col-md-4 control-label">New Password</label>
                    <input id="new-password" type="password" class="form-control" name="new-password" required>

                    @if ($errors->has('new-password'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('new-password') }}</strong>
                                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="new-password-confirm" class="col-md-6 control-label">Confirm New Password</label>
                    <input id="new-password-confirm" type="password" class="form-control"
                           name="new-password_confirmation"
                           required>
                </div>
                <button type="submit" class="btn btn-deep-orange">Submit</button>
            </form>
    </div>
</div>
