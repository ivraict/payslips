
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <style type="text/css">
        <!--
        span.cls_002{font-family:Tahoma,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_002{font-family:Tahoma,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_003{font-family:Tahoma,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_003{font-family:Tahoma,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_004{font-family:Tahoma,serif;font-size:8.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_004{font-family:Tahoma,serif;font-size:8.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_005{font-family:Tahoma,serif;font-size:8.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_005{font-family:Tahoma,serif;font-size:8.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_006{font-family:Tahoma,serif;font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_006{font-family:Tahoma,serif;font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_007{font-family:Tahoma,serif;font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_007{font-family:Tahoma,serif;font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_008{font-family:Tahoma,serif;font-size:8.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_008{font-family:Tahoma,serif;font-size:8.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        -->
    </style>
    {{--<script type="text/javascript" src="323b58d8-6110-11e9-9d71-0cc47a792c0a_id_323b58d8-6110-11e9-9d71-0cc47a792c0a_files/wz_jsgraphics.js"></script>--}}
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-297px;top:0px;width:595px;height:842px;border-style:outset;overflow:hidden">
    {{--<div style="position:absolute;left:0px;top:0px">--}}
        {{--<img src="323b58d8-6110-11e9-9d71-0cc47a792c0a_id_323b58d8-6110-11e9-9d71-0cc47a792c0a_files/background1.jpg" width=595 height=842></div>--}}
    <div style="position:absolute;left:213.83px;top:7.94px" class="cls_002"><span class="cls_002">SALARISSPECIFICATIE</span></div>
    <div style="position:absolute;left:50.55px;top:51.13px" class="cls_002"><span class="cls_002">VASTE GEGEVENS</span></div>
    <div style="position:absolute;left:307.72px;top:51.13px" class="cls_002"><span class="cls_002">Afzender:</span></div>
    <div style="position:absolute;left:32.66px;top:61.92px" class="cls_003"><span class="cls_003">Werknemer</span></div>
    {{--External ID--}}
    <div style="position:absolute;left:166.88px;top:61.92px" class="cls_003"><span class="cls_003">{{$payslipdetail->external_id}}</span></div>
    {{--Company Name: Mastiek B.V. --}}
    <div style="position:absolute;left:307.72px;top:61.92px" class="cls_003"><span class="cls_003">{{$payslipdetail->company->name}}</span></div>
    <div style="position:absolute;left:32.66px;top:72.72px" class="cls_003"><span class="cls_003">Periodenummer</span></div>
    <div style="position:absolute;left:169.05px;top:72.72px" class="cls_003"><span class="cls_003">{{$payslipdetail->period_number}}</span></div>
    {{--Company Address: Willemstraat 111 1RA--}}
    <div style="position:absolute;left:307.72px;top:72.72px" class="cls_003"><span class="cls_003">{{$payslipdetail->company->address}}</span></div>
    <div style="position:absolute;left:32.66px;top:83.52px" class="cls_003"><span class="cls_003">Minimumloon</span></div>
    {{--Minimum wage--}}
    <div style="position:absolute;left:154.28px;top:83.52px" class="cls_003"><span class="cls_003">{{$payslipdetail->minimum_wage}}</span></div>
    {{--Company Postal Code: 1015 JA  Amsterdam--}}
    <div style="position:absolute;left:307.72px;top:83.52px" class="cls_003"><span class="cls_003">{{$payslipdetail->company->postal_code}}</span></div>
    <div style="position:absolute;left:32.66px;top:94.31px" class="cls_003"><span class="cls_003">Burgerservicenummer</span></div>
    {{--Burger Service Nummer BSN--}}
    <div style="position:absolute;left:137.41px;top:94.31px" class="cls_003"><span class="cls_003">{{$payslipdetail->bsn}}</span></div>
    <div style="position:absolute;left:32.66px;top:105.11px" class="cls_003"><span class="cls_003">Loonheffingskorting</span></div>
    {{--Loonheffingskorting--}}
    <div style="position:absolute;left:173.25px;top:105.24px" class="cls_004"><span class="cls_004">{{$payslipdetail->payroll_tax_credit}}</span></div>
    <div style="position:absolute;left:32.66px;top:115.91px" class="cls_003"><span class="cls_003">{{$payslipdetail->payroll_tax_tables}}</span></div>
    {{--Start date--}}
    <div style="position:absolute;left:32.66px;top:126.70px" class="cls_003"><span class="cls_003">Begindatum</span></div>
    <div style="position:absolute;left:135.79px;top:126.70px" class="cls_003"><span class="cls_003">{{$start_date}}</span></div>
    {{--End date--}}
    <div style="position:absolute;left:32.66px;top:137.50px" class="cls_003"><span class="cls_003">Einddatum</span></div>
    <div style="position:absolute;left:135.79px;top:137.50px" class="cls_003"><span class="cls_003">{{$end_date}}</span></div>
    <div style="position:absolute;left:307.72px;top:137.50px" class="cls_002"><span class="cls_002">Aan:</span></div>
    <div style="position:absolute;left:32.66px;top:148.29px" class="cls_003"><span class="cls_003">Geboortedatum</span></div>
    {{--Date of Birth--}}
    <div style="position:absolute;left:135.79px;top:148.29px" class="cls_003"><span class="cls_003">{{$payslipdetail->date_of_birth}}</span></div>
    {{--Full Name--}}
    <div style="position:absolute;left:307.72px;top:148.29px" class="cls_003">< class="cls_003">{{$payslipdetail->first_name}} {{$payslipdetail->insertion}} {{$payslipdetail->last_name}}</div>
    <div style="position:absolute;left:32.66px;top:159.09px" class="cls_003"><span class="cls_003">In dienst</span></div>
    <div style="position:absolute;left:135.79px;top:159.09px" class="cls_003"><span class="cls_003">{{$payslipdetail->employed_since}}</span></div>
    {{--Employee address--}}
    <div style="position:absolute;left:307.72px;top:159.09px" class="cls_003"><span class="cls_003">{{$payslipdetail->address}}</span></div>
    <div style="position:absolute;left:32.66px;top:169.89px" class="cls_003"><span class="cls_003">Basisl.p/u</span></div>
    {{--Basis loon--}}
    <div style="position:absolute;left:164.12px;top:169.89px" class="cls_003"><span class="cls_003">{{$payslipdetail->wage_per_hour}}</span></div>
    <div style="position:absolute;left:307.72px;top:169.89px" class="cls_003"><span class="cls_003">{{$payslipdetail->place_of_residence}}</span></div>
    <div style="position:absolute;left:32.66px;top:180.68px" class="cls_003"><span class="cls_003">Horeca medewerker</span></div>
    <div style="position:absolute;left:32.66px;top:277.85px" class="cls_002"><span class="cls_002">DAGEN</span></div>
    <div style="position:absolute;left:230.76px;top:277.85px" class="cls_002"><span class="cls_002">UREN</span></div>
    <div style="position:absolute;left:32.66px;top:288.64px" class="cls_003"><span class="cls_003">Gewerkt var</span></div>
    {{--Gewerkte dagen--}}
    <div style="position:absolute;left:133.75px;top:288.64px" class="cls_003"><span class="cls_003">1,00</span></div>
    <div style="position:absolute;left:230.76px;top:288.64px" class="cls_003"><span class="cls_003">Gewerkt var</span></div>
    {{--Gewerkte uren--}}
    <div style="position:absolute;left:331.85px;top:288.64px" class="cls_003"><span class="cls_003">3,42</span></div>
    <div style="position:absolute;left:224.87px;top:364.22px" class="cls_002"><span class="cls_002">OVERZICHT DEZE PERIODE</span></div>
    <div style="position:absolute;left:32.66px;top:375.01px" class="cls_003"><span class="cls_003">Basisloon per uur</span></div>
    <div style="position:absolute;left:449.03px;top:375.01px" class="cls_003"><span class="cls_003">24,56</span></div>
    <div style="position:absolute;left:513.98px;top:375.01px" class="cls_003"><span class="cls_003">24,56</span></div>
    <div style="position:absolute;left:32.66px;top:385.81px" class="cls_003"><span class="cls_003">Vakantie-</span><span class="cls_005">uren</span></div>
    <div style="position:absolute;left:453.95px;top:385.81px" class="cls_003"><span class="cls_003">2,56</span></div>
    <div style="position:absolute;left:518.90px;top:385.81px" class="cls_003"><span class="cls_003">2,56</span></div>
    <div style="position:absolute;left:32.66px;top:396.73px" class="cls_005"><span class="cls_005">Feestdagen</span></div>
    <div style="position:absolute;left:453.95px;top:396.61px" class="cls_003"><span class="cls_003">0,64</span></div>
    <div style="position:absolute;left:518.90px;top:396.61px" class="cls_003"><span class="cls_003">0,64</span></div>
    <div style="position:absolute;left:32.66px;top:407.50px" class="cls_006"><span class="cls_006">Kort verzuim</span></div>
    <div style="position:absolute;left:453.95px;top:407.40px" class="cls_003"><span class="cls_003">0,15</span></div>
    <div style="position:absolute;left:518.90px;top:407.40px" class="cls_003"><span class="cls_003">0,15</span></div>
    <div style="position:absolute;left:32.66px;top:418.30px" class="cls_007"><span class="cls_007">Vakantietoeslag</span></div>
    <div style="position:absolute;left:453.95px;top:418.20px" class="cls_003"><span class="cls_003">2,22</span></div>
    <div style="position:absolute;left:518.90px;top:418.20px" class="cls_003"><span class="cls_003">2,22</span></div>
    <div style="position:absolute;left:407.01px;top:428.99px" class="cls_003"><span class="cls_003">----------------------------------------</span></div>
    <div style="position:absolute;left:32.66px;top:439.79px" class="cls_003"><span class="cls_003">Heffingsloon/subtotaal</span></div>
    <div style="position:absolute;left:449.03px;top:439.79px" class="cls_003"><span class="cls_003">30,13</span></div>
    <div style="position:absolute;left:513.98px;top:439.79px" class="cls_003"><span class="cls_003">30,13</span></div>
    <div style="position:absolute;left:472.08px;top:450.59px" class="cls_003"><span class="cls_003">--------------------</span></div>
    <div style="position:absolute;left:32.66px;top:461.38px" class="cls_003"><span class="cls_003">Subtotaal</span></div>
    <div style="position:absolute;left:513.98px;top:461.38px" class="cls_003"><span class="cls_003">30,13</span></div>
    <div style="position:absolute;left:472.08px;top:472.18px" class="cls_003"><span class="cls_003">--------------------</span></div>
    <div style="position:absolute;left:32.66px;top:483.07px" class="cls_008"><span class="cls_008">Netto loon</span></div>
    <div style="position:absolute;left:513.98px;top:482.97px" class="cls_003"><span class="cls_003">30,13</span></div>
    <div style="position:absolute;left:45.86px;top:644.92px" class="cls_002"><span class="cls_002">TOTALEN T/M DEZE PERIODE</span></div>
    <div style="position:absolute;left:359.34px;top:644.92px" class="cls_002"><span class="cls_002">BETALINGEN</span></div>
    <div style="position:absolute;left:38.66px;top:655.71px" class="cls_003"><span class="cls_003">Heffingsloon tabel</span></div>
    <div style="position:absolute;left:165.20px;top:655.71px" class="cls_003"><span class="cls_003">30,13</span></div>
    <div style="position:absolute;left:224.75px;top:655.71px" class="cls_003"><span class="cls_003">Netto loon</span></div>
    <div style="position:absolute;left:366.78px;top:655.71px" class="cls_003"><span class="cls_003">NL03INGB0003999567</span></div>
    <div style="position:absolute;left:528.99px;top:655.71px" class="cls_003"><span class="cls_003">30,13</span></div>
    <div style="position:absolute;left:38.66px;top:666.51px" class="cls_003"><span class="cls_003">Inhouding Loonheffin</span></div>
    <div style="position:absolute;left:170.13px;top:666.51px" class="cls_003"><span class="cls_003">0,00</span></div>
    <div style="position:absolute;left:513.02px;top:666.51px" class="cls_003"><span class="cls_003">------------</span></div>
    <div style="position:absolute;left:38.66px;top:677.31px" class="cls_003"><span class="cls_003">Loondagen</span></div>
    <div style="position:absolute;left:170.13px;top:677.31px" class="cls_003"><span class="cls_003">1,00</span></div>
    <div style="position:absolute;left:528.99px;top:677.31px" class="cls_003"><span class="cls_003">30,13</span></div>
</div>

</body>

