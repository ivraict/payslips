@extends('layouts.app-admin')
@section('content')

    <div class="container">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">Make Annual Statement <b>(BETA)</b></div>
                <div class="card-body">
                    @if(auth()->user()->can('generate pdf'))

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="editform" method="post" action="{{url('admin/make-annual-statement/submitForm')}}">
                        @csrf

                        <div id="firstStep">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                     aria-valuemax="100"></div>
                            </div
                            <br><br><br>
                            <div>
                                <select id="selectuser" onchange="autoFill()">
                                    <option value="empty">Select</option>
                                    @if($users)
                                        @foreach($users as $user)
                                            <option value="{{$user}}">
                                                {{$user->external_id}}
                                                . {{$user->first_name}} {{$user->insertion}} {{$user->last_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>




                                <div class="form-group">
                                    <label class="control-label">External ID</label>
                                    <input id="externalid" type="text" class="form-control" name="external_id">
                                </div>


                            <div class="form-row">

                                <div class= "col">

                                    <div class="form-group">
                                        <label for="firstname" class="control-label">First Name</label>
                                        <input id="firstname" type="text" class="form-control" name="first_name">
                                    </div>

                                </div>

                                <div class="col">

                                    <div class="form-group">
                                        <label class="control-label">Insertion</label>
                                        <input id="insertion" type="text" class="form-control" name="insertion">
                                    </div>
                            </div>
                                <div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label>
                                        <input id="lastname" type="text" class="form-control" name="last_name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Date of Birth</label>
                                <input id="dateofbirth" type="date" class="form-control" name="date_of_birth">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Employed Since</label>
                                <input id="employedsince" type="date" class="form-control" name="employed_since">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Address</label>
                                <input id="address" type="text" class="form-control"
                                       name="address">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Place of Residence</label>
                                <input id="placeofresidence" type="text" class="form-control"
                                       name="place_of_residence">
                            </div>

                            <div class="form-group">
                                <label class="control-label">BSN</label>
                                <input id="bsn" type="text" class="form-control"
                                       name="bsn">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Bank account number</label>
                                <input id="bankaccountnumber" type="text" class="form-control"
                                       name="bank_account_number">
                            </div>


                            <button id="next" class="btn btn-primary">Next step</button>
                        </div>

                        <div id="secondStep" style="display: none">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                            </div>
                            <br><br><br>

                            <div class="form-group">
                                <button id="previous" class="btn btn-primary">Previous step</button>
                                <button type="submit" class="btn btn-primary">Generate</button>
                            </div>
                        </div>
                    </form>
                </div>
                @else
                    <div class="alert alert-warning" role="alert">
                        <i class="fas fa-exclamation-triangle"></i> You do not have this permission!
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        // auto fill user data

        function autoFill() {
            var e = document.getElementById("selectuser");
            var user = e.options[e.selectedIndex].value;
            console.log(user);
            if (user !== "empty") {
                var userobject = JSON.parse(user);
                if (userobject['street_name'] && userobject['house_number']) {
                    var address = userobject['street_name'] + " " + userobject['house_number'];
                } else {
                    var address = "";
                }
                if (userobject['street_name'] && userobject['house_number']) {
                    var placeofresidence = userobject['postal_code'] + " " + userobject['residence'];
                } else {
                    var placeofresidence = "";
                }
                document.getElementById('externalid').value = userobject['external_id'];
                document.getElementById('firstname').value = userobject['first_name'];
                document.getElementById('insertion').value = userobject['insertion'];
                document.getElementById('lastname').value = userobject['last_name'];
                document.getElementById('dateofbirth').value = userobject['date_of_birth'];
                document.getElementById('employedsince').value = userobject['employed_since'];
                document.getElementById('bsn').value = userobject['bsn'];
                document.getElementById('address').value = address;
                document.getElementById('placeofresidence').value = placeofresidence;
                document.getElementById('bankaccountnumber').value = userobject['bank_account_number'];

            } else {
                document.getElementById('externalid').value = "";
                document.getElementById('firstname').value = "";
                document.getElementById('insertion').value = "";
                document.getElementById('lastname').value = "";
                document.getElementById('dateofbirth').value = "";
                document.getElementById('employedsince').value = "";
                document.getElementById('bsn').value = "";
                document.getElementById('address').value = "";
                document.getElementById('placeofresidence').value = "";
                document.getElementById('bankaccountnumber').value = "";

            }
        }

        // show/hide


        $(document).ready(function () {
            // $('#firstStep').hide();

            $('#next').on('click', function (event) {
                event.preventDefault();
                $('#firstStep').hide();
                $('#secondStep').fadeIn(1000);
            });

            $('#previous').on('click', function (event) {
                event.preventDefault();
                $('#firstStep').fadeIn(1000);
                $('#secondStep').hide();
            });
        });


        function makeFilename() {
            var e = document.getElementById("selectuser");
            var user = e.options[e.selectedIndex].value;
            var userobject = JSON.parse(user);

            var part1      = 'Salarisspecificatie';
            var externalid = userobject['external_id'];
            var period = document.getElementById("period").value;
            var year = period.substr(0, 4);
            var month = period.substr(5, 6);
            console.log(year);

            return part1 + "_" + externalid + "_" + year + "_" + month;
        }

        function autoFillFilename()
        {
            document.getElementById('filename').value = makeFilename();
        }

    </script>
@endsection
