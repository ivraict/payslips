@extends('layouts.app-admin')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Generated Annual Statements
            </div>
            <div class="card-body">
                <a href="/admin/make-annual-statement">
                    <button class="btn btn-primary">
                        New
                    </button>
                </a>
                <table class="table table-striped">
                    <thead>
                    <th>Created at</th>
                    <th>Employee</th>
                    <th>Filename</th>
                    <th>Download</th>
                    </thead>
                    <tbody>
                    @foreach($annualstatementdetails as $annualstatementdetail)
                        <tr>
                            <td>{{$annualstatementdetail->created_at}}</td>
                            <td>{{$annualstatementdetail->first_name}} {{$annualstatementdetail->insertion}} {{$annualstatementdetail->last_name}}</td>
                            <td>{{$annualstatementdetail->filename}}</td>
                            <td>
                                <a href="{{action('PdfController@download_annual_statement_PDF', $annualstatementdetail->id)}}"><i class="fas fa-file-download"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
