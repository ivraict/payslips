@extends('layouts.app-admin')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Generated Payslips
            </div>
            <div class="card-body">
                <a href="/admin/make-payslip">
                    <button class="btn btn-primary">
                        New
                    </button>
                </a>
                <table class="table table-striped">
                    <thead>
                    <th>Created at</th>
                    <th>Employee</th>
                    <th>Filename</th>
                    <th>Download</th>
                    </thead>
                    <tbody>
                    @foreach($payslipdetails as $payslipdetail)
                        <tr>
                            <td>{{$payslipdetail->created_at}}</td>
                            <td>{{$payslipdetail->first_name}} {{$payslipdetail->insertion}} {{$payslipdetail->last_name}}</td>
                            <td>{{$payslipdetail->filename}}</td>
                            <td>
                                <a href="{{action('PdfController@downloadPDF', $payslipdetail->id)}}"><i class="fas fa-file-download"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
