@extends('layouts.ppolicy')
@section('content')

<div class="container">
    <h2 align="center">Privacy Policy</h2>
    <br>
    Mastiek B.V. respects the privacy of its website visitors, in particular their rights regarding the automatic processing of personal data. We have therefore formulated and implemented a policy on complete transparency with our customers regarding the processing of personal data, its purpose(s) and the possibilities to exercise your legal rights in the best possible way.<br><br>

    If you require any additional information about the protection of personal data, please visit the website of the Dutch Data Protection Authority (Autoriteit Persoonsgegevens): https://autoriteitpersoonsgegevens.nl/nl.<br><br>

    Until you accept the use of cookies and other tracking devices, we will not place any non-anonymised analytical cookies and / or tracking cookies on your computer, mobile phone or tablet.<br>
    With the continued visit of this website you accept these terms of use and you accept the use of cookies and other tracking systems, unless we have provided for another method of accepting cookies on our website.<br><br>

    The current available version of this privacy policy is the only version that applies while visiting our website until a new version replaces the current version.<br>
    <br><br>

    <b>Article 1 - Definitions</b>

    <br><br>
    <div >
        1.	Website (hereinafter: "Website") https://www.payslips.nl.<br><br>
        2.	Party responsible for processing personal data (hereinafter: "the controller"): Mastiek B.V., established at Stadionplein 2, 1067 <br>
        &nbsp;&nbsp;&nbsp; Amsterdam, The Netherlands, Chamber of Commerce number: 1   .

    </div>
    <br><br>

    <b>Article 2 - Access to the website</b>
    <br><br>
    <div >
        Access to and use of the website are strictly personal. You will refrain from using the data and information of this website for <br>
        your own commercial, political or advertising purposes, as well as for any commercial offers, in particular unsolicited electronic offers.
    </div>
    <br><br>
    <b>Article 3 - Website content</b>

    <br><br>
    <div >
        All brands, images, texts, comments, illustrations (animated) images, video images, sounds and all the technical applications that can be used to operate this website and more generally all the components used on this website, are protected by the laws on intellectual property. Any reproduction, repetition, use or modification, by any means whatsoever, of all or just part of it, including technical applications, without the prior written permission of the controller, is strictly prohibited. The fact that the controller may not take immediate action against any infringement, can not be considered as a tacit consent, nor of a waiver of any right to prosecute the infringing party.
    </div>
    <br><br><br><br>



    <b>Article 4 - Management of the website</b>

    <br><br>
    <div >
        For the purpose of proper management of the site, the controller may at any time:<br><br>
        <div >
            <li>suspend, interrupt, reduce or decline the access to the website for a particular category of visitors</li>
            <li>delete all information that may disrupt the functioning of the website or conflicts with national or international laws or
                &emsp;&nbsp;is contrary to internet etiquette</li>
            <li>make the website temporarily unavailable in order to perform updates</li>
        </div>
    </div>
    <br><br>



    <b>Article 5 - Responsibilities</b>
    <br><br>
    <div >
        1.	The controller is not liable for any failure, disturbances, difficulties or interruptions in the functioning of the website, causing <br>
        &emsp; the (temporary) inaccessibility of the website or of any of its functionalities. You, yourself, are responsible for the way you <br>
        &emsp;seek connection to our website. You need to take all appropriate steps to protect your equipment and data against hazards <br>
        &emsp;such as virus attacks on the Internet. Furthermore, you are responsible for which websites you visit and what information <br>
        &emsp;you seek.<br>
        2.	The controller is not liable for any legal proceedings taken against you:<br><br>
        <div >
            <li>	because of the use of the website or services accessible via the Internet</li>
            <li>	for violating the terms of this privacy policy</li><br>
        </div>
        3.	The controller is not liable for any damages that incur to you or third parties or your equipment, as a result of your <br>
        &emsp;connection to or use of the website and you will refrain from any subsequent (legal) action against the controller.<br>
        4.	If the controller is involved in a dispute because of your (ab)use of this website, he is entitled to (re)claim all subsequent <br>
        &emsp;damages from you.<br>
    </div>
    <br><br>



    <b> Article 6 - Collection of data</b>
    <br><br>
    <div>
        1.	Your personal data will be collected by Mastiek B.V. and (an) external processor(s) .<br>
        2.	Personal data means any information relating to an identified or identifiable natural person (‘data subject’).<br>
        3.	An identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such<br>
        &emsp;as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical,<br>
        &emsp;physiological, genetic, mental, economic, cultural or social identity of that natural person.<br>
        4.	The personal data that are collected on the website are used mainly by the collector in order to maintain a (commercial)<br>
        &emsp;relationship with you and if applicable in order to process your orders. They are recorded in an (electronic) register.<br>
    </div>
    <br><br>



    <b>Article 7 - Your rights regarding information</b>
    <br><br>
    <div>
        1.	Pursuant to Article 13 paragraph 2 sub b GDPR each data subject has the right to information on and access to, and <br>
        &emsp;rectification, erasure and restriction of processing of his personal data, as well as the right to object to the processing and <br>
        &emsp;the right to data portability.<br>
        2.	You can exercise these rights by contacting us at v.verberne@mastiek.work.<br>
        3.	Each request must be accompanied by a copy of a valid ID, on which you put your signature and state the address where we<br>
        &emsp;can contact you.<br>
        4.	Within one month of the submitted request, you will receive an answer from us.<br>
        5.	Depending on the complexity and the number of the requests this period may be extended to two months.<br>
    </div>
    <br><br>



    <b>Article 8 - Legal obligations</b>
    <br><br>
    <div>
        1.	In case of infringement of any law or regulation, of which a visitor is suspected and for which the authorities require the<br>
        &emsp;personal data collected by the collector, they will be provided to them after an explicit and reasoned request of those<br>
        &emsp;authorities, after which these personal data do not fall anymore under the protection of the provisions of this Privacy policy.<br>
        2.	If any information is necessary in order to obtain access to certain features of the website, the controller will indicate the<br>
        &emsp;mandatory nature of this information when requesting these data.<br>
    </div>
    <br><br>




    <b>Article 9 - Collected data and commercial offers</b>
    <br><br>
    <div>
        1.	You may receive commercial offers from the collector. If you do not wish to receive them (anymore), please send us an email<br>
        &emsp;to the following address: v.verberne@mastiek.work.<br>
        2.	Your personal data will not be used by our partners for commercial purposes.<br>
        3.	If you encounter any personal data from other data subjects while visiting our website, you are to refrain from collection, any<br>
        &emsp;unauthorized use or any other act that constitutes an infringement of the privacy of the data subject(s) in question. The<br>
        &emsp;collector is not responsible in these circumstances.<br>
    </div>
    <br><br>



    <b>  Article 10 - Data retention</b>
    <br><br>
    <div>
        The collected data are used and retained for the duration determined by law.
    </div>
    <br><br>




    <b> Article 11 - Cookies</b>
    <br><br>
    <div >
        1.	A cookie is a small text file placed on the hard drive of your electronic device upon visiting our website. A cookie contains<br>
        &emsp;data so you can be recognized as a visitor when you are visiting our website. It enables us to adjust to your needs and it<br>
        &emsp;facilitates you to log in on our website. When you visit our website, we inform you about the use of cookies. By continuing to <br>
        &emsp;use our website you accept its use, unless we ask permission by other means. Your consent is valid for a period of thirteen <br>
        &emsp;months.<br>
        2.	We use the following types of cookies on our website:<br><br>
        <div>
            - Functional cookies: like session and login cookies to collect session and login information.<br>
            - Anonymised Analytic cookies: to obtain information regarding the visits to our website, like numbers of visitors, popular<br>
            &nbsp; pages and topics. In this way we can adjust our communication and information to the needs of our visitors. We can<br>
            &nbsp; not see who visits our sites or from which personal device the visit has taken place.<br>
            - Non-anonymised Analytic cookies: to obtain information regarding the visits to our website, like the number of visitors,<br>
            &nbsp; popular pages and topics. In this way we can adjust our communication and information to the needs of our visitors.<br>
            - Tracking Cookies: like advertising cookies that are intended to show relevant advertisements. By using these cookies<br>
            &nbsp; we may deduce your personal interests. Thus (other) organisations may show you targeted advertisements when you<br>
            &nbsp; visit their website. Tracking cookies make profiling possible and treat categories of people differently when targeting<br>
            &nbsp; advertisements. Tracking cookies usually process personal data.<br><br>
        </div>
        3.	Specifically, we use the following cookies on our website:<br><br>
        <div>
            Anonymised Google Analytics (analytical cookie)<br>
            Google Analytics (analytical cookie)<br>
            Adobe (analytical cookie)<br>
            Facebook (tracking cookie)<br>
            Google Adwords (tracking cookie)<br>
        </div>
        <br>
        4.	When you visit our website, cookies from the controller and / or third parties may be installed on your equipment.<br>
        5.	For more information about using, managing and deleting cookies for each electronic device, we invite you to consult the<br>
        &emsp;following link:  https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/internet-telefoon-tv-en-post / cookies # faq<br>
    </div>
    <br><br>




    <b> Article 12 - Imagery and products offered</b>
    <br><br>
    <div>
        You cannot derive any rights from the imagery that accompanies any offered product on our website.
    </div>
    <br><br>




    <b>Article 13 - Applicable Law</b>
    <br><br>
    <div>
        These conditions are governed by Dutch law. The court in the district where the collector has its place of business has the sole jurisdiction if any dispute regarding these conditions may arise, save when a legal exception applies.
    </div>
    <br><br>



    <b> Article 14 - Contact</b>
    <br><br>
    <div>
        For questions, product information or information about the website itself, please contact: Vincent Verberne, v.verberne@mastiek.work.
    </div>

</div>
@endsection