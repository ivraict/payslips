<?php

return [
// ...

'unavailable_audits' => 'No Article Audits available',

'updated'            => [
'metadata' => 'On :audit_created_at, <b>:user_first_name :user_insertion :user_last_name</b>[:audit_ip_address] updated this record via :audit_url',
'modified' => [
'title'   => 'The Title has been modified from <strong>:old</strong> to <strong>:new</strong>',
'content' => 'The Content has been modified from <strong>:old</strong> to <strong>:new</strong>',
],
],

// ...
];
