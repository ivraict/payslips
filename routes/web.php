<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;



Route::get('/', function () {
    // IF USER ALREADY LOGGED IN, FORBID ACCESS TO LOGINFORM
    if (Auth::check()) {
        return redirect()->route('logout');
    }
    return view('auth/login');
});

Route::get('/privacypolicy', function () {
    return view('PPolicy');
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();
Route::post('/2fa', function () {

    // Check user role, WHERE HE GOES AFTER LOGGING IN
    if (auth()->user()->hasRole('owner_admin')) {
        return redirect()->route('owneradmin.index');
    } elseif (auth()->user()->hasRole('admin')) {
        return redirect()->route('admin.dashboard');
    } elseif (auth()->user()->hasRole('employee')) {
        return redirect()->route('payslips');
    } else {
        return redirect()->route('/login');
    }
})->name('2fa')->middleware('2fa');

// EMPLOYEE ROUTES
Route::group(['middleware' => ['IsEmployee', '2fa']], function () {
    Route::get('/changepw-form', 'HomeController@showChangePasswordForm');
    Route::get('/change-pw', 'HomeController@changePassword')->name('changepassword');
    Route::get('/change-pw-firsttime', 'HomeController@changePasswordFirstTime')->name('changepassword-firsttime');
    Route::get('/payslips', 'HomeController@showPayslips')->name('payslips');
    Route::get('/annual-statements', 'HomeController@showAnnualStatements')->name('annual-statements');


    Route::get('/download-payslip/{id}', 'HomeController@download_payslip')->name('download');
    Route::get('/view-payslip/{id}/{filename}', 'HomeController@pdf_viewer')->name('view-payslip');
    Route::get('/view-annualstatement/{id}/{filename}', 'HomeController@view_annualstatement')->name('view-annualstatement');
    Route::get('/download-annualstatement/{id}', 'HomeController@download_annual_statement')->name('download-annualstatement');
    Route::get('/Google2FA', 'HomeController@AuthenticateGoogle2FA')->name('employee.google2fa');;
    Route::get('/complete-authentication', 'HomeController@completeAuthentication');
    Route::get('/delete-authentication', 'HomeController@deleteAuthentication');
    Route::get('/personal-information', 'HomeController@showPersonalInformation')->name('personalinformation.view');
    Route::PATCH('/update-personal-information', 'HomeController@update')->name('employee.update');
    Route::get('/settings', 'HomeController@settings')->name('settings');
    Route::PATCH('/updatesettings', 'HomeController@update_settings');
    Route::get('/instructions', function () {
        return view('/employee/instructions');
    });
});


// with 'auth' in middleware array, sends to loginform if trying to access /admin. otherwise "Call to a member function isAdmin() on null"
Route::group(['middleware' => ['IsAdmin', 'auth', '2fa']], function () {
    Route::prefix('admin')->group(function () {
        Route::get('/log', 'AdminController@audits')->name('admin.audits');
        Route::get('/support', function () {
            return view('admin.support');
        });
        Route::get('/instructions', function () {
            return view('instructions');
        });
        Route::get('/contact', function () {
            return view('admin.contact');
        });
        // settings
        Route::prefix('settings')->group(function () {
            Route::get('/', 'AdminController@settings');
            Route::get('/personal-info', function () {
                return view('admin.settings.edit-personal-info');
            });
            Route::get('/change-pw', 'AdminController@changePassword')->name('admin.changepassword');
            Route::get('/edit-company-data/', 'AdminController@edit_company_data')->name('admin.editform-companydata');
            Route::PATCH('/update-company-data/', 'AdminController@update_company_data')->name('admin.update-companydata');
            Route::get('/edit-auto-assign-settings/', 'AdminController@edit_auto_assign_settings')->name('admin.editform-autoassign');
            Route::PATCH('/update-auto-assign-settings', 'AdminController@update_auto_assign_settings')->name('admin.update-autoassign');
            Route::post('/upload-test', 'AdminController@uploadTest');
            Route::PATCH('/update-personal-information/{id}', 'AdminController@update_personal_details')->name('admin.updatepd');
        });
        // trash
        Route::prefix('trash')->group(function () {
            Route::get('/', 'TrashController@index');
        });
        // pdf
        Route::prefix('make-payslip')->group(function () {
            Route::get('/', 'PdfController@show_make_payslip_form');
            Route::post('submitForm', 'PdfController@store');
            Route::get('/index', 'PdfController@index_pdf')->name('admin.indexpdf');
            Route::get('/downloadPDF/{id}', 'PdfController@download_annual_statement_PDF');
        });

        Route::prefix('make-annual-statement')->group(function () {
            Route::get('/', 'PdfController@show_make_annual_statement_form');
            Route::post('submitForm', 'PdfController@store_annual_statement_detail');
            Route::get('/index', 'PdfController@index_annual_statement_pdf')->name('admin.indexannualstatementpdf');
            Route::get('/downloadPDF/{id}', 'PdfController@downloadPDF');
        });

        // tickets
        Route::get('new_ticket', 'TicketsController@create');
        Route::post('new_ticket', 'TicketsController@store');
        Route::get('my_tickets', 'TicketsController@userTickets');
        Route::get('tickets/{ticket_id}', 'TicketsController@show');
        Route::post('comment', 'CommentsController@postComment');

        // CRUD users
//        Route::get('/', function () {
//            return redirect()->route('admin.viewallusers');
//        })->name('admin.dashboard'); // send to 'all-users' (route redirecting to other route)
        Route::get('/', 'EmployeeController@index')->name('admin.dashboard');
        Route::get('/all-users', 'EmployeeController@index')->name('admin.viewallusers');
        Route::PATCH('/updateuser/{id}', 'EmployeeController@update')->name('admin.update');
        Route::PATCH('/updateuserwork/{id}', 'EmployeeController@update_work')->name('admin.updatework');
        Route::get('/softdelete/{id}', 'EmployeeController@softdelete')->name('admin.softdelete');
        Route::get('/forcedelete-user/{id}', 'EmployeeController@destroy')->name('admin.forcedelete');
        Route::get('/trashed-users', 'EmployeeController@readSoftdeleted')->name('admin.readsoftdeleted');
        Route::get('/restore-user/{id}', 'EmployeeController@restore')->name('admin.restoresoftdeleted');
        Route::get('/insertuser', 'EmployeeController@store')->name('admin.insertuser');
        Route::get('/Google2FA', 'AdminController@AuthenticateGoogle2FA')->name('admin.google2fa');
        Route::get('/complete-authentication', 'AdminController@completeAuthentication');
        Route::get('/delete-authentication', 'AdminController@deleteAuthentication');
        Route::get('/show-user-information/{id}', 'EmployeeController@show_user_information')->name('admin.show-personal-information');
        Route::get('/personal-information/{id}', 'EmployeeController@showPersonalInformation')->name('admin.personalinformation');
        Route::PATCH('/update-personal-information/{id}', 'EmployeeController@update_personal_information')->name('employee.update');
        Route::get('/user-settings/{id}', 'EmployeeController@user_settings')->name('admin.user-settings');
        Route::PATCH('/update-user-settings/{id}', 'EmployeeController@update_user_settings')->name('admin.update-user-settings');
        Route::get('/all-professions', 'ProfessionController@view_all_professions')->name('admin.viewallprofessions');
        Route::PATCH('/updateprofession/{id}', 'ProfessionController@update_profession')->name('admin.updateprofession');
        Route::get('/userexport', 'ExcelController@user_export')->name('admin.userexport');
        Route::post('/userimport', 'ExcelController@user_import')->name('admin.userimport');

        Route::post('/multisoftdelete/users/', 'EmployeeController@multiDelete');
        Route::post('/multiPermaDeleteOrRestore/users/', 'EmployeeController@multiPermaDeleteOrRestore');

        // admins & permissions
        Route::get('/all-admins', 'AdminController@view_admins')->name('admin.viewalladmins');
        Route::get('/permissions/{id}', 'PermissionController@view_admin_permissions');
        Route::post('/assign-permission/{id}', 'PermissionController@assign_permission');
        Route::DELETE('/delete-admin/{id}', 'AdminController@forcedelete_admin');
        Route::get('/store-admin/', 'AdminController@store_admin');
        // CRUD payslips
        Route::get('/all-payslips', 'PayslipController@view_all_payslips')->name('admin.viewallpayslips');
        Route::get('/payslips/{id}', 'PayslipController@view_user_payslips')->name('admin.view');
        Route::post('/multiuploads', 'PayslipController@uploadAutoAssignSubmit');
        Route::post('/singleupload', 'PayslipController@uploadSingleSubmit');
        Route::get('/editpayslip/{id}', 'PayslipController@show_editform_payslip')->name('admin.editformpayslip');
        Route::PATCH('/updatepayslip/{id}', 'PayslipController@update_payslip')->name('admin.updatepayslip');
        Route::get('/softdelete-payslip/{id}', 'PayslipController@softdelete_payslip')->name('admin.softdelete-payslip');
        Route::get('/trashed-payslips', 'PayslipController@read_softdeleted_payslips')->name('admin.readsoftdeleted-payslips');
        Route::get('/restore-payslip/{id}', 'PayslipController@restore_softdeleted_payslip')->name('admin.restoresoftdeleted-payslip');
        Route::get('/forcedelete-payslip/{id}', 'PayslipController@forcedelete_payslip')->name('admin.forcedelete-payslip');
        Route::get('/download-payslip/{id}', 'PayslipController@download')->name('admin.download');
        Route::get('/view-payslip/{id}/{filename}', 'PayslipController@pdf_viewer')->name('admin.view-payslip');

        Route::post('/multisoftdelete/payslips/', 'PayslipController@multiDelete');
        Route::post('/multiPermaDeleteOrRestore/payslips/', 'PayslipController@multiPermaDeleteOrRestore');

        // CRUD Annual Statements
        Route::get('/annual-statements/{id}', 'AnnualStatementController@view_user_annuals')->name('admin.viewannuals');
        Route::post('/upload-annualstatements', 'AnnualStatementController@uploadAnnualStatement');
        Route::post('/singleupload-annual-statement', 'AnnualStatementController@uploadSingleAnnualStatement');
        Route::get('/all-annual-statements', 'AnnualStatementController@view_all_annual_statements')->name('admin.viewallannualstatements');
        Route::get('/download-annual-statement/{id}', 'AnnualStatementController@download_annual_statement')->name('admin.downloadannualstatement');
        Route::get('/view-annual-statement/{id}/{filename}', 'AnnualStatementController@view_annualstatement')->name('admin.view-annualstatement');
        Route::get('/softdelete-annual-statement/{id}', 'AnnualStatementController@softdelete_annual_statement')->name('admin.softdelete-annual-statement');
        Route::get('/editannualstatement/{id}', 'AnnualStatementController@show_editform_annual_statement')->name('admin.editannualstatement');
        Route::PATCH('/updateannualstatement/{id}', 'AnnualStatementController@update_annual_statement')->name('admin.updateannualstatement');
        Route::get('/restore-annual-statement/{id}', 'AnnualStatementController@restore_softdeleted_annual_statement')->name('admin.restoresoftdeleted-annualstatement');
        Route::get('/trashed-annual-statements', 'AnnualStatementController@read_softdeleted_annual_statements')->name('admin.readsoftdeleted-annualstatements');
        Route::get('/forcedelete-annual-statement/{id}', 'AnnualStatementController@forcedelete_annual_statement')->name('admin.forcedelete-as');

        Route::post('/multisoftdelete/annualstatements/', 'AnnualStatementController@multiDelete');
        Route::post('/multiPermaDeleteOrRestore/annualstatements/', 'AnnualStatementController@multiPermaDeleteOrRestore');
        // CRUD Fiscal

        //Payment Statements
        Route::post('/upload-paymentstatements', 'PaymentStatementController@uploadPaymentStatement');
        Route::get('/all-payment-statements', 'PaymentStatementController@view_all_payment_statements')->name('admin.viewallpaymentstatements');
        Route::get('/download-payment-statement/{id}', 'PaymentStatementController@download_payment_statement')->name('admin.downloadpaymentstatement');
        Route::get('/softdelete-payment-statement/{id}', 'PaymentStatementController@softdelete_payment_statement')->name('admin.softdelete-payment-statement');
        Route::get('/restore-payment-statement/{id}', 'PaymentStatementController@restore_softdeleted_payment_statement')->name('admin.restoresoftdeleted-paymentstatement');
        Route::get('/trashed-payment-statements', 'PaymentStatementController@read_softdeleted_payment_statements')->name('admin.readsoftdeleted-paymentstatements');
        Route::get('/forcedelete-payment-statement/{id}', 'PaymentStatementController@forcedelete_payment_statement')->name('admin.forcedelete-ps');

        //Journal Entries

        Route::post('/upload-journalentries', 'JournalEntryController@uploadJournalEntry');
        Route::get('/all-journal-entries', 'JournalEntryController@view_all_journal_entries')->name('admin.viewalljournalentries');
        Route::get('/download-journal-entry/{id}', 'JournalEntryController@download_journal_entry')->name('admin.downloadjournalentry');
        Route::get('/softdelete-journal-entry/{id}', 'JournalEntryController@softdelete_journal_entry')->name('admin.softdelete-journal-entry');
        Route::get('/restore-journal-entry/{id}', 'JournalEntryController@restore_softdeleted_journal_entry')->name('admin.restoresoftdeleted-journalentry');
        Route::get('/trashed-journal-entries', 'JournalEntryController@read_softdeleted_journal_entries')->name('admin.readsoftdeleted-journalentries');
        Route::get('/forcedelete-journal-entry/{id}', 'JournalEntryController@forcedelete_journal_entry')->name('admin.forcedelete-je');

        //Payroll Taxes
        Route::post('/upload-payrolltaxes', 'PayrollTaxController@uploadPayrollTax');
        Route::get('/all-payroll-taxes', 'PayrollTaxController@view_all_payroll_taxes')->name('admin.viewallpayrolltaxes');
        Route::get('/download-payroll-tax/{id}', 'PayrollTaxController@download_payroll_tax')->name('admin.downloadpayrolltax');
        Route::get('/softdelete-payroll-tax/{id}', 'PayrollTaxController@softdelete_payroll_tax')->name('admin.softdelete-payroll-tax');
        Route::get('/restore-payroll-tax/{id}', 'PayrollTaxController@restore_softdeleted_payroll_tax')->name('admin.restoresoftdeleted-payroll-tax');
        Route::get('/trashed-payroll-taxes', 'PayrollTaxController@read_softdeleted_payroll_taxes')->name('admin.readsoftdeleted-payrolltaxes');
        Route::get('/forcedelete-payroll-tax/{id}', 'PayrollTaxController@forcedelete_payroll_tax')->name('admin.forcedelete-pt');

        //Excel
        Route::get('/export', 'ExcelController@export')->name('admin.export');
        Route::post('/import', 'ExcelController@import')->name('admin.import');
        Route::get('/all-bank-transactions', 'ExcelController@view_all_bank_transactions')->name('admin.viewallbanktransactions');
        Route::get('/all-failed-bank-transactions', 'ExcelController@view_all_failed_bank_transactions')->name('admin.viewallfailedbanktransactions');
        Route::get('/restore-bank-transaction/{id}', 'ExcelController@restore_softdeleted_bank_transaction')->name('admin.restoresoftdeleted-banktransaction');
        Route::get('/softdelete-bank-transaction/{id}', 'ExcelController@softdelete_bank_transaction')->name('admin.softdelete-bank-transacion');
        Route::PATCH('/auto-assign-bank-transaction/', 'ExcelController@auto_assign_bank_transactions')->name('admin.autoassign-banktrans');
        Route::get('/bank-transactions/{id}', 'ExcelController@view_user_banktracs')->name('admin.viewbanktracs');

        Route::post('/multisoftdelete/banktransactions/', 'ExcelController@multiDelete');

        //Minimum Wage
        Route::get('/minimum-wages', 'MinimumWageController@index')->name('admin.viewallminimumwages');
        Route::post('/store/minimum-wages', 'MinimumWageController@store')->name('admin.storeminimumwage');
        Route::PATCH('/update/minimum-wage/{id}', 'MinimumWageController@update')->name('admin.updateminimumwage');
        Route::get('/destroy/minimum-wage/{id}', 'MinimumWageController@destroy')->name('owneradmin.destroyminimumwage');

        //Professions
        Route::get('/professions', 'ProfessionController@view_all_professions')->name('admin.viewallprofessions');
        Route::get('/store/professions', 'ProfessionController@store')->name('admin.storeprofession');
        Route::PATCH('/update/profession/{id}', 'ProfessionController@update')->name('admin.updateprofession');
        Route::get('/destroy/profession/{id}', 'ProfessionController@destroy')->name('admin.destroyprofession');

    });
});

// OWNER ROUTES, /owneradmin/...
// with 'auth' in middleware array, sends to loginform if trying to access /-megaadmin. otherwise "Call to a member function isMegaAdmin() on null"
Route::group(['middleware' => ['IsOwnerAdmin', 'auth', '2fa']], function () {
    Route::prefix('owner-admin')->group(function () {
        Route::get('/', function () {
            return redirect()->route('owneradmin.viewallcompanies');
        })->name('owneradmin.index'); // send to 'all-users' (route redirecting to other route)
        Route::get('/all-companies', 'CompanyController@view_all_companies')->name('owneradmin.viewallcompanies');
        Route::get('/log', 'OwnerAdminController@audits')->name('owneradmin.audits');
        Route::get('/log/{id}', 'OwnerAdminController@view_company_audits')->name('owneradmin.viewcompanylog');
        Route::get('/Google2FA', 'OwnerAdminController@AuthenticateGoogle2FA')->name('owneradmin.google2fa');;
        Route::get('/complete-authentication', 'OwnerAdminController@completeAuthentication');
        Route::get('/delete-authentication', 'OwnerAdminController@deleteAuthentication');
        Route::get('/change-pw', 'OwnerAdminController@changePassword')->name('owneradmin.changepassword');

        // Companies
        Route::get('/show-company-information/{id}', 'CompanyController@show_company_information')->name('owneradmin.show-company-information');
        Route::get('/insertuser', 'CompanyController@insert_admin_to_company')->name('owneradmin.insertsa');
        Route::get('/insertcompany', 'CompanyController@insert_company')->name('owneradmin.insertcompany');
        Route::get('/pp-content', 'OwnerAdminController@pp_content')->name('owneradmin.ppcontent');
        Route::get('/edit-pp/{id}', 'OwnerAdminController@show_editform_pp')->name('owneradmin.editformpp');
        Route::PATCH('/update-pp{id}', 'OwnerAdminController@update_pp')->name('owneradmin.updatepp');
        Route::get('/edituser/{id}', 'CompanyController@edit_sa')->name('owneradmin.edit');
        Route::PATCH('/updateuser/{id}', 'CompanyController@update_sa')->name('owneradmin.update');
        Route::get('/company/logo/{id}', 'CompanyController@view_company_logo');
        Route::post('/upload-logo', 'CompanyController@upload_company_logo');
        Route::post('/delete-logo', 'CompanyController@delete_company_logo');
        Route::get('/softdelete-company/{id}', 'CompanyController@softdelete_company');
        Route::get('/trashed-companies', 'CompanyController@read_softdeleted_companies')->name('owneradmin.trashed-companies');
        Route::get('/restore-company/{id}', 'CompanyController@restore_softdeleted_company');
        Route::get('/forcedelete-company/{id}', 'CompanyController@forcedelete_company');

        // tickets
        Route::get('tickets', 'TicketsController@index');
        Route::get('tickets/{ticket_id}', 'TicketsController@OwnerAdmin_show');
        Route::post('close_ticket/{ticket_id}', 'TicketsController@close');
        Route::post('comment', 'CommentsController@postComment');

        // old payslips
        Route::get('/old-payslips', 'OwnerAdminController@old_payslips')->name('owneradmin.viewoldpayslips');
        Route::get('/forcedelete-payslip/{id}', 'OwnerAdminController@forcedelete_payslip');
        Route::get('/forcedelete-old-payslips', 'OwnerAdminController@delete_all_old_payslips')->name('owneradmin.deleteoldpayslips');

        // permissions
        Route::get('/permissions', 'PermissionController@index');
        Route::get('/permissions/{id}', 'PermissionController@view_admin_permissions_as_owner_admin');
        Route::post('/assign-permission/{id}', 'PermissionController@assign_permission_as_owner_admin');

        // settings
        Route::prefix('settings')->group(function () {
            Route::get('/', 'OwnerAdminController@settings');
            Route::get('/Google2FA', 'OwnerAdminController@AuthenticateGoogle2FA')->name('owneradmin.settings.google2fa');
            Route::get('/change-pw', 'OwnerAdminController@changePassword')->name('owneradmin.changepassword');

        });

        //Minimum Wage
        Route::get('/minimum-wages', 'MinimumWageController@index')->name('owneradmin.viewallminimumwages');
        Route::post('/store/minimum-wages', 'MinimumWageController@store')->name('owneradmin.storeminimumwage');
        Route::PATCH('/update/minimum-wage/{id}', 'MinimumWageController@update')->name('owneradmin.updateminimumwage');
        Route::get('/destroy/minimum-wage/{id}', 'MinimumWageController@destroy')->name('owneradmin.destroyminimumwage');
    });
});
