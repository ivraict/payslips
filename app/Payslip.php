<?php

namespace App;

use App\Scopes\OrganisationPayslipScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Payslip extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $date = ['deleted_at'];

    protected $fillable = [
        'id', 'user_id', 'filename', 'period', 'path', 'company_id'
    ];

    protected $auditExclude = [
        'id',
        'company_id',
        'path',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationPayslipScope);
    }

    //Many to one
    public function user(){
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }
}

