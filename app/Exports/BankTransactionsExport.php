<?php

namespace App\Exports;

use App\BankTransaction;
use Maatwebsite\Excel\Concerns\FromCollection;

class BankTransactionsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return BankTransaction::all();
    }
}
