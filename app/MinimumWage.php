<?php

namespace App;

use App\Scopes\OrganisationScope;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class MinimumWage extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [
        'id'
    ];

    protected $auditExclude = [
        'law',
    ];

    //Many to one
    public function company(){
        return $this->belongsTo('App\Company')->withTrashed();
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationScope);
    }

}
