<?php

namespace App;

use App\Scopes\OrganisationUserScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements Auditable
{
    use SoftDeletes, HasRoles;
    use \OwenIt\Auditing\Auditable;

    protected $date = ['deleted_at'];

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'id','role_id', 'insertion', 'first_name', 'last_name', 'email', 'password', 'company_id', 'phone_number', 'external_id', 'account_number', 'postal_code', 'street_name', 'house_number', 'residence', 'date_of_birth'
//    ];

    protected $guarded = [
        'id'
    ];

    protected $auditExclude = [
//        'id',
        'remember_token',
        'password',
//        'company_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'account_number', 'google2fa_secret',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    // Users has many Payslips
    public function payslips(){
        return $this->hasMany('App\Payslip')->withTrashed();
    }

    // User has many Annual Statements
    public function annualstatements(){
        return $this->hasMany('App\AnnualStatement')->withTrashed();
    }

    // Belongs to company
    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function profession(){
        return $this->belongsTo('App\Profession');
    }


    public static function boot()
    {
        parent::boot();

        // add global scope to only work with users from the same company as the logged in user
        static::addGlobalScope(new OrganisationUserScope);
    }

//    public function role(){
//        return $this->belongsTo('App\Role');
//    }
//
//    // CHECK IF USER IS EMPLOYEE
//    public function isEmployee()
//    {
//        if ($this->role->name == 'employee') {
//            return true;
//        }
//        return false;
//    }
//
//    // CHECK IF USER IS ADMIN
//    public function isAdmin(){
//        if ($this->role->name == 'admin') {
//            return true;
//        }
//        return false;
//    }
//
//    // CHECK IF USER IS SUPER ADMIN
//    public function isSuperAdmin()
//    {
//        if ($this->role->name == 'super_admin') {
//            return true;
//        }
//        return false;
//    }
//
//    // CHECK IF USER IS SUPER MEGA ADMIN
//    public function isOwnerAdmin()
//    {
//        if ($this->role->name == 'owner_admin') {
//            return true;
//        }
//        return false;
//    }
//
//    // CHECK IF USER HAS PAYSLIP
//    public function EmployeeHasPayslip()
//    {
//        if ($this->id == 'super_admin') {
//            return true;
//        }
//        return false;
//    }
//
//    // CHECK IF USER HAS ANNUAL STATEMENT
//    public function EmployeeHasAnnualStatement()
//    {
//        if ($this->id == 'super_admin') {
//            return true;
//        }
//        return false;
//    }
}
