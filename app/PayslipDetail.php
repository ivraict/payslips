<?php

namespace App;

use App\Scopes\OrganisationPayslipDetailScope;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PayslipDetail extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
//    protected $fillable = ['first_name', 'insertion', 'last_name','gross_salary', 'filename', 'company_id'
//    'external_id', 'minimum_wage', 'bsn', 'start_date', 'end_date', 'date_of_birth'];

    // all columns fillable except:
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationPayslipDetailScope);
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }

}
