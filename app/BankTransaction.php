<?php

namespace App;

use App\Scopes\OrganisationBankTransactionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class BankTransaction extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $date = ['deleted_at'];

//    protected $fillable = [
//        'id', 'user_id', 'company_id', 'date', 'name', 'account_number', 'contra_account_number', 'code', 'out_in', 'amount', 'mutation_type', 'communications'
//    ];

    protected $guarded = [
        'id'
    ];

    protected $auditExclude = [
        'id',
        'company_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationBankTransactionScope);
    }

    //Many to one
    public function company(){
        return $this->belongsTo('App\Company')->withTrashed();
    }

}
