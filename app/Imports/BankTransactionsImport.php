<?php

namespace App\Imports;

use App\BankTransaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;

class BankTransactionsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!isset($row[0])) {
            return null;
        }

        if ($row[0] == 'Datum') {
            return null;
        }
        return new BankTransaction([
//            'date' => date('Y-m-d', strtotime("1899-12-30 + " . $row[0] . " days")),
//            'date' => $row[0],
//            'user_id' => User::where('account_number', $row[2])->first()->id,
            'date' => $row[0],
            'name' => $row[1],
            'bank_account_number' => $row[2],
            'code' => $row[4],
            'out_in' => $row[5],
            'amount' => $row[6],
            'mutation_type' => $row[7],
            'communications' => $row[8],
            'company_id' => Auth::user()->company_id

        ]);
    }
}
