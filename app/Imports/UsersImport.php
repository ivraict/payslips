<?php

namespace App\Imports;


use App\User;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!isset($row[0])) {
            return null;
        }

        if ($row[0] == 'Achternaam') {
            return null;
        }
        $random = str_random(8);

        $user = User::where('email', '=', $row[3])->first();
    //     if ($user === null) {
    //    $employee = new User([
    //        'password' => bcrypt($random),
    //        'temp_password' => $random,
    //        'last_name' => $row[0],
    //        'first_name' => $row[1],
    //        'initials' => $row[2],
    //        'email' => $row[3],
    //        'gender' => $row[4],
    //        'phone_number' => $row[5],
    //        'street_name' => $row[6],
    //        'house_number' => $row[7],
    //        'postal_code' => $row[8],
    //        'residence' => $row[9],
    //        'country' => $row[10],
    //        'nationality' => $row[11],
    //        'date_of_birth' => date('Y-m-d', strtotime($row[12])),
    //        'birthplace' => $row[13],
    //        'bsn' => $row[14],
    //        'status' => $row[15],
    //        'workplace' => $row[16],
    //        'bank_account_number' => $row[21],
    //        'company_id' => Auth::user()->company_id
    //    ]);
    //  }
        //   check if email exists
          $user = User::where('email', '=', $row[3])->first();
          if ($user === null) {
          $employee = new User();
          $employee->password = bcrypt($random);
          $employee->temp_password = $random;
          $employee->last_name = $row[0];
          $employee->first_name = $row[1];
          $employee->initials = $row[2];
          $employee->email = $row[3];
          $employee->gender = $row[4];
          $employee->phone_number = $row[6];
          $employee->street_name = $row[7];
          $employee->house_number = $row[8];
          $employee->postal_code = $row[9];
          $employee->residence = $row[10];
          $employee->country = $row[11];
          $employee->nationality = $row[12];
          $employee->date_of_birth = date('Y-m-d', strtotime($row[13]));
          $employee->birthplace = $row[14];
          $employee->bsn = $row[15];
          $employee->status = $row[16];
          $employee->workplace = $row[17];
        //   $employee->bank_account_number = $row[21];
          $employee->company_id = Auth::user()->company_id;

          $employee->assignRole('employee');

          return $employee;
          }

    }
}
