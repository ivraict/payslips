<?php

namespace App\Providers;

use App\Company;
use App\Payslip;
use App\PrivacyPolicy;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        schema::defaultStringLength(191);

        View::composer('*', function($view){
            $view->with('auth', Auth::user());
        });

        View::composer('*', function($view){
            $view->with('ppcontents', PrivacyPolicy::all());
        });

        View::composer('*', function($view){
            $date = date("Y-m-d", strtotime("-7 years"));
            $view->with('oldpayslipcount', Payslip::where('period', '<', $date)->get()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
