<?php

namespace App;

use App\Scopes\OrganisationJournalEntryScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class JournalEntry extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $date = ['deleted_at'];

    protected $fillable = [
        'id', 'company_id', 'filename', 'path', 'period'
    ];

    protected $auditExclude = [
        'id',
        'company_id',
        'path',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationJournalEntryScope);
    }

    //Many to one
    public function company(){
        return $this->belongsTo('App\Company')->withTrashed();
    }
}
