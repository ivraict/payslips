<?php

namespace App;

use App\Scopes\OrganisationAnnualStatementScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class AnnualStatement extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $date = ['deleted_at'];

    protected $fillable = [
        'id', 'user_id', 'filename', 'year', 'path', 'company_id'
    ];

    protected $auditExclude = [
        'id',
        'company_id',
        'path',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationAnnualStatementScope);
    }

    //Many to one
    public function user(){
        return $this->belongsTo('App\User')->withTrashed();
    }
}
