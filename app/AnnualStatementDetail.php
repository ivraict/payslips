<?php

namespace App;

use App\Scopes\OrganisationAnnualStatementDetailScope;
use Illuminate\Database\Eloquent\Model;

class AnnualStatementDetail extends Model
{
    protected $fillable = ['first_name', 'insertion', 'last_name', 'filename', 'company_id', 'external_id'];

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationAnnualStatementDetailScope);
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }

}
