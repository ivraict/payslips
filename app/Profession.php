<?php

namespace App;

use App\Scopes\OrganisationProfessionScope;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Profession extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'name'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrganisationProfessionScope);
    }

    //Many to one

    public function user(){
        return $this->hasMany('App\User')->withTrashed();
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }

}
