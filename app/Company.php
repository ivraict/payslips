<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Company extends Model implements Auditable
{
    //
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $date = ['deleted_at'];

    protected $fillable = [
        'id','name', 'address', 'email', 'phone_number', 'phone_number2'
    ];

    protected $guarded = [
        'id'
    ];

    protected $auditExclude = [
        'id',
    ];

    public function users(){
        return $this->hasMany('App\User')->withTrashed();
    }

    public function payslips(){
        return $this->hasMany('App\Payslip')->withTrashed();
    }

    public function journal_entries(){
        return $this->hasMany('App\JournalEntry')->withTrashed();
    }

    public function payment_statements(){
        return $this->hasMany('App\PaymentStatement')->withTrashed();
    }

    public function payroll_taxes(){
        return $this->hasMany('App\PayrollTax')->withTrashed();
    }

    public function bank_transactions(){
        return $this->hasMany('App\BankTransaction')->withTrashed();
    }

}
