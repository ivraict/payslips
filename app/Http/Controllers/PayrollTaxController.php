<?php

namespace App\Http\Controllers;

use App\PayrollTax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PayrollTaxController extends Controller
{
    public function view_all_payroll_taxes()
    {
        //Hiermee word er een view getoond waarin alle journaalposten worden weergeven.

        $payrolltaxes = Payrolltax::all();
        return view('admin.all-payroll-taxes', compact('payrolltaxes'));
    }

    public function download_payroll_tax($id)
    {

        //Hiermee kunnen de journaalopgaven worden gedownload.

        $payrolltax = Payrolltax::find($id);
        $path = $payrolltax->path;
        $dlpayrolltax = storage_path("app/" . $path);

        return response()->download($dlpayrolltax, $payrolltax->filename);
    }


    public function softdelete_payroll_tax($id)
    {
        $payrolltax = PayrollTax::find($id);
        PayrollTax::find($id)->delete();
        return back()->with('deletedAS', 'Payroll Tax with filename ' . $payrolltax->filename . ' has been soft-deleted');
    }

    public function restore_softdeleted_payroll_tax($id)
    {
        $trashedpayrolltax = PayrollTax::withTrashed()->where('id', $id)->first();
        $trashedpayrolltax->restore();
        return back()->withInput();
    }

    public function read_softdeleted_payroll_taxes()
    {
        $trashedpayrolltaxes = PayrollTax::onlyTrashed()->get();
        return view('admin.trash.trashed-payroll-taxes', compact('trashedpayrolltaxes'));
    }

    public function uploadPayrollTax(Request $request)
    {
        $auth = Auth::user();
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            foreach ($files as $file) {

                $yearmonth = Input::post('period');
                $companyId = Input::post('company_id');
                $period = $yearmonth . "-01";

                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    $uploaded[] = "Journal Entry with filename " . $filename . " has succesfully been uploaded";
                    $path = $document->store('payroll-taxes/' . $auth->company->id);
                    PayrollTax::create([
//                            $request->all(),
                        'company_id' => $companyId,
                        'period' => $period,
                        'filename' => $filename,
                        'path' => $path

                    ]);
                }
                return back()->with(['uploaded' => $uploaded]);
            }
        }
    }

    public function forcedelete_payroll_tax($id)
    {
        if(auth()->user()->can('perma delete')) {
            $trashedpayrolltax = PayrollTax::withTrashed()->where('id', $id)->first();

            // check if file exists in storage
            $exists = Storage::exists("app/" . $trashedpayrolltax->path);
            if ($exists) {
                // delete from storage directory
                unlink(storage_path("app/" . $trashedpayrolltax->path));
            }
            // delete record from database
            $trashedpayrolltax->forceDelete();

        return back()
            ->with('succes', 'Annual Statement with filename ' . $trashedpayrolltax->filename . ' has been perma-deleted');
        } else {
            return back();
        }
    }
}
