<?php

namespace App\Http\Controllers;

use App\Mail\Notification;
use App\Payslip;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PayslipController extends Controller
{
    public function view_user_payslips($id)
    {
        // show payslips of a certain user
        $user = User::where('id', $id)->first();
        $payslips = Payslip::where('user_id', $id)->get();

        // these variables are returned from the upload payslip session
        $uploaded = session()->get('uploaded');
        return view('admin.employee-page.user-payslips', compact('payslips', 'user', 'annualstatements', 'uploaded'));
    }

    public function view_all_payslips()
    {
        $users = User::role('employee')->get();
        $payslips = Payslip::all();

        // these variables are returned from the upload payslip session
        $succeeded = session()->get('succeeded');
        $failed = session()->get('failed');
        $uploaded = session()->get('uploaded');
        // these variables are returned from the mutlti delete payslip session
        $deleted = session()->get('deleted');
        return view('admin.all-payslips', compact('payslips', 'users', 'succeeded', 'failed', 'uploaded', 'deleted'));
    }

    public function uploadAutoAssignSubmit(Request $request)
    {
        $auth = Auth::user();

        if ($request->get('submit-auto')) {
            if ($request->hasFile('documents')) {
                $files = $request->file('documents');

                $failed = [];
                $succeeded = [];
                foreach ($files as $file) {
                    $year = Input::post('year');
                    $companyId = Input::post('company_id');

                    foreach ($request->documents as $document) {
                        $filename = $document->getClientOriginalName();
                        if ($auth->company->substr_start_or_end == 0) {
                            $AutoUserId = substr($filename, $auth->company->filename_id_start, $auth->company->filename_id_length);
                        } elseif ($auth->company->substr_start_or_end == 1) {
                            $AutoUserId = substr($filename, -$auth->company->filename_id_start, $auth->company->filename_id_length);
                        }

                        $user = User::where('external_id', $AutoUserId)->first();

                        $path = $document->store('payslips/' . $auth->company->id);

                        if (!$user) {
                            $failed[] = "Payslip with filename " . $filename . " has NOT been assigned";
                        }
                        if ($user) {
                            $succeeded[] = "Payslip with filename " . $filename . " has succesfully been assigned to " .
                                $user->first_name . " " . $user->insertion . " " . $user->last_name;
                        }


                        if (isset($auth->company->filename_month_start, $auth->company->filename_month_length)) {
                            if ($auth->company->substr_start_or_end == 0) {
                                $MonthString = substr($filename, $auth->company->filename_month_start, $auth->company->filename_month_length);
                            } elseif ($auth->company->substr_start_or_end == 1) {
                                $MonthString = substr($filename, -$auth->company->filename_month_start, $auth->company->filename_month_length);
                            }
                        }

                        if (isset($auth->company->filename_year_start, $auth->company->filename_year_length)) {
                            if ($auth->company->substr_start_or_end == 0) {
                                $YearString = substr($filename, $auth->company->filename_year_start, $auth->company->filename_year_length);
                            } elseif ($auth->company->substr_start_or_end == 1) {
                                $YearString = substr($filename, -$auth->company->filename_year_start, $auth->company->filename_year_length);
                            }
                        }

                        if (isset($YearString)) {
                            switch ($MonthString) {
                                case $auth->company->jan:
                                    $AutoPeriod = "$YearString-1-01";
                                    break;
                                case $auth->company->feb:
                                    $AutoPeriod = "$YearString-2-01";
                                    break;
                                case $auth->company->mar:
                                    $AutoPeriod = "$YearString-3-01";
                                    break;
                                case $auth->company->apr:
                                    $AutoPeriod = "$YearString-4-01";
                                    break;
                                case $auth->company->may:
                                    $AutoPeriod = "$YearString-5-01";
                                    break;
                                case $auth->company->jun:
                                    $AutoPeriod = "$YearString-6-18";
                                    break;
                                case $auth->company->jul:
                                    $AutoPeriod = "$YearString-7-01";
                                    break;
                                case $auth->company->aug:
                                    $AutoPeriod = "$YearString-8-01";
                                    break;
                                case $auth->company->sep:
                                    $AutoPeriod = "$YearString-9-01";
                                    break;
                                case $auth->company->oct:
                                    $AutoPeriod = "$YearString-10-01";
                                    break;
                                case $auth->company->nov:
                                    $AutoPeriod = "$YearString-11-01";
                                    break;
                                case $auth->company->dec:
                                    $AutoPeriod = "$YearString-12-01";
                                    break;
                                default:
                                    $AutoPeriod = NULL;
                            }
                        } elseif (isset($year)) {
                            switch ($MonthString) {
                                case $auth->company->jan:
                                    $AutoPeriod = "$year-1-01";
                                    break;
                                case $auth->company->feb:
                                    $AutoPeriod = "$year-2-01";
                                    break;
                                case $auth->company->mar:
                                    $AutoPeriod = "$year-3-01";
                                    break;
                                case $auth->company->apr:
                                    $AutoPeriod = "$year-4-01";
                                    break;
                                case $auth->company->may:
                                    $AutoPeriod = "$year-5-01";
                                    break;
                                case $auth->company->jun:
                                    $AutoPeriod = "$year-6-18";
                                    break;
                                case $auth->company->jul:
                                    $AutoPeriod = "$year-7-01";
                                    break;
                                case $auth->company->aug:
                                    $AutoPeriod = "$year-8-01";
                                    break;
                                case $auth->company->sep:
                                    $AutoPeriod = "$year-9-01";
                                    break;
                                case $auth->company->oct:
                                    $AutoPeriod = "$year-10-01";
                                    break;
                                case $auth->company->nov:
                                    $AutoPeriod = "$year-11-01";
                                    break;
                                case $auth->company->dec:
                                    $AutoPeriod = "$year-12-01";
                                    break;
                                default:
                                    $AutoPeriod = NULL;
                            }
                        } else {
                            $AutoPeriod = NULL;
                        }

                        if ($user) {
                            if ($user->email_notif_payslip == 1) {
                                \Mail::to($user)->send(new Notification($user));
                            }
                        }

                        if ($user) {
                            Payslip::create([
                                'user_id' => $user->id,
                                'period' => $AutoPeriod,
                                'filename' => $filename,
                                'path' => $path,
                                'company_id' => $companyId
                            ]);
                        } else {
                            Payslip::create([
                                'period' => $AutoPeriod,
                                'filename' => $filename,
                                'path' => $path,
                                'company_id' => $companyId
                            ]);
                        }
                    }

                    return redirect()->route('admin.viewallpayslips')->with([
                        'succeeded' => $succeeded,
                        'failed' => $failed
                    ]);
                }
            }
        } else {
            if ($request->hasFile('documents')) {
                $files = $request->file('documents');
                $uploaded = [];
                foreach ($files as $file) {
                    $userId = Input::post('user_id');
                    $companyId = Input::post('company_id');

                    foreach ($request->documents as $document) {
                        $filename = $document->getClientOriginalName();
                        $path = $document->store('payslips/' . $auth->company->id);
                        $uploaded[] = "Payslip with filename " . $filename . " has succesfully been uploaded";
                        Payslip::create([
                            'user_id' => $userId,
                            'filename' => $filename,
                            'path' => $path,
                            'company_id' => $companyId
                        ]);
                    }
                    return redirect()->route('admin.viewallpayslips')->with(['uploaded' => $uploaded]);
                }
            }
        }
    }

    public function uploadSingleSubmit(Request $request)
    {
        $auth = Auth::user();
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            foreach ($files as $file) {
                $userId = Input::post('user_id');
                $yearmonth = Input::post('period');
                $period = $yearmonth . "-01";
                $companyId = Input::post('company_id');

                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    $uploaded = "Payslip with filename " . $filename . " has succesfully been uploaded";
                    $path = $document->store('payslips/' . $auth->company->id);

                    $user = User::find($userId);
                    if ($user) {
                        if ($user->email_notif_payslip == 1) {
                            \Mail::to($user)->send(new Notification($user));
                        }
                    }
                    Payslip::create([
                        'user_id' => $userId,
                        'period' => $period,
                        'filename' => $filename,
                        'path' => $path,
                        'company_id' => $companyId
                    ]);
                }
                return redirect()->back()->with(['uploadedPS' => $uploaded]);
            }
        }
    }

    public function download($id)
    {
        $payslip = Payslip::find($id);
        $path = $payslip->path;
        $dlpayslip = storage_path("app/" . $path);

        return response()->download($dlpayslip, $payslip->filename);
    }

    public function pdf_viewer($id)
    {
        $payslip = Payslip::find($id);
        // $path is de directory waar de payslip is geupload
        $path = $payslip->path;
        $viewpayslip = storage_path("app/" . $path);

        return response()->file($viewpayslip);
    }

    public function update_payslip(Request $request, $id)
    {

        $this->validate($request, [
            'user_id',
            'period'
        ]);
        $payslip = Payslip::find($id);
        $payslip->user_id = $request->get('user_id');
        $yearmonth = $request->get('period');
        $period = $yearmonth . "-01";
        if ($request->get('period')) {
            $payslip->period = $period;
        } else {
            $payslip->period = NULL;
        }
        $user = User::find($payslip->user_id);
        if ($payslip->isDirty('user_id')) {
            if ($user->email_notif_payslip == 1) {
                \Mail::to($user)->send(new Notification($user));
            }
        }
        $payslip->save();
        return redirect()->route('admin.viewallpayslips')->with('succes', 'Data updated');
    }

    public function softdelete_payslip($id)
    {
        // SOFT DELETE
        $payslip = Payslip::find($id);
        Payslip::find($id)->delete();
        return back()
            ->with('deletedPS', 'Payslip with filename ' . $payslip->filename . ' has been soft-deleted');
    }

    public function restore_softdeleted_payslip($id)
    {
        $trashedpayslip = Payslip::withTrashed()->where('id', $id)->first();
        $trashedpayslip->restore();
        return back()
            ->with('succes', 'Payslip with filename ' . $trashedpayslip->filename . ' has been restored');
    }


    public function forcedelete_payslip($id)
    {
        if (auth()->user()->can('perma delete')) {
            $trashedpayslip = Payslip::withTrashed()->where('id', $id)->first();

            // check if file exists in storage
            $exists = Storage::exists("app/" . $trashedpayslip->path);
            if ($exists) {
                // delete from storage directory
                unlink(storage_path("app/" . $trashedpayslip->path));
            }
            // delete record from databse
            $trashedpayslip->forceDelete();

            return back()
                ->with('succes', 'Payslip with filename ' . $trashedpayslip->filename . ' has been perma-deleted');
        } else {
            return back();
        }
    }


    public function read_softdeleted_payslips()
    {
        $trashedpayslips = Payslip::onlyTrashed()->get();
        return view('admin.trash.trashed-payslips', compact('trashedpayslips'));
    }


    public function old_payslips()
    {
        $date = date("Y-m-d", strtotime("-7 years"));

        $payslips = Payslip::where('period', '<', $date)->get();

        return view('admin.all-old-payslips', compact('payslips'));
    }

    public function delete_all_old_payslips()
    {
        $date = date("Y-m-d", strtotime("-7 years"));

        $payslips = Payslip::where('period', '<', $date)->get();

        foreach ($payslips as $payslip) {

            // check if file exists in storage
            $exists = Storage::exists("app/" . $payslip->path);
            if ($exists) {
                // delete from storage directory
                unlink(storage_path("app/" . $payslip->path));
            }
            // delete record from databse
            $payslip->forceDelete();
        }

        return redirect()->back()->with('succes', 'All old payslips have been perma-deleted');
    }

    public function multiDelete(Request $request)
    {
        if ($request->get('payslips')) {
            $payslips = $request->get('payslips');
            foreach ($payslips as $payslipId) {
                $payslip = Payslip::find($payslipId);
                $payslip->delete();
                $deleted[] = "Payslip with filename " . $payslip->filename . " has succesfully been deleted";
            }
        }

        return redirect()->route('admin.viewallpayslips')->with(['deleted' => $deleted]);
    }

    public function multiPermaDeleteOrRestore(Request $request)
    {
        if ($request->get('permadelete')) {
            if ($request->get('payslips')) {
                $payslips = $request->get('payslips');
                foreach ($payslips as $payslipId) {
                    $payslip = Payslip::withTrashed()->where('id', $payslipId)->first();
                    $payslip->forceDelete();
                    $permadeletedPS[] = "Payslip with filename " . $payslip->filename . " has succesfully been deleted";
                }
                return back()->with(['permadeletedPS' => $permadeletedPS]);
            } else {
                return back()->with('warningPS', 'Please select some records');
            }
        } elseif ($request->get('restore')) {
            if ($request->get('payslips')) {
                $payslips = $request->get('payslips');
                foreach ($payslips as $payslipId) {
                    $payslip = Payslip::withTrashed()->where('id', $payslipId)->first();
                    $payslip->restore();
                    $restoredPS[] = "Payslip with filename " . $payslip->filename . " has succesfully been restored";
                }
                return back()->with(['restoredPS' => $restoredPS]);
            } else {
                return back()->with('warningPS', 'Please select some records');
            }
        } else {
            return back();
        }
    }
}
