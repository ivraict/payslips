<?php

namespace App\Http\Controllers;

use App\AnnualStatement;
use App\BankTransaction;
use App\Company;
use App\JournalEntry;
use App\PaymentStatement;
use App\PayrollTax;
use App\Payslip;
use App\PrivacyPolicy;
use App\Scopes\OrganisationPayslipScope;
use App\User;
use Faker\Provider\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use OwenIt\Auditing\Models\Audit;


class OwnerAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // DEZE FUNCTIE ZORGT ERVOOR DAT ALS DE USER NAAR /super-mega-admin GAAT, EN NIET INGELOGD IS, NAAR /login STUURT.
    public function __construct()
    {
        $this->middleware(['IsOwnerAdmin', '2fa']);
    }

    public function AuthenticateGoogle2FA(Request $request)
    {
        // get the logged in user
        $user = \Auth::user();

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // generate a new secret key for the user
        $secret = $google2fa->generateSecretKey();
        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $secret
        );

        $request->session()->flash('google2fa_secret', $secret);

        // Pass the QR barcode image to our view.
        return view('owner-admin.settings.google2fa', [
            'QR_Image' => $QR_Image,
            'secret' => $secret,
        ], compact('user'));
    }

    public function completeAuthentication(Request $request)
    {
        if (!Session::exists('google2fa_secret')) {
            return;
        }

        $user = Auth::user();

        $user->google2fa_secret = Session::get('google2fa_secret');
        $user->save();

        return redirect()->back()->with("succes", "Your account has been secured with Google 2 Factor!");
    }
    public function deleteAuthentication(Request $request)
    {


        $user = \Auth::user();

        $user->google2fa_secret = NULL;
        $user->save();
        return redirect()->route('owneradmin.settings.google2fa')->with('succes', 'Authentication Turned Off!');
    }
    // function that changes password
    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success", "Password changed successfully !");
    }
    public function settings()
    {
        $auth = Auth::user();
        return view('owner-admin.settings.settings');
    }

    public function pp_content()
    {
        return view('owner-admin/pp-content');
    }

    public function show_editform_pp($id)
    {
        $pp_particle = PrivacyPolicy::find($id);
//        dd($pp_particle);
        return view('owner-admin/edit-pp', compact('pp_particle'));
    }

    public function update_pp(Request $request, $id)
    {

        try {
            $this->validate($request, [
                'header',
                'text_field'
            ]);
        } catch (ValidationException $e) {
        }
        $privacypolicy = PrivacyPolicy::find($id);
        $privacypolicy->header = $request->get('header');
        $privacypolicy->text_field = $request->get('text_field');
        $privacypolicy->save();
        return redirect()->route('owneradmin.ppcontent')->with('succes', 'Data updated');
    }

    public function audits()
    {
        $companies = Company::orderBy('name', 'asc')->withTrashed()->get();
        $audits = Audit::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        $users = User::withTrashed()->get();
        return view('owner-admin/log', compact('audits', 'companies', 'users'));
    }

    public function view_company_audits($id)
    {
        // logcompany is the one company the owner is viewing
        $logcompany = Company::find($id);
        // all companies (for the dropdown)
        $companies = Company::orderBy('name', 'asc')->withTrashed()->get();

        $companyUsers = User::where('company_id', $id)->get();

        $userIds = array();
        foreach ($companyUsers as $companyUser) {
            $userIds[] = $companyUser->id;
        }

        $users = User::withTrashed()->get();

        $audits = Audit::whereIn('user_id', $userIds)->orderBy('created_at', 'desc')->get();
        return view('owner-admin/company-log', compact('audits', 'companies', 'logcompany', 'users'));
    }



    public function old_payslips(){
        $date = date("Y-m-d", strtotime("-7 years"));

        $payslips = Payslip::where('period', '<', $date)->get();

        return view('owner-admin.all-old-payslips', compact('payslips'));
    }

    public function forcedelete_payslip($id)
    {
        $trashedpayslip = Payslip::withTrashed()->where('id', $id)->first();

        // check if file exists in storage
        $exists = Storage::exists("app/" . $trashedpayslip->path);
        if ($exists) {
            // delete from storage directory
            unlink(storage_path("app/" . $trashedpayslip->path));
        }
        // delete record from databse
        $trashedpayslip->forceDelete();
        return back()
            ->with('succes', 'Payslip with filename ' . $trashedpayslip->filename . ' has been perma-deleted');
    }

    public function delete_all_old_payslips(){
        $date = date("Y-m-d", strtotime("-7 years"));

        $payslips = Payslip::where('period', '<', $date)->get();

        foreach ($payslips as $payslip) {

            // check if file exists in storage
            $exists = Storage::exists("app/" . $payslip->path);
            if ($exists) {
                // delete from storage directory
                unlink(storage_path("app/" . $payslip->path));
            }
            // delete record from databse
            $payslip->forceDelete();
        }

        return redirect()->back()->with('succes', 'All old payslips have been perma-deleted');
    }



}
