<?php

namespace App\Http\Controllers;

use App\MinimumWage;
use App\Scopes\OrganisationScope;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MinimumWageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $minimumwages = MinimumWage::where('law', 0)->get();
        $lawminimumwages = MinimumWage::where('law', 1)->withoutGlobalScope(OrganisationScope::class)->get();

        if (auth()->user()->hasRole('owner_admin')) {
            return view('owner-admin.minimum-wages', compact('lawminimumwages'));
        } else {
            return view('admin.all-minimum-wages', compact('minimumwages', 'lawminimumwages'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type',
            'age15',
            'age16',
            'age17',
            'age18',
            'age19',
            'age20',
            'age21',
            'age22AndOlder',
        ]);

        $minimumwage = new MinimumWage();
        $minimumwage->company_id = Auth::user()->company_id;
        if (auth()->user()->hasRole('owner_admin')) {
            $minimumwage->law = true;
        } else {
            $minimumwage->law = false;
        }
        $minimumwage->hoursweek = $request->input('hoursweek');
        $minimumwage->age15 = $request->input('age15');
        $minimumwage->age16 = $request->input('age16');
        $minimumwage->age17 = $request->input('age17');
        $minimumwage->age18 = $request->input('age18');
        $minimumwage->age19 = $request->input('age19');
        $minimumwage->age20 = $request->input('age20');
        $minimumwage->age21 = $request->input('age21');
        $minimumwage->age22AndOlder = $request->input('age22AndOlder');
        $minimumwage->save();
        return back()->with('succes', 'Minimum wage saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type',
            'age15',
            'age16',
            'age17',
            'age18',
            'age19',
            'age20',
            'age21',
            'age22AndOlder',
        ]);

        $minimumwage = MinimumWage::find($id);
        $minimumwage->company_id = Auth::user()->company_id;
        $minimumwage->hoursweek = $request->input('hoursweek');
        $minimumwage->age15 = $request->input('age15');
        $minimumwage->age16 = $request->input('age16');
        $minimumwage->age17 = $request->input('age17');
        $minimumwage->age18 = $request->input('age18');
        $minimumwage->age19 = $request->input('age19');
        $minimumwage->age20 = $request->input('age20');
        $minimumwage->age21 = $request->input('age21');
        $minimumwage->age22AndOlder = $request->input('age22');
        $minimumwage->save();
        return back()->with('succes', 'Minimum wage saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $minimumwage = MinimumWage::find($id);
        if (auth()->user()->hasRole('owner_admin')) {
            if ($minimumwage->law == true) {
                $minimumwage->forceDelete();
            }
        } else {
            if ($minimumwage->law == false) {
                $minimumwage->forceDelete();
            }
        }
        return redirect()->back()->with('succes', 'Record succesfully deleted');
    }
}
