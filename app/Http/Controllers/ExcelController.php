<?php

namespace App\Http\Controllers;

use App\BankTransaction;
use App\Exports\BankTransactionsExport;
use App\Imports\BankTransactionsImport;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function export()
    {
        return Excel::download(new BankTransactionsExport, 'banktransactions.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import()
    {

        Excel::import(new BankTransactionsImport, request()->file('file'));


        return back();
    }

    public function view_all_bank_transactions()
    {
        $banktransactions = BankTransaction::all();
        // these variables are returned from the mutlti delete bank transaction session
        $deleted = session()->get('deleted');

        return view('admin.all-bank-transactions', compact('banktransactions', 'deleted'));
    }

    public function view_all_failed_bank_transactions()
    {


        $trashedbanktransactions = BankTransaction::onlyTrashed()->get();
        return view('admin.all-failed-bank-transactions', compact('trashedbanktransactions'));
    }

    public function auto_assign_bank_transactions()
    {
        $banktransactions = BankTransaction::whereNull('user_id')->get();
        foreach ($banktransactions as $banktransaction) {
            $user = User::where('bank_account_number', $banktransaction->bank_account_number)->first();
            if ($user && isset($banktransaction->bank_account_number)) {
                $banktransaction->user_id = $user->id;
                $banktransaction->save();
            }
        }

        return back();
    }

    public function softdelete_bank_transaction($id)
    {
        BankTransaction::find($id)->delete();
        return back()->withInput();
    }

    public function restore_softdeleted_bank_transaction($id)
    {
        $trashedbanktransaction = BankTransaction::withTrashed()->where('id', $id)->first();
        $trashedbanktransaction->restore();
        return back()->withInput();
    }

    public function view_user_banktracs($id)
    {
        $user = User::where('id', $id)->first();
        $banktransactions = Banktransaction::where('user_id', $id)->get();

        return view('admin.user-bank-transactions', compact('banktransactions', 'user'));
    }

    //USER EXCEL
    public function user_export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function user_import()
    {
        $collection = Excel::import(new UsersImport, request()->file('file'));

        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if ($request->get('banktransactions')) {
            $banktransactions = $request->get('banktransactions');
            foreach ($banktransactions as $banktransactionId) {
                $banktransaction = BankTransaction::find($banktransactionId);
                $banktransaction->delete();
                $deleted[] = "Bank Transaction with ID " . $banktransaction->id . " has succesfully been deleted";
            }
        }

        return redirect()->route('admin.viewallbanktransactions')->with(['deleted' => $deleted]);
    }

}
