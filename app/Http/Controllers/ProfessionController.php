<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profession;
use App\User;
use App\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class ProfessionController extends Controller
{
    public function view_all_payslips()
    {
        $users = User::role('employee , admin')->get();
        $professions = Profession::all();

        // these variables are returned from the upload payslip session
        $succeeded = session()->get('succeeded');
        $failed = session()->get('failed');
        return view('admin.employee-page.work-information', compact('professions', 'users', 'succeeded', 'failed'));
    }




    public function update_profession(Request $request, $id)
    {
        $request->validate([
            'profession_id'


        ]);
        $profession = Profession::find($id);
        $profession->profession_id = $request->input('profession_id');
        $profession->save();
        return redirect()->back()
            ->with('workinfo', 'User with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been updated');
    }

    public function view_all_professions()
    {
        $professions = Profession::all();
        return view('admin.all-professions', compact('professions'));

    }

    public function store(Request $request)
    {
        $request->validate([
            'name',
            'company_id'
        ]);

        $profession = new Profession();
        $profession->id = $request->get('id');
        $profession->name = $request->get('name');
        $profession->company_id = $request->get('company_id');
        $profession->save();

        return redirect()->back();
    }


    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type',
            'nr1',
            'nr2',
            'nr3',
            'nr4',
            'nr5',
            'nr6',
            'nr7',
            'nr8',
            'nr9',
            'nr10',
        ]);

        $profession = Profession::find($id);
        $profession->company_id = Auth::user()->company_id;

        $profession->nr1 = $request->input('nr1');
        $profession->nr2 = $request->input('nr2');
        $profession->nr3 = $request->input('nr3');
        $profession->nr4 = $request->input('nr4');
        $profession->nr5 = $request->input('nr5');
        $profession->nr6 = $request->input('nr6');
        $profession->nr7 = $request->input('nr7');
        $profession->nr8 = $request->input('nr8');
        $profession->nr9 = $request->input('nr9');
        $profession->nr10 = $request->input('nr10');
        $profession->save();
        return back()->with('succes', 'Profession saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profession = Profession::find($id);
        $profession->forceDelete();

        return redirect()->back()->with('succes', 'Record succesfully deleted');
    }


}
