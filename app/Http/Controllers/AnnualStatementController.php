<?php

namespace App\Http\Controllers;

use App\Mail\NotificationAnnualStatement;
use App\AnnualStatement;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class AnnualStatementController extends Controller
{
    public function view_user_annuals($id)
    {
        $user = User::where('id', $id)->first();
        $annualstatements = AnnualStatement::where('user_id', $id)->get();

        // these variables are from the upload annual statement session
        $uploaded = session()->get('uploaded');
        return view('admin.user-annual-statements', compact('annualstatements', 'user', 'uploaded'));
    }

    public function uploadAnnualStatement(Request $request)
    {
        $auth = Auth::user();
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');
            $uploaded = [];

            foreach ($files as $file) {
                $userId = Input::post('user_id');
                $year = Input::post('year');
                $companyId = Input::post('company_id');

                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    $uploaded[] = "Annual Statement with filename " . $filename . " has succesfully been uploaded";
                    $path = $document->store('annual-statements/' . $auth->company->id);
                    AnnualStatement::create([
                        'user_id' => $userId,
                        'year' => $year,
                        'filename' => $filename,
                        'path' => $path,
                        'company_id' => $companyId
                    ]);
                }
                return redirect()->route('admin.viewallannualstatements')->with(['uploaded' => $uploaded]);
            }
        }
    }

    public function uploadSingleAnnualStatement(Request $request)
    {
        $auth = Auth::user();
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            foreach ($files as $file) {
                $userId = Input::post('user_id');
                $year = Input::post('year');
                $companyId = Input::post('company_id');

                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    $uploaded = "Annual Statement with filename " . $filename . " has succesfully been uploaded";
                    $path = $document->store('annual-statements/' . $auth->company->id);
                    AnnualStatement::create([
                        'user_id' => $userId,
                        'year' => $year,
                        'filename' => $filename,
                        'path' => $path,
                        'company_id' => $companyId

                    ]);
                }
                $user = User::find($userId);
                if ($user->email_notif_annual_statement == 1) {
                    \Mail::to($user)->send(new NotificationAnnualStatement($user));
                }
                return redirect()->back()->with(['uploadedAS' => $uploaded]);
            }
        }
    }

    public function view_all_annual_statements()
    {
        $users = User::role('employee')->get();
        $annualstatements = AnnualStatement::all();

        // these variables are returned from the mutlti delete annual statement session
        $deleted = session()->get('deleted');
        return view('admin.all-annual-statements', compact('annualstatements','users', 'deleted'));
    }

    public function download_annual_statement($id)
    {
        $annualstatement = AnnualStatement::find($id);
        $path = $annualstatement->path;
        $dlannualstatement = storage_path("app/" . $path);

        return response()->download($dlannualstatement, $annualstatement->filename);
    }

    public function view_annualstatement($id)
    {
        $annualstatement = AnnualStatement::find($id);
        // $path is the directory where the payslip is uploaded
        $path = $annualstatement->path;
        $viewannualstatement = storage_path("app/" . $path);

        return response()->file($viewannualstatement);
    }

    public function update_annual_statement(Request $request, $id)
    {

        //Hiermee kan de admin een ander file versturen, in het geval dat er een verkeerd file is verstuurd.

        $this->validate($request, [
            'user_id',
            'year'
        ]);

        $annualstatement = AnnualStatement::find($id);
        $annualstatement->user_id = $request->get('user_id');
        $annualstatement->year = $request->get('year');
        if ($annualstatement->isDirty('user_id')) {
            $user = User::find($annualstatement->user_id);
            if ($user->email_notif_annual_statement == 1) {
                \Mail::to($user)->send(new NotificationAnnualStatement($user));
            }
        }
        $annualstatement->save();
        return redirect()->route('admin.viewallannualstatements')->with('succes', 'Data updated');
    }

    public function softdelete_annual_statement($id)
    {
        $annualstatement = AnnualStatement::find($id);
        AnnualStatement::find($id)->delete();
        return back()
            ->with('deletedAS', 'Annual Statement with filename ' . $annualstatement->filename . ' has been soft-deleted');
    }

    public function restore_softdeleted_annual_statement($id)
    {
        $trashedannualstatement = AnnualStatement::withTrashed()->where('id', $id)->first();
        $trashedannualstatement->restore();
        return back()->withInput();
    }

    public function read_softdeleted_annual_statements()
    {
        $trashedannualstatements = AnnualStatement::onlyTrashed()->get();
        return view('admin.trash.trashed-annual-statements', compact('trashedannualstatements'));
    }

    public function forcedelete_annual_statement($id)
    {
        $trashedannualstatement = AnnualStatement::withTrashed()->where('id', $id)->first();

        // check if file exists in storage
        $exists = Storage::exists("app/" . $trashedannualstatement->path);
        if ($exists) {
            // delete from storage directory
            unlink(storage_path("app/" . $trashedannualstatement->path));
        }
        // delete record from database
        $trashedannualstatement->forceDelete();
        return back()
            ->with('succes', 'Annual Statement with filename ' . $trashedannualstatement->filename . ' has been perma-deleted');
    }

    public function multiDelete(Request $request)
    {
        if ($request->get('annualstatements')) {
            $annualstatements = $request->get('annualstatements');
            foreach ($annualstatements as $annualstatementId) {
                $annualstatement = AnnualStatement::find($annualstatementId);
                $annualstatement->delete();
                $deleted[] = "Annual Statement with filename " . $annualstatement->filename . " has succesfully been deleted";
            }
        }

        return redirect()->route('admin.viewallannualstatements')->with(['deleted' => $deleted]);
    }

    public function multiPermaDeleteOrRestore(Request $request)
    {
        if ($request->get('permadelete')) {
            if ($request->get('annualstatements')) {
                $annualstatements = $request->get('annualstatements');
                foreach ($annualstatements as $annualstatementId) {
                    $annualstatement = AnnualStatement::withTrashed()->where('id', $annualstatementId)->first();
                    $annualstatement->forceDelete();
                    $permadeletedAS[] = "Annual Statement with filename " . $annualstatement->filename . " has succesfully been deleted";
                }
                return back()->with(['permadeletedAS' => $permadeletedAS]);
            } else {
                return back()->with('warningAS', 'Please select some records');
            }
        } elseif ($request->get('restore')) {
            if ($request->get('annualstatements')) {
                $annualstatements = $request->get('annualstatements');
                foreach ($annualstatements as $annualstatementId) {
                    $annualstatement = AnnualStatement::withTrashed()->where('id', $annualstatementId)->first();
                    $annualstatement->restore();
                    $restoredAS[] = "Annual Statement with filename " . $annualstatement->filename . " has succesfully been restored";
                }
                return back()->with(['restoredAS' => $restoredAS]);
            } else {
                return back()->with('warningAS', 'Please select some records');
            }
        } else {
        return back();
      }
    }
}
