<?php

namespace App\Http\Controllers;

use App\AnnualStatement;
use App\Banktransaction;
use App\JournalEntry;
use App\PaymentStatement;
use App\PayrollTax;
use App\Payslip;
use App\User;
use Illuminate\Http\Request;

class TrashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // counts
        $usercount = User::role('employee')->onlyTrashed()->get()->count();
        $payslipcount = Payslip::onlyTrashed()->get()->count();
        $banktransactioncount = Banktransaction::onlyTrashed()->get()->count();
        $annualstatementcount = AnnualStatement::onlyTrashed()->get()->count();
        $payrolltaxcount = PayrollTax::onlyTrashed()->get()->count();
        $paymentstatementcount = PaymentStatement::onlyTrashed()->get()->count();
        $journalentrycount = JournalEntry::onlyTrashed()->get()->count();
        // trashed records
        $trashedusers = User::role('employee')->onlyTrashed()->get();
        $trashedpayslips = Payslip::onlyTrashed()->get();
        $trashedbanktransactions = Banktransaction::onlyTrashed()->get();
        $trashedannualstatements = AnnualStatement::onlyTrashed()->get();
        $trashedpayrolltaxes = PayrollTax::onlyTrashed()->get();
        $trashedpaymentstatements = PaymentStatement::onlyTrashed()->get();
        $trashedjournalentries = JournalEntry::onlyTrashed()->get();

        // these variables are returned from the multi delete/restore session
        $permadeletedUser = session()->get('permadeletedUser');
        $restoredUser = session()->get('restoredUser');
        $permadeletedPS = session()->get('permadeletedPS');
        $restoredPS = session()->get('restoredPS');
        $permadeletedAS = session()->get('permadeletedAS');
        $restoredAS = session()->get('restoredAS');
        return view('admin.trash.index', compact(
            'usercount', 'payslipcount', 'banktransactioncount', 'annualstatementcount',
            'payrolltaxcount', 'paymentstatementcount', 'journalentrycount', 'trashedusers', 'trashedpayslips',
            'trashedannualstatements', 'trashedpayrolltaxes', 'trashedpaymentstatements', 'trashedjournalentries',
            'trashedbanktransactions', 'permadeletedPS', 'restoredPS', 'permadeletedAS', 'restoredAS',
            'permadeletedUser', 'restoredUser'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
