<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();

        return view('owner-admin.permissions', compact('permissions'));
    }

    public function view_admin_permissions_as_owner_admin($id)
    {
        $admin = User::find($id);
        $permissions = Permission::all();
        $adminpermissions = $admin->getAllPermissions();
//        $adminpermissions = $admin->permission->name;

        $adminperminames = [];
        foreach ($adminpermissions as $adminpermission){
            array_push($adminperminames, $adminpermission->name);
        }


        return view('owner-admin.admin-permissions', compact('admin', 'permissions',
            'adminpermissions', 'adminperminames'));
    }

    public function assign_permission_as_owner_admin(Request $request, $id)
    {
        $admin = User::find($id);

        $admin->syncPermissions($request->get('permission'));

        return redirect()->route('owneradmin.show-company-information', ['id' => $admin->company_id])
        ->with('succes', 'Permissions updated for ' . $admin->first_name . ' ' . $admin->insertion . ' ' . $admin->last_name);
    }


    public function view_admin_permissions($id)
    {
        // view as admin

        $admin = User::find($id);
        $auth = Auth::user();

        $permissions = Permission::all();
        $adminpermissions = $admin->getAllPermissions();

        $adminperminames = [];
        foreach ($adminpermissions as $adminpermission){
            array_push($adminperminames, $adminpermission->name);
        }

        $admins = User::where('company_id', $auth->company_id)->role('admin')->get();

        $counter = [];
        foreach ($admins as $a) {
            $permis = $a->getPermissionNames();
            foreach ($permis as $permi)
                array_push($counter, $permi);
        }

        $uniquepermis = array_unique($counter);

        return view('admin.admin-permissions', compact('admin', 'authpermissions',
            'adminpermissions', 'adminperminames', 'permissions', 'authperminames', 'uniquepermis'));
    }


    public function assign_permission(Request $request, $id)
    {
        // as admin

        $admin = User::find($id);

        $admin->syncPermissions($request->get('permission'));

        return redirect()->route('admin.viewalladmins')
            ->with('succes', 'Permissions updated for ' . $admin->first_name . ' ' . $admin->insertion . ' ' . $admin->last_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
