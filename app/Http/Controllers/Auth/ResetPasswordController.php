<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    public function redirectTo(){

        // User role
        $role = Auth::user()->role->name;

        // Check user role, WHERE HE GOES AFTER LOGGING IN
        switch ($role) {
            case 'owner_admin':
                return '/owner-admin';
                break;
            case 'super_admin':
                return '/super-admin';
                break;
            case 'admin':
                return '/admin';
                break;
            case 'employee':
                if (Hash::check(Auth::user()->temp_password, Auth::user()->password)) {
                    return '/changepw-form';
                } else {
                    return '/payslips';
                }
                break;
            default:
                return '/';
                break;
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
