<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    public function redirectTo(){
        if (auth()->user()->hasRole('employee')){
            if (Hash::check(Auth::user()->temp_password, Auth::user()->password)) {
                return '/changepw-form';
            } else {
                return '/payslips';
            }
        } elseif (auth()->user()->hasRole('admin')) {
            return '/admin';
        } elseif (auth()->user()->hasRole('owner_admin')) {
            return '/owner-admin';
        } else {
            return '/';
        }
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'userLogout');
    }

    public function authenticated(Request $request, $user)
    {
        Session::put('company_id', $user->company_id);
    }

    public function userLogout()
    {
        Session::remove('company_id');
        Auth::guard('web')->logout();

        return redirect('/');
    }

}
