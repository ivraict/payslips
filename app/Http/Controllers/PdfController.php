<?php

namespace App\Http\Controllers;

use App\AnnualStatementDetail;
use App\PayslipDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class PdfController extends Controller
{
    public function show_make_payslip_form()
    {
        $users = User::role('employee')->get();

        return view('admin.pdf.form', compact('users'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'bank_account_number' => 'required',
            'period' => 'required',
        ]);

//
//

        $payslipdetail = new PayslipDetail();
        $payslipdetail->company_id = Auth::user()->company_id;
        $payslipdetail->external_id = $request->get('external_id');
        $payslipdetail->first_name = $request->get('first_name');
        $payslipdetail->insertion = $request->get('insertion');
        $payslipdetail->address = $request->get('address');
        $payslipdetail->place_of_residence = $request->get('place_of_residence');
        $payslipdetail->bank_account_number = $request->get('bank_account_number');
        $payslipdetail->last_name = $request->get('last_name');
        $payslipdetail->bsn = $request->get('bsn');
        $payslipdetail->period_number = $request->get('period_number');
        $payslipdetail->minimum_wage = $request->get('minimum_wage');
        $payslipdetail->employed_since = $request->get('employed_since');
        $periodRequested = $request->get('period');
        $periodStore = $periodRequested . "-01";
        $payslipdetail->period = $periodStore;
        $payslipdetail->date_of_birth = $request->get('date_of_birth');
        $payslipdetail->gross_salary = $request->get('gross_salary');
        $payslipdetail->filename = $request->get('filename');
        $payslipdetail->wage_per_hour = $request->get('wage_per_hour');
        $payslipdetail->payroll_tax_credit = $request->get('payroll_tax_credit');
        $payslipdetail->payroll_tax_tables = $request->get('payroll_tax_tables');

        $payslipdetail->save();


        return redirect()->route('admin.indexpdf');
    }

    public function index_pdf()
    {

        $payslipdetails = PayslipDetail::all();

        return view('admin.pdf.index', compact('payslipdetails'));
    }

    public function downloadPDF($id)
    {
        $payslipdetail = PayslipDetail::find($id);

        $start_date = $payslipdetail->period;
        $end_date = substr_replace($start_date, '30', -2, 2);

        $pdf = PDF::loadView('admin.pdf.pdf', compact('payslipdetail', 'start_date', 'end_date'));
        return $pdf->download($payslipdetail->filename . '.pdf');

    }


    /*
     |--------------------------------------------------------------------------
     | Make annual statement PDF
     |--------------------------------------------------------------------------
     */

    public function show_make_annual_statement_form()
    {
        $users = User::role('employee')->get();

        return view('admin.pdf.annual-statements.form', compact('users'));
    }

    public function store_annual_statement_detail(Request $request)
    {

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'bank_account_number' => 'required',
        ]);

        $annualstatementdetail = new AnnualStatementDetail();
        $annualstatementdetail->company_id = Auth::user()->company_id;
        $annualstatementdetail->external_id = $request->get('external_id');
        $annualstatementdetail->first_name = $request->get('first_name');
        $annualstatementdetail->insertion = $request->get('insertion');
        $annualstatementdetail->last_name = $request->get('last_name');
        $annualstatementdetail->bsn = $request->get('bsn');
        $annualstatementdetail->bank_account_number = $request->get('bank_account_number');
        $annualstatementdetail->address = $request->get('address');
        $annualstatementdetail->place_of_residence = $request->get('place_of_residence');
        $annualstatementdetail->bank_account_number = $request->get('bank_account_number');
        $annualstatementdetail->employed_since = $request->get('employed_since');
        $annualstatementdetail->date_of_birth = $request->get('date_of_birth');
        $annualstatementdetail->save();


        return redirect()->route('admin.indexannualstatementpdf');
    }

    public function index_annual_statement_pdf()
    {

        $annualstatementdetails = AnnualStatementDetail::all();

        return view('admin.pdf.annual-statements.index', compact('annualstatementdetails'));
    }

    public function download_annual_statement_PDF($id)
    {
        $annualstatementdetail = AnnualStatementDetail::find($id);

        $pdf = PDF::loadView('admin.pdf.annual-statements.pdf', compact('annualstatementdetail'));
        return $pdf->download($annualstatementdetail->filename . '.pdf');
    }
}
