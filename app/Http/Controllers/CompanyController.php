<?php

namespace App\Http\Controllers;

use App\AnnualStatement;
use App\BankTransaction;
use App\Company;
use App\JournalEntry;
use App\PaymentStatement;
use App\PayrollTax;
use App\Payslip;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Spatie\Permission\Models\Permission;

class CompanyController extends Controller
{
    public function view_all_companies()
    {
        $companies = Company::all();
        return view('owner-admin/all-companies', compact('companies'));
    }
    public function show_company_information($id)
    {
        $company = Company::find($id);
        $permissions = Permission::all();
        $admins = User::where('company_id', $id)->role('admin')->get();

        $counter = [];
        foreach ($admins as $admin) {
            $permis = $admin->getPermissionNames();
            foreach ($permis as $permi)
            array_push($counter, $permi);
        }

        $uniquepermis = array_unique($counter);


        return view('owner-admin.company-page.index', compact('admins', 'company', 'permissions',
            'uniquepermis'));
    }


    public function view_company_logo($id)
    {
        $company = Company::find($id);
        return view('owner-admin.company-page.company-logo', compact('company'));
    }

    public function insert_admin_to_company(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'insertion',
            'last_name' => 'required',
            'email' => 'required|unique:users',
            'company_id'
        ]);

        $user = new User();
        $user->assignRole('admin');
        $user->givePermissionTo($request->get('permission'));
        $user->first_name = $request->get('first_name');
        $user->insertion = $request->get('insertion');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = bcrypt(request('password'));
        $user->company_id = $request->get('company_id');
        $user->save();
        return redirect()->back()
            ->with('succes', 'Admin with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been created');
    }

    public function insert_company(Request $request)
    {

        //Hiermee kan de super admin nieuwe users toevoegen.

        $this->validate($request, [
            'name' => 'required',
        ]);

        $company = new Company();
        $company->name = $request->get('name');
        $company->save();
        return redirect()->back()->with('succes', 'Data inserted');
    }


    public function upload_company_logo(Request $request)
    {
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            $allowedfileExtension = ['jpg', 'png'];
            foreach ($files as $file) {
                $companyId = Input::post('company_id');
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);

                if ($check) {
                    foreach ($request->documents as $document) {
                        $document->storeAs('public/logos/' . $companyId, 'logo.' . $extension);
                        $logo_path = 'logos/' . $companyId . '/logo.' . $extension;

                        $company = Company::find($companyId);
                        $company->logo_path = $logo_path;
                        $company->save();
                    }
                }
                return back()->with('success', 'Logo succesfully uploaded');
            }
        }
    }

    public function delete_company_logo()
    {
        $companyId = Input::post('company_id');
        $company = Company::find($companyId);
        // check if file exists in storage
        // $exists = Storage::exists("/app/public/" . $company->logo_path);

        // delete from storage directory
        unlink(storage_path("/app/public/" . $company->logo_path));

        // delete from db
        $company->logo_path = NULL;
        $company->save();

        return back()->with('success', 'Logo deleted');

    }

    public function update_sa(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'insertion',
            'last_name' => 'required',
            'email' => 'required',
            'google2fa_secret'
        ]);

        $user = User::find($id);
        $user->first_name = $request->get('first_name');
        $user->insertion = $request->get('insertion');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->google2fa_secret = $request->get('google2fa_secret');
        $user->save();
        return back();
    }

    public function softdelete_company($id)
    {
        Company::find($id)->delete();

        // soft delete users belonging to the company
        $users = User::where('company_id', $id)->get();
        foreach ($users as $user) {
            $user->delete();
        }

        // soft delete payslips belonging to the company
        $payslips = Payslip::where('company_id', $id)->get();
        foreach ($payslips as $payslip) {
            $payslip->delete();
        }

        // soft delete annual statements belonging to the company
        $annualstatements = AnnualStatement::where('company_id', $id)->get();
        foreach ($annualstatements as $annualstatement) {
            $annualstatement->delete();
        }

        // soft delete journal entries belonging to the company
        $journalentries = JournalEntry::where('company_id', $id)->get();
        foreach ($journalentries as $journalentry) {
            $journalentry->delete();
        }

        // soft delete payment statements belonging to the company
        $paymentstatements = PaymentStatement::where('company_id', $id)->get();
        foreach ($paymentstatements as $paymentstatement) {
            $paymentstatement->delete();
        }

        // soft delete payroll taxes belonging to the company
        $payrolltaxes = PayrollTax::where('company_id', $id)->get();
        foreach ($payrolltaxes as $payrolltax) {
            $payrolltax->delete();
        }

        // soft delete bank transactions belonging to the company
        $banktransactions = BankTransaction::where('company_id', $id)->get();
        foreach ($banktransactions as $banktransaction) {
            $banktransaction->delete();
        }

        return back()->withInput();
    }

    public function restore_softdeleted_company($id)
    {
        $company = Company::withTrashed()->where('id', $id)->first();
        $company->restore();

        // restore users belonging to the company
        $users = User::onlyTrashed()->where('company_id', $id)->get();
        foreach ($users as $user) {
            $user->restore();
        }

        // restore payslips belonging to the company
        $payslips = Payslip::onlyTrashed()->where('company_id', $id)->get();
        foreach ($payslips as $payslip) {
            $payslip->restore();
        }

        // restore annual statements belonging to the company
        $annualstatements = AnnualStatement::onlyTrashed()->where('company_id', $id)->get();
        foreach ($annualstatements as $annualstatement) {
            $annualstatement->restore();
        }

        // restore journal entries belonging to the company
        $journalentries = JournalEntry::onlyTrashed()->where('company_id', $id)->get();
        foreach ($journalentries as $journalentry) {
            $journalentry->restore();
        }

        // restore payment statements belonging to the company
        $paymentstatements = PaymentStatement::onlyTrashed()->where('company_id', $id)->get();
        foreach ($paymentstatements as $paymentstatement) {
            $paymentstatement->restore();
        }

        // restore payroll taxes belonging to the company
        $payrolltaxes = PayrollTax::onlyTrashed()->where('company_id', $id)->get();
        foreach ($payrolltaxes as $payrolltax) {
            $payrolltax->restore();
        }

        // restore bank transactions belonging to the company
        $banktransactions = BankTransaction::onlyTrashed()->where('company_id', $id)->get();
        foreach ($banktransactions as $banktransaction) {
            $banktransaction->restore();
        }

        return back()->withInput();
    }

    public function forcedelete_company($id)
    {
        $company = Company::withTrashed()->where('id', $id)->first();
        $company->forceDelete();

        // force delete users belonging to the company
        $users = User::onlyTrashed()->where('company_id', $id)->get();
        foreach ($users as $user) {
            $user->forceDelete();
        }

        // force delete payslips belonging to the company
        $payslips = Payslip::onlyTrashed()->where('company_id', $id)->get();
        foreach ($payslips as $payslip) {
            $payslip->forceDelete();
        }

        // force delete annual statements belonging to the company
        $annualstatements = AnnualStatement::onlyTrashed()->where('company_id', $id)->get();
        foreach ($annualstatements as $annualstatement) {
            $annualstatement->forceDelete();
        }

        // force delete journal entries belonging to the company
        $journalentries = JournalEntry::onlyTrashed()->where('company_id', $id)->get();
        foreach ($journalentries as $journalentry) {
            $journalentry->forceDelete();
        }

        // force delete payment statements belonging to the company
        $paymentstatements = PaymentStatement::onlyTrashed()->where('company_id', $id)->get();
        foreach ($paymentstatements as $paymentstatement) {
            $paymentstatement->forceDelete();
        }

        // force delete payroll taxes belonging to the company
        $payrolltaxes = PayrollTax::onlyTrashed()->where('company_id', $id)->get();
        foreach ($payrolltaxes as $payrolltax) {
            $payrolltax->forceDelete();
        }

        // force delete bank transactions belonging to the company
        $banktransactions = BankTransaction::onlyTrashed()->where('company_id', $id)->get();
        foreach ($banktransactions as $banktransaction) {
            $banktransaction->forceDelete();
        }
        return back()->withInput();
    }


    public function read_softdeleted_companies()
    {
        $trashedcompanies = Company::onlyTrashed()->get();
        return view('owner-admin.trashed-companies', compact('trashedcompanies'));
    }


}
