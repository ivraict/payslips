<?php

namespace App\Http\Controllers;

use App\AnnualStatement;
use App\Payslip;
use App\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public $msg;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // unused

    public function AuthenticateGoogle2FA(Request $request)
    {
        // get the logged in user
        $user = \Auth::user();

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // generate a new secret key for the user
        $secret = $google2fa->generateSecretKey();
        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $secret
        );

        $request->session()->flash('google2fa_secret', $secret);

        // Pass the QR barcode image to our view.
        return view('employee.google2fa', [
            'QR_Image' => $QR_Image,
            'secret' => $secret,
        ], compact('user'));
    }

    public function completeAuthentication(Request $request)
    {
        if (!Session::exists('google2fa_secret')) {
            return;
        }

        $user = Auth::user();

        $user->google2fa_secret = Session::get('google2fa_secret');
        $user->save();

        return redirect()->back()->with("succes", "Your account has been secured with Google 2 Factor!");
    }

    public function deleteAuthentication(Request $request)
    {


        $user = \Auth::user();

        $user->google2fa_secret = NULL;
        $user->save();
        return redirect()->route('employee.google2fa')->with('succes', 'Authentication Turned Off!');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // unused
    public function ShowPayslips()
    {
        // SHOW PAYSLIPS THAT BELONG TO THE USER ID THAT IS LOGGED IN

        $user = Auth::user();
        $payslips = Payslip::where('user_id', Auth::user()->id)->get();
        return view('employee.payslips', compact('payslips', 'user'));

    }

    public function showAnnualStatements()
    {
        // SHOW PAYSLIPS THAT BELONG TO THE USER ID THAT IS LOGGED IN
        $user = Auth::user();
        $annualstatements = AnnualStatement::where('user_id', Auth::user()->id)->get();
        return view('employee.annual-statements', compact('user', 'annualstatements'));

    }


    // show change password form
    public function showChangePasswordForm()
    {
        return view('employee.changepassword');
    }

    // function that changes password
    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        User::disableAuditing();
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        User::enableAuditing();
        return redirect()->back()->with("changePw", "Password changed successfully !");
    }

    // changing password for the first time
    public function changePasswordFirstTime(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->route('payslips')->with("success", "Password changed successfully !");
    }

//    $id = de id van de payslip
    public function download_payslip($id)
    {
        // $auth_id = de id van de ingelogde user
        $auth_id = Auth::user()->id;
        $payslip = Payslip::find($id);
        // $path is de directory waar de payslip is geupload
        $path = $payslip->path;
        $dlpayslip = storage_path("app/" . $path);

        // de ingelogde user mag alleen zijn eigen loonstroken downloaden:
        // als diegene de download adress link van een andere user invoert, returnt back.
        if ($auth_id == $payslip->user_id) {
            return response()->download($dlpayslip, $payslip->filename);
        } else return back();
    }

    public function pdf_viewer($id)
    {
        // $auth_id = de id van de ingelogde user
        $auth_id = Auth::user()->id;
        $payslip = Payslip::find($id);
        // $path is de directory waar de payslip is geupload
        $path = $payslip->path;
        $viewpayslip = storage_path("app/" . $path);

        // de ingelogde user mag alleen zijn eigen loonstroken downloaden:
        // als diegene de download adress link van een andere user invoert, returnt back.
        if ($auth_id == $payslip->user_id) {
            return response()->file($viewpayslip);
        } else return back();
    }


    //    $id = de id van de jaaropgave
    public function download_annual_statement($id)
    {
        // $auth_id = de id van de ingelogde user
        $auth_id = Auth::user()->id;
        $annualstatement = AnnualStatement::find($id);
        // $path is de directory waar de payslip is geupload
        $path = $annualstatement->path;
        $dlannualstatement = storage_path("app/" . $path);

        // de ingelogde user mag alleen zijn eigen jaaropgaves downloaden:
        // als diegene de download adress link van een andere user invoert, returnt back.
        if ($auth_id == $annualstatement->user_id) {
            return response()->download($dlannualstatement);
        } else return back();
    }


    public function view_annualstatement($id)
    {
        // $auth_id = de id van de ingelogde user
        $auth_id = Auth::user()->id;
        $annualstatement = AnnualStatement::find($id);
        // $path is de directory waar de payslip is geupload
        $path = $annualstatement->path;
        $viewannualstatement = storage_path("app/" . $path);

        // de ingelogde user mag alleen zijn eigen jaaropgaves downloaden:
        // als diegene de download adress link van een andere user invoert, returnt back.
        if ($auth_id == $annualstatement->user_id) {
            return response()->file($viewannualstatement);
        } else return back();
    }


    /*
    |--------------------------------------------------------------------------
    | CRUD Personal Information
    |--------------------------------------------------------------------------
    */


    public function update(Request $request)
    {

        //Hiermee worden gegevens van employees aangepast.

        $this->validate($request, [

            'email',
            'date_of_birth',
            'residence',
            'street_name',
            'house_number',
            'postal_code',
            'bank_account_number',
            'phone_number',
            'bsn'
        ]);

        $personalinformationn = Auth::user();
//        $personalinformationn->first_name = $request->input('first_name');
//        $personalinformationn->insertion = $request->input('insertion');
//        $personalinformationn->last_name = $request->input('last_name');
//        $personalinformationn->email = $request->input('email');
        $personalinformationn->date_of_birth = $request->input('date_of_birth');
        $personalinformationn->residence = $request->input('residence');
        $personalinformationn->street_name = $request->input('street_name');
        $personalinformationn->house_number = $request->input('house_number');
        $personalinformationn->postal_code = $request->input('postal_code');
        $personalinformationn->bank_account_number = $request->input('bank_account_number');
        $personalinformationn->phone_number = $request->input('phone_number');
        $personalinformationn->bsn = $request->input('bsn');
        $personalinformationn->save();
        return redirect()->back()->with('succes', 'Data updated');
    }


    public function showPersonalInformation()
    {
        // SHOW PAYSLIPS THAT BELONG TO THE USER ID THAT IS LOGGED IN
        $user = Auth::user();
        return view('employee/personal-information', compact('user'));
    }


    /*
|--------------------------------------------------------------------------
| CRUD Settings
|--------------------------------------------------------------------------
*/

    public function settings()
    {
        $user = Auth::user();
        return view('employee.settings.settings', compact('user'));
    }

    public function update_settings(Request $request)
    {


//        $this->validate($request, [
//            'email_notif_payslip',
//            'email_notif_annual_statement',
//            'google2fa_secret'
//        ]);

        $user = Auth::user();
        $user->email_notif_payslip = $request->get('email_notif_payslip');
        $user->email_notif_annual_statement = $request->get('email_notif_annual_statement');
        $user->google2fa_secret = $request->get('google2fa_secret');
        $user->save();
        return back()->with('settings', 'Settings updated');

    }


}

