<?php

namespace App\Http\Controllers;

use App\PaymentStatement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PaymentStatementController extends Controller
{
    public function view_all_payment_statements()
    {
        $paymentstatements = PaymentStatement::all();
        return view('admin.all-payment-statements', compact('paymentstatements'));
    }

    public function download_payment_statement($id)
    {
        $paymentstatement = PaymentStatement::find($id);
        $path = $paymentstatement->path;
        $dlpaymentstatement = storage_path("app/" . $path);

        return response()->download($dlpaymentstatement, $paymentstatement->filename);
    }


    public function softdelete_payment_statement($id)
    {
        $paymentstatement = PaymentStatement::find($id);
        PaymentStatement::find($id)->delete();
        return back()->with('deletedAS', 'Payment Statement with filename ' . $paymentstatement->filename . ' has been soft-deleted');
    }

    public function restore_softdeleted_payment_statement($id)
    {
        $trashedpaymentstatement = PaymentStatement::withTrashed()->where('id', $id)->first();
        $trashedpaymentstatement->restore();
        return back()->withInput();
    }

    public function read_softdeleted_payment_statements()
    {
        $trashedpaymentstatements = PaymentStatement::onlyTrashed()->get();
        return view('admin.trash.trashed-payment-statements', compact('trashedpaymentstatements'));
    }

    public function uploadPaymentStatement(Request $request)
    {
        $auth = Auth::user();
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            foreach ($files as $file) {

                $yearmonth = Input::post('period');
                $companyId = Input::post('company_id');
                $period = $yearmonth . "-01";
                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    $uploaded[] = "Journal Entry with filename " . $filename . " has succesfully been uploaded";
                    $path = $document->store('payment-statements/' . $auth->company->id);
                    PaymentStatement::create([
                        'company_id' => $companyId,
                        'period' => $period,
                        'filename' => $filename,
                        'path' => $path

                    ]);
                }
                return back()->with(['uploaded' => $uploaded]);
            }
        }
    }

    public function forcedelete_payment_statement($id)
    {
        if(auth()->user()->can('perma delete')) {
            $trashedpaymentstatement = PaymentStatement::withTrashed()->where('id', $id)->first();

            // check if file exists in storage
            $exists = Storage::exists("app/" . $trashedpaymentstatement->path);
            if ($exists) {
                // delete from storage directory
                unlink(storage_path("app/" . $trashedpaymentstatement->path));
            }
            // delete record from database
            $trashedpaymentstatement->forceDelete();

        return back()
            ->with('succes', 'Annual Statement with filename ' . $trashedpaymentstatement->filename . ' has been perma-deleted');
        } else {
            return back();
        }

    }
}
