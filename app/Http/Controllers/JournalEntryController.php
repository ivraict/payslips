<?php

namespace App\Http\Controllers;

use App\JournalEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class JournalEntryController extends Controller
{
    public function view_all_journal_entries()
    {
        //Hiermee word er een view getoond waarin alle journaalposten worden weergeven.

        $journalentries = JournalEntry::all();
        return view('admin.all-journal-entries', compact('journalentries'));
    }

    public function download_journal_entry($id)
    {

        //Hiermee kunnen de journaalopgaven worden gedownload.

        $journalentry = JournalEntry::find($id);
        $path = $journalentry->path;
        $dljournalentry = storage_path("app/" . $path);

        return response()->download($dljournalentry, $journalentry->filename);
    }


    public function softdelete_journal_entry($id)
    {
        $journalentry = Journalentry::find($id);
        JournalEntry::find($id)->delete();
        return back()->with('deletedAS', 'Journal Entry with filename ' . $journalentry->filename . ' has been soft-deleted');
    }

    public function restore_softdeleted_journal_entry($id)
    {
        $trashedjournalentry = JournalEntry::withTrashed()->where('id', $id)->first();
        $trashedjournalentry->restore();
        return back()->withInput();
    }

    public function read_softdeleted_journal_entries()
    {
        $trashedjournalentries = JournalEntry::onlyTrashed()->get();
        return view('admin.trash.trashed-journal-entries', compact('trashedjournalentries'));
    }

    public function uploadJournalEntry(Request $request)
    {
        $auth = Auth::user();
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            foreach ($files as $file) {

                $yearmonth = Input::post('period');
                $companyId = Input::post('company_id');
                $period = $yearmonth . "-01";
                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    $uploaded[] = "Journal Entry with filename " . $filename . " has succesfully been uploaded";
                    $path = $document->store('journal-entries/' . $auth->company->id);
                    JournalEntry::create([
                        'company_id' => $companyId,
                        'period' => $period,
                        'filename' => $filename,
                        'path' => $path

                    ]);
                }
                return back()->with(['uploaded' => $uploaded]);
            }
        }
    }

    public function forcedelete_journal_entry($id)
    {
        if(auth()->user()->can('perma delete')) {
            $trashedjournalentry = JournalEntry::withTrashed()->where('id', $id)->first();

            // check if file exists in storage
            $exists = Storage::exists("app/" . $trashedjournalentry->path);
            if ($exists) {
                // delete from storage directory
                unlink(storage_path("app/" . $trashedjournalentry->path));
            }
            // delete record from database
            $trashedjournalentry->forceDelete();
        return back()
            ->with('succes', 'Journal Entry with filename ' . $trashedjournalentry->filename . ' has been perma-deleted');
        } else {
            return back();
        }
    }
}
