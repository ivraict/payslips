<?php

namespace App\Http\Controllers;

use App\AnnualStatementDetail;
use App\BankTransaction;
use App\Exports\BankTransactionsExport;
use App\Http\Requests\UserStoreRequest;
use App\Imports\BankTransactionsImport;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\PayrollTax;
use App\PaymentStatement;
use App\JournalEntry;
use App\AnnualStatement;
use App\Company;
use App\Mail\Notification;
use App\Mail\NotificationAnnualStatement;
use App\Mail\Welcome;
use App\Payslip;
use App\PayslipDetail;
use App\User;
use App\UserDetail;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Session;
use OwenIt\Auditing\Models\Audit;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // DEZE FUNCTIE ZORGT ERVOOR DAT ALS DE USER NAAR /admin GAAT, EN NIET INGELOGD IS, NAAR /login STUURT.
    public function __construct()
    {
        $this->middleware(['IsAdmin']);
    }

    // function that sets or rewrites the google 2-factor secret
    public function AuthenticateGoogle2FA(Request $request)
    {
        // get the logged in user
        $user = \Auth::user();

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // generate a new secret key for the user
        $secret = $google2fa->generateSecretKey();
        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $secret
        );

        $request->session()->flash('google2fa_secret', $secret);

        // Pass the QR barcode image to our view.
        return view('admin.settings.google2fa', [
            'QR_Image' => $QR_Image,
            'secret' => $secret,
        ], compact('user'));
    }

    public function completeAuthentication(Request $request)
    {
        if (!Session::exists('google2fa_secret')) {
            dd('ho');
        }

        $user = Auth::user();

        $user->google2fa_secret = Session::get('google2fa_secret');
        $user->save();

        return redirect()->back()->with("succes", "Your account has been secured with Google 2 Factor!");
    }

    public function deleteAuthentication(Request $request)
    {
        $user = \Auth::user();

        $user->google2fa_secret = NULL;
        $user->save();
        return redirect()->route('admin.google2fa')->with('succes', 'Authentication Turned Off!');
    }

    // function that changes password
    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not match with the password you provided. Please try again.");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        User::disableAuditing();
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        User::enableAuditing();
        return redirect()->back()->with("changePw", "Password changed successfully !");
    }

    public function edit_company_data()
    {
        //Hiermee word een form opgehaald waarmee de  admin gegevens van employees kan aanpassen.

        $auth = Auth::user();
        $company = Company::where('id', $auth->company_id)->first();
        return view('admin.settings.edit-company-data', compact('company'));

    }

    public function update_company_data(Request $request)
    {

        //Hiermee worden de gegevens van een employee aangepast.


        $this->validate($request, [
            'name' => 'required',
            'email',
            'address',
            'phone_number',
            'phone_number2',
            'footer_text',
            'footer_color',
            'theme_color',
            'facebook_link',
            'instagram_link',
            'linkedin_link'
        ]);

        $auth = Auth::user();
        $company = Company::where('id', $auth->company_id)->first();
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->address = $request->get('address');
        $company->phone_number = $request->get('phone_number');
        $company->phone_number2 = $request->get('phone_number2');
        $company->footer_text = $request->get('footer_text');
        $company->footer_color = $request->get('footer_color');
        $company->theme_color = $request->get('theme_color');
        $company->facebook_link = $request->get('facebook_link');
        $company->instagram_link = $request->get('instagram_link');
        $company->linkedin_link = $request->get('linkedin_link');
        $company->save();

        return back()->with('CompanyUpdated', 'Data updated');
    }

    public function edit_auto_assign_settings()
    {
        //Hiermee word een form opgehaald waarmee de  admin gegevens van employees kan aanpassen.

        $auth = Auth::user();
        $company = Company::where('id', $auth->company_id)->first();
        return view('admin.settings.edit-auto-assign-settings', compact('company'));

    }

    public function update_auto_assign_settings(Request $request)
    {

        //Hiermee worden de gegevens van een employee aangepast.

        try {
            $this->validate($request, [
                'substr_start_or_end',
                'filename_id_start',
                'filename_id_length',
                'filename_year_start',
                'filename_year_length',
                'filename_month_start',
                'filename_month_length',
                'jan',
                'feb',
                'mar',
                'apr',
                'may',
                'jun',
                'jul',
                'aug',
                'sep',
                'oct',
                'nov',
                'dec'

            ]);
        } catch (ValidationException $e) {
        }
        $auth = Auth::user();
        $company = Company::where('id', $auth->company_id)->first();
        $company->substr_start_or_end = $request->get('substr_start_or_end');
        $company->filename_id_start = $request->get('filename_id_start');
        $company->filename_id_length = $request->get('filename_id_length');
        $company->filename_year_start = $request->get('filename_year_start');
        $company->filename_year_length = $request->get('filename_year_length');
        $company->filename_month_start = $request->get('filename_month_start');
        $company->filename_month_length = $request->get('filename_month_length');
        $company->jan = $request->get('jan');
        $company->feb = $request->get('feb');
        $company->mar = $request->get('mar');
        $company->apr = $request->get('apr');
        $company->may = $request->get('may');
        $company->jun = $request->get('jun');
        $company->jul = $request->get('jul');
        $company->aug = $request->get('aug');
        $company->sep = $request->get('sep');
        $company->oct = $request->get('oct');
        $company->nov = $request->get('nov');
        $company->dec = $request->get('dec');
        $company->save();

        return back()->with('uploadSettings', 'Data updated');
    }

    public function uploadTest(Request $request)
    {
        $auth = Auth::user();
//Hiermee worden loonstrook files geupload.
        if ($request->hasFile('documents')) {
            $files = $request->file('documents');
            foreach ($files as $file) {
                foreach ($request->documents as $document) {
                    $filename = $document->getClientOriginalName();
                    if ($auth->company->substr_start_or_end == 0) {
                        $AutoUserId = substr($filename, $auth->company->filename_id_start, $auth->company->filename_id_length);
                    } elseif ($auth->company->substr_start_or_end == 1) {
                        $AutoUserId = substr($filename, -$auth->company->filename_id_start, $auth->company->filename_id_length);
                    }

                    if ($auth->company->substr_start_or_end == 0) {
                        $MonthString = substr($filename, $auth->company->filename_month_start, $auth->company->filename_month_length);
                    } elseif ($auth->company->substr_start_or_end == 1) {
                        $MonthString = substr($filename, -$auth->company->filename_month_start, $auth->company->filename_month_length);
                    }

                    if ($auth->company->substr_start_or_end == 0) {
                        $YearString = substr($filename, $auth->company->filename_year_start, $auth->company->filename_year_length);
                    } elseif ($auth->company->substr_start_or_end == 1) {
                        $YearString = substr($filename, -$auth->company->filename_year_start, $auth->company->filename_year_length);
                    }

                    switch ($MonthString) {
                        case $auth->company->jan:
                            $month = "January";
                            break;
                        case $auth->company->feb:
                            $month = "February";
                            break;
                        case $auth->company->mar:
                            $month = "March";
                            break;
                        case $auth->company->apr:
                            $month = "April";
                            break;
                        case $auth->company->may:
                            $month = "May";
                            break;
                        case $auth->company->jun:
                            $month = "June";
                            break;
                        case $auth->company->jul:
                            $month = "July";
                            break;
                        case $auth->company->aug:
                            $month = "August";
                            break;
                        case $auth->company->sep:
                            $month = "September";
                            break;
                        case $auth->company->oct:
                            $month = "October";
                            break;
                        case $auth->company->nov:
                            $month = "November";
                            break;
                        case $auth->company->dec:
                            $month = "December";
                            break;
                        default:
                            $AutoPeriod = NULL;
                    }
                }
                return view('admin/settings/upload-test', compact('AutoUserId', 'MonthString', 'YearString',
                    'AutoPeriod', 'filename', 'month'));
            }
        }
    }


    public function audits()
    {
//        $audits = Payslip::find(1)->audits;
        $companyUsers = User::where('company_id', Auth::user()->company_id)->get();

        $userIds = array();
        foreach ($companyUsers as $companyUser) {
            $userIds[] = $companyUser->id;
        }

        $companies = Company::orderBy('name', 'asc')->withTrashed()->get();
        $users = User::withTrashed()->get();
        $audits = Audit::whereIn('user_id', $userIds)->orderBy('created_at', 'desc')->get();
        return view('admin/log', compact('audits', 'users', 'companies'));
    }

    public function settings()
    {
        $auth = Auth::user();
        $company = Company::where('id', $auth->company_id)->first();
        return view('admin.settings.settings', compact('company'));
    }

    public function update_personal_details(Request $request)
    {
        $this->validate($request, [

            'first_name',
            'insertion',
            'last_name',
        ]);
        $personalinformationn = Auth::user();
        $personalinformationn->first_name = $request->input('first_name');
        $personalinformationn->insertion = $request->input('insertion');
        $personalinformationn->last_name = $request->input('last_name');
        $personalinformationn->save();
        return back()->with('succes', 'Data updated');
    }

    /*
   |--------------------------------------------------------------------------
   | CRUD Admins
   |--------------------------------------------------------------------------
   */


    public function view_admins()
    {
        $admins = User::role('admin')->get();

        $permissions = Permission::all();

        return view('admin.all-admins', compact('admins', 'permissions'));
    }

    public function forcedelete_admin(Request $request, $id)
    {
        if(auth()->user()->can('delete admins')) {
            $admin = User::find($id);
            if (auth()->user()->id !== $admin->id) {
                $admin->forceDelete();
                return redirect()->back()
                    ->with('deleted', 'Admin "' . $admin->first_name . " " . $admin->insertion . " " . $admin->last_name .
                        '" succesfully perma-deleted');
            } else {
                return redirect()->back()->with('warning', 'You cannot delete yourself');
            }
        } else {
            return redirect()->back()->with('warning', 'You do not have this permission');
        }
    }

    public function destroy_admin()
    {
        return back();
    }

    public function store_admin(Request $request)
    {
        $this->validate($request, [
            'role',
            'permission',
            'first_name' => 'required',
            'insertion',
            'last_name' => 'required',
            'password' => 'required|string|min:6',
            'email' => 'required|unique:users',
            'company_id'
        ]);

        $user = new User();
        $user->assignRole('admin');
        $user->givePermissionTo($request->get('permission'));
        $user->company_id= auth()->user()->company_id;
        $user->first_name = $request->get('first_name');
        $user->insertion = $request->get('insertion');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = bcrypt(request('password'));
        $user->temp_password = $request->get('password');
        \Mail::to($user)->send(new Welcome($user));
        $user->save();
        return redirect()->back()
            ->with('succes', 'Admin with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been created');
    }


    /*
    |--------------------------------------------------------------------------
    | User Info
    |--------------------------------------------------------------------------
    */


}
