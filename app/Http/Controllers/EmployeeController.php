<?php

namespace App\Http\Controllers;

use App\AnnualStatement;
use App\BankTransaction;
use App\Company;
use App\Mail\Welcome;
use App\Payslip;
use App\Profession;
use App\User;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::role('employee')->get();
        // these variables are returned from the mutlti delete user session
        $deleted = session()->get('deleted');
        // import
        $employees = session()->get('employees');
        return view('admin/all-users', compact('users', 'deleted', 'employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'role',
            'external_id',
            'first_name' => 'required',
            'insertion',
            'last_name' => 'required',
            'date_of_birth' ,
            'residence' ,
            'street_name' ,
            'house_number'  ,
            'postal_code'  ,
            'bank_account_number' => 'required|unique:users',
            'bsn' => 'required|unique:users',
            'email' => 'required|unique:users|',
            'password' => 'required|string|min:6',
            'company_id'
        ]);

        $user = new User();
        $user->id = $request->get('id');
        $user->assignRole('employee');
        $user->external_id = $request->get('external_id');
        $user->first_name = $request->get('first_name');
        $user->insertion = $request->get('insertion');
        $user->last_name = $request->get('last_name');
        $user->date_of_birth = $request->get('date_of_birth');
        $user->residence = $request->get('residence');
        $user->street_name = $request->get('street_name');
        $user->house_number = $request->get('house_number');
        $user->postal_code = $request->get('postal_code');
        $user->bank_account_number = $request->get('bank_account_number');
        $user->bsn = $request->get('bsn');
        $user->email = $request->get('email');
        $user->password = bcrypt(request('password'));
        $user->temp_password = $request->get('password');
        $user->company_id = $request->get('company_id');
        $user->save();
        if ($request->get('mailnotif')) {
            \Mail::to($user)->send(new Welcome($user));
        }
        return redirect()->back()
            ->with('succes', 'User with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'external_id' ,
            'first_name' => 'required',
            'insertion',
            'last_name' => 'required',
            'email' => 'required',
            'date_of_birth'  ,
            'residence'  ,
            'street_name' ,
            'house_number'  ,
            'postal_code'  ,
            'bank_account_number' => 'required' ,
            'phone_number' ,
            'bsn' => 'required',


        ]);
        $user = User::find($id);
        $user->external_id = $request->input('external_id');
        $user->first_name = $request->input('first_name');
        $user->insertion = $request->input('insertion');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->date_of_birth = $request->input('date_of_birth');
        $user->residence = $request->input('residence');
        $user->street_name = $request->input('street_name');
        $user->house_number = $request->input('house_number');
        $user->postal_code = $request->input('postal_code');
        $user->bank_account_number = $request->input('bank_account_number');
        $user->phone_number = $request->input('phone_number');
        $user->bsn = $request->input('bsn');
        $user->save();
        return redirect()->back()
            ->with('succes', 'User with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been updated');
    }

    public function update_work(Request $request, $id)
    {
        $request->validate([
            'external_id',
            'profession',
            'employed_since',
            'work_days',
            'minimum_wage',
            'wage_per_hour',
            'payroll_tax_credit'

        ]);
        $user = User::find($id);
        $user->external_id = $request->input('external_id');
        $user->profession_id = $request->input('profession');
        $user->employed_since = $request->input('employed_since');
        $user->work_days = $request->input('work_days');
        $user->minimum_wage = $request->input('minimum_wage');
        $user->wage_per_hour = $request->input('wage_per_hour');
        $user->payroll_tax_credit = $request->input('payroll_tax_credit');
        $user->save();
        return redirect()->back()
            ->with('workinfo', 'User with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->can('perma delete')) {
            // Force delete
            // the variable $user is used for the succes message, at the bottom of this function
            $trasheduser = User::withTrashed()->where('id', $id)->first();

            $trasheduser->forceDelete();

            // force delete payslips belonging to that user
            $trashedpayslips = Payslip::onlyTrashed()->where('user_id', $id)->get();
            foreach ($trashedpayslips as $trashedpayslip) {
                $trashedpayslip->forceDelete();
            }

            // force delete annual statements belonging to that user
            $trashedannualstatements = AnnualStatement::onlyTrashed()->where('user_id', $id)->get();
            foreach ($trashedannualstatements as $trashedannualstatement) {
                $trashedannualstatement->forceDelete();
            }

        return back()
            ->with('succes', 'User with the name "' . $trasheduser->first_name . ' ' . $trasheduser->insertion . ' ' . $trasheduser->last_name . '" has succesfully been perma-deleted');
        } else {
            return back();
        }
    }


    public function softdelete($id)
    {

        $user = User::find($id);
        if (User::find($id)->hasRole('employee')) {
            User::find($id)->delete();
        }

        // soft delete payslips belonging to that user
        $payslips = Payslip::where('user_id', $id)->get();
        foreach ($payslips as $payslip) {
            $payslip->delete();
        }

        // soft delete annual statements belonging to that user
        $annualstatements = AnnualStatement::where('user_id', $id)->get();
        foreach ($annualstatements as $annualstatement) {
            $annualstatement->delete();
        }

        return redirect()->back()
            ->with('succes', 'User with the name "' . $user->first_name . ' ' . $user->insertion . ' ' . $user->last_name . '" has succesfully been soft-deleted');
    }

    public function restore($id)
    {
        $trasheduser = User::withTrashed()->where('id', $id)->first();
        $trasheduser->restore();

        // restore payslips belonging to that user
        $trashedpayslips = Payslip::onlyTrashed()->where('user_id', $id)->get();
        foreach ($trashedpayslips as $trashedpayslip) {
            $trashedpayslip->restore();
        }

        // restore annual statements belonging to that user
        $trashedannualstatements = AnnualStatement::onlyTrashed()->where('user_id', $id)->get();
        foreach ($trashedannualstatements as $trashedannualstatement) {
            $trashedannualstatement->restore();
        }

        return back()
            ->with('succes', 'User with the name "' . $trasheduser->first_name . ' ' . $trasheduser->insertion . ' ' . $trasheduser->last_name . '" has succesfully been restored');
    }

    public function readSoftdeleted()
    {
        $trashedusers = User::onlyTrashed()->get();
        return view('admin.trash.trashed-users', compact('trashedusers'));
    }


    /*
    |--------------------------------------------------------------------------
    | User Info
    |--------------------------------------------------------------------------
    */


//    public function showPersonalInformation($id)
//    {
//        $user = User::where('id', $id)->first();
//
//        return view('admin/personal-information', compact('user'));
//    }

    public function show_user_information($id)
    {
        $user = User::where('id', $id)->first();
        $company = Company::where('id', $user->company_id)->first();
        $payslips = Payslip::where('user_id', $id)->get();
        $annualstatements = AnnualStatement::where('user_id', $id)->get();
        $banktransactions = Banktransaction::where('user_id', $id)->get();
        $professions = Profession::where('company_id', $user->company_id)->get();


        return view('admin.employee-page.user-profilepage', compact('company', 'user', 'annualstatements', 'banktransactions', 'payslips', 'professions'));
    }

//    public function user_settings($id)
//    {
//        $user = User::where('id', $id)->first();
//        return view('admin.user-settings', compact('user', 'id'));
//    }

    public function update_user_settings(Request $request, $id)
    {


        $this->validate($request, [
            'email_notif_payslip',
            'email_notif_annual_statement',
            'google2fa_secret'
        ]);

        $user = User::where('id', $id)->first();
        $user->email_notif_payslip = $request->get('email_notif_payslip');
        $user->email_notif_annual_statement = $request->get('email_notif_annual_statement');
        $user->google2fa_secret = $request->get('google2fa_secret');
        $user->save();
        return back()->with('settings', 'Settings updated');

    }

    public function multiDelete(Request $request)
    {
        if ($request->get('users')) {
            $users = $request->get('users');
            foreach ($users as $userId) {
                $user = User::find($userId);
                $user->delete();
                $deleted[] = "Employee with name " . $user->first_name . " " .
                    $user->insertion . " " .
                    $user->last_name .
                    " has succesfully been deleted";
            }
        }

        return redirect()->route('admin.viewallusers')->with(['deleted' => $deleted]);
    }

    public function multiPermaDeleteOrRestore(Request $request)
    {
        if ($request->get('permadelete')) {
            if ($request->get('users')) {
                $users = $request->get('users');
                foreach ($users as $userId) {
                    $user = User::withTrashed()->where('id', $userId)->first();
                    $user->forceDelete();
                    $permadeletedUser[] = "Employee with name " . $user->first_name . " " .
                        $user->insertion . " " .
                        $user->last_name .
                        " has succesfully been perma deleted";
                }
                return back()->with(['permadeletedUser' => $permadeletedUser]);
            } else {
                return back()->with('warningUser', 'Please select some records');
            }
        } elseif ($request->get('restore')) {
            if ($request->get('users')) {
                $users = $request->get('users');
                foreach ($users as $userId) {
                    $user = User::withTrashed()->where('id', $userId)->first();
                    $user->restore();
                    $restoredUser[] = "Employee with name " . $user->first_name . " " .
                        $user->insertion . " " .
                        $user->last_name .
                        " has succesfully been restored";
                }
                return back()->with(['restoredUser' => $restoredUser]);
            } else {
                return back()->with('warningUser', 'Please select some records');
            }
        } else {
          return back();
        }
    }
}
