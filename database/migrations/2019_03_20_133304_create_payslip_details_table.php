<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslip_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('insertion')->nullable();
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('place_of_residence')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('filename')->nullable();
            $table->integer('external_id')->nullable();
            $table->string('minimum_wage')->nullable();
            $table->string('wage_per_hour')->nullable();
            $table->string('payroll_tax_credit')->nullable();
            $table->string('payroll_tax_tables')->nullable();
            $table->string('surcharge_hours')->nullable();
            $table->string('holiday_hours')->nullable();
            $table->string('holidays')->nullable();
            $table->string('short_absence')->nullable();
            $table->string('net_salary')->nullable();
            $table->string('bsn')->nullable();
            $table->date('period')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('employed_since')->nullable();
            $table->string('gross_salary')->nullable();
            $table->string('period_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslip_details');
    }
}
