<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualStatementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annual_statement_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('insertion')->nullable();
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('place_of_residence')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('filename')->nullable();
            $table->integer('external_id')->nullable();
            $table->string('bsn')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('employed_since')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_statement_details');
    }
}
