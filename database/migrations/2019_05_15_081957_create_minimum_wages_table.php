<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinimumWagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minimum_wages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_id')->nullable();
            $table->string('hoursweek')->nullable();
            $table->string('age15')->nullable();
            $table->string('age16')->nullable();
            $table->string('age17')->nullable();
            $table->string('age18')->nullable();
            $table->string('age19')->nullable();
            $table->string('age20')->nullable();
            $table->string('age21')->nullable();
            $table->string('age22AndOlder')->nullable();
            $table->boolean('law')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minimum_wages');
    }
}
