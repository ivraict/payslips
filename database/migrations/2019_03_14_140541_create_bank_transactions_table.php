<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->date('date')->nullable();
            $table->string('name')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('contra_account_number')->nullable();
            $table->string('code')->nullable();
            $table->string('out_in')->nullable();
            $table->string('amount')->nullable();
            $table->string('mutation_type')->nullable();
            $table->text('communications')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_transactions');
    }
}
