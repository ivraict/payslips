<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('external_id')->nullable();
            $table->string('first_name');
            $table->string('insertion')->nullable();
            $table->string('last_name');
            $table->string('initials')->nullable();
            $table->string('email')->unique();
            $table->string('gender')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('house_number')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('residence')->nullable();
            $table->string('country')->nullable();
            $table->string('nationality')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('birthplace')->nullable();
            $table->string('bsn')->nullable();
            $table->string('status')->nullable();
            $table->string('workplace')->nullable();
            $table->string('bank_account_number')->nullable()->unique();
            $table->string('minimum_wage')->nullable();
            $table->string('wage_per_hour')->nullable();
            $table->date('employed_since')->nullable();
            $table->integer('profession_id')->nullable();
            $table->boolean('payroll_tax_credit')->nullable();
            $table->string('work_days')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('temp_password')->nullable();
            $table->integer('company_id')->nullable();
            $table->boolean('email_notif_payslip')->default(0);
            $table->boolean('email_notif_annual_statement')->default(0);
            $table->rememberToken();
            $table->timestamps();

            $table->index('external_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
