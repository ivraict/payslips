<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $category = new Category();
        $category->name = "Technical Issues";
        $category->save();
        $category = new Category();
        $category->name = "Feedback & Suggestion";
        $category->save();
        $category = new Category();
        $category->name = "General inquiries";
        $category->save();
    }
}
