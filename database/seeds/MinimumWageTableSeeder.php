<?php

use App\MinimumWage;
use Illuminate\Database\Seeder;

class MinimumWageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $minimumwage = new MinimumWage();
        $minimumwage->company_id = 1;
        $minimumwage->hoursweek = 36;
        $minimumwage->age15 = 5;
        $minimumwage->age16 = 6;
        $minimumwage->age17 = 7;
        $minimumwage->age18 = 7.50;
        $minimumwage->age19 = 8;
        $minimumwage->age20 = 10;
        $minimumwage->age21 = 11;
        $minimumwage->age22AndOlder = 12;
        $minimumwage->law = 0;
        $minimumwage->save();
        $minimumwage = new MinimumWage();
        $minimumwage->company_id = NULL;
        $minimumwage->hoursweek = 36;
        $minimumwage->age15 = 3.15;
        $minimumwage->age16 = 3.62;
        $minimumwage->age17 = 4.15;
        $minimumwage->age18 = 5.25;
        $minimumwage->age19 = 6.30;
        $minimumwage->age20 = 8.39;
        $minimumwage->age21 = 10.49;
        $minimumwage->age22AndOlder = 10.49;
        $minimumwage->law = 1;
        $minimumwage->save();
    }
}
