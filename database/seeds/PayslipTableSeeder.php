<?php

use App\Payslip;
use Illuminate\Database\Seeder;

class PayslipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $payslip = new Payslip();
        $payslip->user_id = '1';
        $payslip->filename = 'loonstrook_joe_nov';
        $payslip->period = '2011-11-1';
        $payslip->company_id = '1';
        $payslip->save();
        $payslip = new Payslip();
        $payslip->user_id = '1';
        $payslip->filename = 'loonstrook_joe_dec';
        $payslip->period = '2018-12-1';
        $payslip->company_id = '1';
        $payslip->save();
        $payslip = new Payslip();
        $payslip->user_id = '2';
        $payslip->filename = 'loonstrook_alt_okt';
        $payslip->period = '2018-10-1';
        $payslip->company_id = '1';
        $payslip->save();
    }
}
