<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Define order in which tables are seeder
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(PayslipTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
//        $this->call(PrivacyPolicyTableSeeder::class);
//        $this->call(CategoryTableSeeder::class);
//        $this->call(ProfessionTableSeeder::class);
//        $this->call(MinimumWageTableSeeder::class);

    }
}
