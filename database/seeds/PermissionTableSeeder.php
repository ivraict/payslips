<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permission = new Permission();
        $permission->name = 'assign permissions';
        $permission->save();
        $permission = new Permission();
        $permission->name = 'generate pdf';
        $permission->save();
        $permission = new Permission();
        $permission->name = 'perma delete';
        $permission->save();
        $permission = new Permission();
        $permission->name = 'delete admins';
        $permission->save();
        $permission = new Permission();
        $permission->name = 'create admins';
        $permission->save();
        $permission = new Permission();
        $permission->name = 'edit company data';
        $permission->save();
        $permission = new Permission();
        $permission->name = 'edit auto assign';
        $permission->save();
    }
}
