<?php

use App\Company;
use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $company = new Company();
        $company->id = '1';
        $company->name = 'Mastiek';
        $company->email = 'fake@mail.com';
        $company->address = 'Nepplein 5, 666 OD Amsterdam';
        $company->phone_number = '+ 31 99 26 25 77';
        $company->phone_number2 = '+ 31 72 76 40 21';
        // auto assign settings
        $company->filename_id_start = 20;
        $company->filename_id_length = 3;
        $company->filename_month_start = 28;
        $company->filename_month_length = 3;
        $company->facebook_link = 'https://www.facebook.com/Mastiek.Amsterdam/';
        $company->instagram_link = 'https://www.instagram.com/mastiek.work/';
        $company->linkedin_link = 'https://nl.linkedin.com/company/mastiek';
        $company->jan = '10_';
        $company->feb = '20_';
        $company->mar = '30_';
        $company->apr = '40_';
        $company->may = '50_';
        $company->jun = '60_';
        $company->jul = '70_';
        $company->aug = '80_';
        $company->sep = '90_';
        $company->oct = '100';
        $company->nov = '110';
        $company->dec = '120';
        $company->save();
        $company = new Company();
        $company->id = '2';
        $company->name = 'Company2';
        $company->email = 'asdsadasd@mail.com';
        $company->address = 'Ander Adress 1, 231 OD Almere';
        $company->phone_number = '+ 31 53 21 51 55 ';
        $company->phone_number2 = '+ 31 86 83 76 01';
        $company->substr_start_or_end = 1;
        $company->filename_id_start = 38;
        $company->filename_id_length = 5;
        $company->filename_year_start = 33;
        $company->filename_year_length = 4;
        $company->filename_month_start = 29;
        $company->filename_month_length = 2;
        $company->jan = '01';
        $company->feb = '02';
        $company->mar = '03';
        $company->apr = '04';
        $company->may = '05';
        $company->jun = '06';
        $company->jul = '07';
        $company->aug = '08';
        $company->sep = '09';
        $company->oct = '10';
        $company->nov = '11';
        $company->dec = '12';
        $company->save();
    }
}
