<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    $user = new User();
        $user->external_id = '177';
        $user->assignRole('employee');
        $user->first_name = 'Joe';
        $user->insertion = 'van';
        $user->last_name = 'Sick';
        $user->date_of_birth = '2000-1-1';
        $user->employed_since = '2018-1-1';
        $user->email = 'employee@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->company_id = '1';
        $user->bsn = '12345678';
        $user->residence = 'Almere';
        $user->postal_code = '6969 XD';
        $user->street_name = 'floortwente';
        $user->house_number = '420';
        $user->wage_per_hour = '6,50';
        $user->minimum_wage = '400';
        $user->work_days = '10,0';
        $user->bank_account_number = 'NL59INGB0007690573';
        $user->save();
        $user = new User();
        $user->external_id = '183';
        $user->assignRole('employee');
        $user->first_name = 'Alt';
        $user->last_name = 'User';
        $user->date_of_birth = '2000-1-1';
        $user->email = 'employee2@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->company_id = '1';
        $user->save();
        $user = new User();
        $user->assignRole('admin');
        $user->first_name = 'Admin';
        $user->last_name = 'xD';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->company_id = '1';
        $user->save();
        $user = new User();
        $user->assignRole('admin');
        $user->givePermissionTo('generate pdf');
        $user->givePermissionTo('perma delete');
        $user->givePermissionTo('assign permissions');
        $user->givePermissionTo('delete admins');
        $user->givePermissionTo('create admins');
        $user->givePermissionTo('edit company data');
        $user->givePermissionTo('edit auto assign');
        $user->first_name = 'SuperAdmin';
        $user->last_name = 'Best';
        $user->email = 'superadmin@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->company_id = '1';
        $user->save();
        $user = new User();
        $user->assignRole('owner_admin');
        $user->first_name = 'Owner';
        $user->last_name = 'Admin';
        $user->email = 'owneradmin@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->save();
        $user = new User();
        $user->assignRole('admin');
        $user->first_name = 'Johnny';
        $user->last_name = 'Test';
        $user->email = 'sacompany2@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->company_id = '2';
        $user->save();
        $user = new User();
        $user->assignRole('employee');
        $user->external_id = '92127';
        $user->first_name = 'Employee';
        $user->last_name = 'KFC';
        $user->email = 'employee3@example.com';
        $user->password = bcrypt('secret');
        $user->temp_password = 'secret';
        $user->company_id = '2';
        $user->save();

    }
}
