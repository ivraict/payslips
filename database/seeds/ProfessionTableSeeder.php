<?php

use App\Profession;
use Illuminate\Database\Seeder;

class ProfessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profession = new Profession();
        $profession->name = 'Horeca Medewerker';
        $profession->company_id = 1;
        $profession->save();
        $profession = new Profession();
        $profession->name = 'Schoonmaker';
        $profession->company_id = 1;
        $profession->save();
        $profession = new Profession();
        $profession->name = 'Kassamedewerker';
        $profession->company_id = 1;
        $profession->save();

    }
}
