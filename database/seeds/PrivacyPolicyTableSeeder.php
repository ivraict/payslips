<?php

use App\PrivacyPolicy;
use Illuminate\Database\Seeder;

class PrivacyPolicyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $privacy_policy = new PrivacyPolicy();
        $privacy_policy->text_field = 'Wanneer u de loonstrokenapplicatie gebruikt, geeft u toestemming aan Mastiek om uw gegevens op te slaan en te beheren in databases. Deze gegevens betreffen uw loonstroken en persoonlijke gegevens.
    Op het moment dat u het hier niet mee eens bent of wanneer u meer informatie wilt hebben dan kunt u contact opnemen met Mastiek. Het beheer van Mastiek kan uw account verwijderen op het moment dat u het niet eens bent met ons beleid.';
        $privacy_policy->save();
        $privacy_policy = new PrivacyPolicy();
        $privacy_policy->header = 'Doel van de Loonstrokenapplicatie en de Gegevens';
        $privacy_policy->text_field = 'Dit is een loonstrokenapplicatie voor de werknemers van Mastiek. 
    Elke werknemer heeft zijn/haar persoonlijke account. Dit account is toegangkelijk voor de eigenaar van het account en de systeembeeheerders
    Werknemers kunnen door middel van de loonstrokenapplicatie, hun maandelijkse loonstroken downloaden en oudere loonstroken bekijken.
    De interface van de loonstrokenapplicatie bevat zo min mogelijk persoonlijke gegevens. In de loonstroken staan de meest gevoelige gegevens, maar deze zullen niet direct zichtbaar zijn op de pagina.
    De gegevens die in de loonstroken staan zijn verplicht, volgens de wet.
    De Directie van Mastiek en de Fiscalist onder opdracht van Mastiek zijn de enige andere partijen, met toestemming tot de inhoud van de loonstroken.';
        $privacy_policy->save();
        $privacy_policy = new PrivacyPolicy();
        $privacy_policy->header = 'Manier van Privacy Waarborging';
        $privacy_policy->text_field = 'In geval van hackers hebben wij enkele maatregelen getroffen waarbij dit minder waarschijnlijk is. Niet iedereen kan bij het persoonlijk account van een medewerker. Om op uw profiel terecht te komen moet u eerst inloggen.
    Elke gebruiker krijgt van te voren inloggegevens van Mastiek. Op het moment dat u ingelogt bent is er een mogelijkheid om uw wachtwoord aan te passen, dit kan van toepassing zijn wanneer u denkt het huidige wachtwoord te vergeten.
    Om in te loggen op de loonstrokenapplicatie moet u dubbel geverifieerd worden, de applicatie maakt gebruikt van pincode en een normaal wachtwoord.
    Wachtwoorden van gebruikers worden gehasht bewaard in de Database, dit wil zeggen dat wachtwoorden niet direct afgelezen kunnen worden.
    De loonstrokenapplicatie is zorgvuldig in elkaar gezet en tjdens het bouwen van de loonstrokenapplicatie zijn alle functies meerdere malen getest.';
        $privacy_policy->save();
        $privacy_policy = new PrivacyPolicy();
        $privacy_policy->header = 'Gedrag met Persoonsgegevens';
        $privacy_policy->text_field = 'Op het moment dat u akkoord gaat met de voorwaarden van de loonsrokenapplicatie, zal Mastiek voor u een persoonlijk account creëren.
    Alleen de hoog nodige gegevens van de gebruiker zullen in het profiel zichtbaar zijn.
    De inloggegevens voor het account worden gestuurd naar het e-mail adres die de werknemer heeft ogegeven.
    Op het moment dat een werknemer wilt inloggen moet hij/zij deze inloggegevens invoeren en zal daarna kunnen inloggen en een bevestiging krijgen over het e-mail adres.
    Mastiek stuurt alle gegevens van een bepaalde wernemer naar een fiscalist. Deze fiscalist zet al deze informatie om naar een loonstrook. Deze loonstroken zijn in pdf. formaat.
    De loonstroken komen terecht op het persoonlijk profiel van de werknemer, waar de werknemer vervolgens het bestand kan downloaden.';
        $privacy_policy->save();
    }
}
